const axios = require('axios');
const config = require('../../config/custom');
const segfyToken = config.custom.segfy_token;
const segfyAPIUrl = 'https://multicalculo.segfy.com/api';

async function listProfessions(req, res) {
    const url = `${segfyAPIUrl}/profissoes/${segfyToken}`;

    try {
        let response = await axios.get(url);
        res.status(200).json(response.data);
    } catch (err) {
        res.status(500).json({ err: err.toString() });
    }
}

async function listBrands(req, res) {
    const url = `${segfyAPIUrl}/marcas/${segfyToken}`;
    
    try {
        const response = await axios.get(url);
        res.status(200).json(response.data);
    } catch(err) {
        res.status(500).json({ err: err.toString() });
    }
}

async function listModels(req, res) {
    const { brand_id, model_year } = req.params;
    
    if (!brand_id || !model_year) {
        res.status(401).json('missing brand id or model year');
        return;
    }

    const url = `${segfyAPIUrl}/modelos/${segfyToken}/?marca_id=${brand_id}&ano_modelo=${model_year}`;

    try {
        const response = await axios.get(url);
        res.status(200).json(response.data);
    } catch(err) {
        res.status(500).json({ err: err });
    }
}

async function listAntiTheft(req, res) {
    const { category } = req.params;

    if (!category) {
        res.status(401).json('missing category');
        return;
    }

    const url = `${segfyAPIUrl}/dispositivos/${segfyToken}?categoria=${category}`;

    try {
        const response = await axios.get(url);
        res.status(200).json(response.data);
    } catch(err) {
        res.status(500).json({ err: err });
    }
}

module.exports = {
    listProfessions,
    listAntiTheft,
    listBrands,
    listModels
};

