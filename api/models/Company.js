module.exports = {
  attributes: {
		name: {
			type: 'string'
    },
    email: {
			type: 'string'
    },
    phone: {
			type: 'string'
    }
	},
};