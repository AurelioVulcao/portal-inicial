angular.module('vencorr').controller('QuotationEmpresarialController', function($scope, $routeParams, $rootScope, $location, QuotationService, SweetAlert, CustomerService, MetaService, Upload, config) {
    $scope.loading = false;
    $scope.isEdit = false;
    $scope.segment = "empresarial";
    $scope.hash = $routeParams.hash;
    $scope.customerSnapshot = {};
    $scope.loadingCustomer = false;
    $scope.searchCustomer = {};
    $scope.customerId;
    $scope.chooseCustomer = false;
	$scope.cepLoading = false;
    $scope.quotation = {
        ocurrences: [],
        risky_places: [],
        files: []
    };
    $scope.ocurrencesList = [];
    $scope.residenceTypes = [{name: "AEROPORTO", value: "aeroporto"},{name: "ALAMEDA", value: "alameda"},{name: "ÁREA", value: "area"},{name: "AVENIDA", value: "avenida"},{name: "CHÁCARA", value: "chacara"},{name: "CONJUNTO", value: "conjunto"},{name: "COLÔNIA", value: "colonia"},{name: "CAMPO", value: "campo"},{name: "CONDOMÍNIO", value: "condominio"},{name: "DISTRITO", value: "distrito"},{name: "ESPLANADA", value: "esplanada"},{name: "ESTRADA", value: "estrada"},{name: "ESTAÇÃO", value: "estação"},{name: "FEIRA", value: "feira"},{name: "FAVELA", value: "favela"},{name: "FAZENDA", value: "fazenda"},{name: "JARDIM", value: "jardim"},{name: "LAGO", value: "lago"},{name: "LADEIRA", value: "ladeira"},{name: "LAGOA", value: "lagoa"},{name: "LARGO", value: "largo"},{name: "LOTEAMENTO", value: "loteamento"},{name: "MORRO", value: "morro"},{name: "NÚCLEO", value: "nucleo"},{name: "OUTROS", value: "outros"},{name: "PRAÇA", value: "praca"},{name: "PARQUE", value: "parque"},{name: "PASSARELA", value: "passarela"},{name: "PÁTIO", value: "patio"},{name: "QUADRA", value: "quadra"},{name: "RUA", value: "rua"},{name: "RECANTO", value: "recanto"},{name: "RODOVIA", value: "rodovia"},{name: "RESIDENCIAL", value: "residencial"},{name: "SITIO", value: "sitio"},{name: "SETOR", value: "setor"},{name: "TRECHO", value: "trecho"},{name: "TRAVESSA", value: "travessa"},{name: "TREVO", value: "trevo"},{name: "VIA", value: "via"},{name: "VIADUTO", value: "viaduto"},{name: "VILA", value: "vila"},{name: "VALE", value: "vale"},{name: "VIELA", value: "viela"},{name: "VEREDA", value: "vereda"}];
    $scope.listFiles = [{}];
    $scope.quotation.renovation = "novo";
    $scope.showOcurrenceForm = false;
    $scope.dynamicPopover = {
        bonus_anterior : {
            title: 'Deve-se informar a classe de bônus anterior. Seguradoras que usam esse campo:',
            content: 'Sulamerica, Bradesco, Generali'
        },
        bonus_futura : {
            title: 'Classe de bônus a ser calculada',
            content: 'Deve-se informar a classe de bonus que será calculada na nova vigência.\
            Por exemplo: Se a classe de bonus vigente é 2, então você irá informar neste campo 3.'
        }
    };
    $scope.listAllCoverages = [{"name":"Incêndio","status":false,"lmi":null},{"name":"Alagamento","status":false,"lmi":null},{"name":"Anúncios Luminosos / Letreiro","status":false,"lmi":null},{"name":"Bagagem","status":false,"lmi":null},{"name":"Bens do Segurado em Poder de Terceiros - Decorrente Incêndio","status":false,"lmi":null},{"name":"Bens do Segurado em Poder de Terceiros - Coberturas Acessórias","status":false,"lmi":null},{"name":"Bens e Equipamentos Portateis - Perímetro de Cobertura Território Brasileiro","status":false,"lmi":null},{"name":"Bens e Equipamentos Portateis - Perímetro de Cobertura Todo o Mundo","status":false,"lmi":null},{"name":"Danos a Mercadorias em Processo de Fabricação - Work Damage","status":false,"lmi":null},{"name":"Danos Elétricos","status":false,"lmi":null},{"name":"Derrame Acidental de Chuveiros Automáticos de Combate a Incêndio - Sprinklers","status":false,"lmi":null},{"name":"Derrame e/ou Vazamento de Tubulação Hidráulica - Danos Por Água","status":false,"lmi":null},{"name":"Desmoronamento","status":false,"lmi":null},{"name":"Deterioração de Mercadorias em Ambientes Frigorificados","status":false,"lmi":null},{"name":"Equipamentos Cinematográficos","status":false,"lmi":null},{"name":"Equipamentos Eletrônicos Sem Roubo","status":false,"lmi":null},{"name":"Equipamentos Estacionários Sem Roubo - Demais","status":false,"lmi":null},{"name":"Equipamentos Estacionários - Arrendados ou Cedidos a Terceiros Sem Roubo","status":false,"lmi":null},{"name":"Equipamentos Móveis Sem Roubo - \"Mineração ou Obras","status":false,"lmi":null},{"name":"Equipamentos Móveis Sem Roubo - \"Demais","status":false,"lmi":null},{"name":"Especial para Hotéis - Incêndio","status":false,"lmi":null},{"name":"Especial para Hotéis - Roubo / Furto de Bens mediante Arrombamento de \"Bens de Hóspedes","status":false,"lmi":null},{"name":"Especial para Hotéis - Roubo / Furto de Valores mediante Arrombamento de \"Bens de Hóspedes","status":false,"lmi":null},{"name":"Extravasamento ou Derrame de Material em Estado de Fusão","status":false,"lmi":null},{"name":"Fidelidade","status":false,"lmi":null},{"name":"Gastos com Salvamento e Desentulho","status":false,"lmi":null},{"name":"Impacto de Veículos e Queda de Aeronaves.","status":false,"lmi":null},{"name":"Impacto de Veículos Terrestres","status":false,"lmi":null},{"name":"Instalação em Novo Local em decorrência da Cobertura Incêndio","status":false,"lmi":null},{"name":"Instalação em Novo Local em decorrência - Demais eventos","status":false,"lmi":null},{"name":"Moldes","status":false,"lmi":null},{"name":"Movimentação Interna de Mercadorias","status":false,"lmi":null},{"name":"Operação de Carga","status":false,"lmi":null},{"name":"Perda/Pagamento de Aluguel em decorrência de Incêndio","status":false,"lmi":null},{"name":"Perda/Pagamento de Aluguel em decorrência da cobertura Vendaval / Fumaça","status":false,"lmi":null},{"name":"Quebra de Máquinas","status":false,"lmi":null},{"name":"Quebra de Vidros","status":false,"lmi":null},{"name":"Queda de Aeronaves","status":false,"lmi":null},{"name":"Queimadas em Zonas Rurais","status":false,"lmi":null},{"name":"Responsabilidade Civil - Danos Morais","status":false,"lmi":null},{"name":"Responsabilidade Civil Estabelecimento de Ensino","status":false,"lmi":null},{"name":"Responsabilidade Civil Operaçoes","status":false,"lmi":null},{"name":"Responsabilidade Civil Operações","status":false,"lmi":null},{"name":"Responsabilidade Civil Concessionária - Sem Furto Simples e total do veículo.","status":false,"lmi":null},{"name":"Responsabilidade Civil Empregador","status":false,"lmi":null},{"name":"Responsabilidade Civil Guarda de Veículos de Terceiros - Cobertura Colisão","status":false,"lmi":null},{"name":"Responsabilidade Civil Guarda de Veículos de Terceiros - Cobertura Exclusiva de Incêndio e Roubo","status":false,"lmi":null},{"name":"Riscos Diversos Concessionária Sem Furto Simples Total dos veículos","status":false,"lmi":null},{"name":"Recomposição Registros e Documentos","status":false,"lmi":null},{"name":"Riscos de Engenharia - Instalação e Montagem","status":false,"lmi":null},{"name":"Riscos de Engenharia - Obras Civis em Construção","status":false,"lmi":null},{"name":"Roubo e/ou Furto Qualificado de Bens mediante arrombamento","status":false,"lmi":null},{"name":"Roubo e/ou Furto Qualificado de Valores mediante arrombamento - no Interior do Estabelecimento","status":false,"lmi":null},{"name":"Roubo e/ou Furto Qualificado de Valores - em Trânsito em Mãos de Portadores","status":false,"lmi":null},{"name":"Tumultos","status":false,"lmi":null},{"name":"Vazamento Acidental de Tanque - danos a tanques e tubulações","status":false,"lmi":null},{"name":"Valores para Despesas de Viagens","status":false,"lmi":null},{"name":"Vendaval","status":false,"lmi":null}];
    $scope.listAllPlaceFireProtection = [{"name":"Extintores","status":false},{"name":"Hidrante Interno","status":false},{"name":"Hidrante Externo","status":false},{"name":"Sprinklers","status":false},{"name":"Brigada Incêndio Pessoal Treinado","status":false},{"name":"Detectores Automáticos com Central de Alarme","status":false},{"name":"Bomba Móvel Mangotinhos","status":false},{"name":"Termometria","status":false},{"name":"Aeração Transilagem","status":false}];
    $scope.listAllPlaceWaterTank = [{"name":"Elevado","status":false,"value": null},{"name":"Nível do Solo Subterraneo","status":false,"value": null}];
    $scope.listAllPlaceTheftProtection = [{"name":"Controle de Acesso de Veículos","status":false},{"name":"Corta Pneus","status":false},{"name":"Cancelas","status":false},{"name":"Guaritas Blindadas","status":false},{"name":"Vigilância Armada 24 horas","status":false},{"name":"Vigilância 24 horas Desarmada","status":false},{"name":"Alarme","status":false},{"name":"Circuito de TV Interno","status":false},{"name":"Sensores","status":false},{"name":"Botão de Pânico","status":false},{"name":"Alarme concectado a Central de Segurança","status":false},{"name":"Proximidade de Posto Policial","status":false},{"name":"Controle de Acesso Pessoas","status":false}];
    $scope.newRiskyPlace = {
        address: {},
        listAllPlaceFireProtection: JSON.parse(JSON.stringify($scope.listAllPlaceFireProtection)),
        listAllPlaceWaterTank: JSON.parse(JSON.stringify($scope.listAllPlaceWaterTank)),
        listAllPlaceTheftProtection: JSON.parse(JSON.stringify($scope.listAllPlaceTheftProtection))
    };

    function init() {
        if ($scope.hash) {
            $scope.isEdit = true;
            getQuotationData($scope.hash);
		}else{
            setPeriod();
            setInitialValues();
        }
		
		// Load pre data customer form
		if (!$scope.quotation.customer) {
			$scope.quotation.customer = {};
			$scope.quotation.customer.type = 'fisica';
			$scope.quotation.customer.genre = 'masculino';
			$scope.quotation.customer.rg_uf = 'SP';
		}
    }

    function getQuotationData(hash) {
        $scope.loading = true;
        QuotationService.get($scope.hash || hash).then(function(resp) {getQuotationData
            var quotationResp = resp.data;

            if (quotationResp && quotationResp.extra) {
                var receivedQuotation = JSON.parse(resp.data.extra);
                
                receivedQuotation.status = resp.data.status;
                receivedQuotation.hash = resp.data.hash;
                receivedQuotation.id = resp.data.id;
                receivedQuotation.message = resp.data.message;
                receivedQuotation.start_term = resp.data.start_term;
                receivedQuotation.end_term = resp.data.end_term;
                receivedQuotation.info = JSON.parse(resp.data.info);
                $scope.customerSnapshot = JSON.parse(resp.data.customer_snapshot);
                $scope.quotation = formatAfterGet(receivedQuotation);

                $scope.ocurrencesList = $scope.quotation.ocurrences;

                if ($scope.quotation.coverages && $scope.quotation.coverages.length) {
                    var markedCoveragesList = [];

                    $scope.quotation.coverages.forEach(function(coverage) {

                        if (coverage.status) {
                            markedCoveragesList.push(coverage);
                        }
                    });

                    if (markedCoveragesList.length) {
                        $scope.listAllCoverages.forEach(function(coverage) {
                            markedCoveragesList.forEach(function(markedCoverage) {
                                if (markedCoverage.name == coverage.name) {
                                    coverage.status = true;
                                    coverage.lmi = markedCoverage.lmi;
                                }
                            });
                        });
                    }
                }

                if ($scope.quotation.status == 'accepted') {
                    $scope.quotation.info.forEach(function(quote) {
                        if (quote.accept) {
                            $scope.acceptedQuote = quote;
                        }
                    });
                }
            }

            $scope.loading = false;
        }).catch(function(err) {
            console.log(err);
            $scope.loading = false;
            SweetAlert.error('Falha ao consultar cotação', 'Verifique se esta cotação existe no menu [Cotações]');
        });
    }

    function formatAfterGet(quotationToFormat) {
        quotationToFormat.start_term = moment(quotationToFormat.start_term, 'YYYY-MM-DD').format('DD/MM/YYYY');
        quotationToFormat.end_term = moment(quotationToFormat.end_term, 'YYYY-MM-DD').format('DD/MM/YYYY');
        
        return quotationToFormat; 
    }

    function updateDraft() {
        $scope.loading = true;
        QuotationService.updateGeneric({ 
            quotation_id: $scope.quotation.id,
            extra: JSON.stringify($scope.quotation),
            customer_snapshot: JSON.stringify($scope.customerSnapshot) || null,
            segment: 'empresarial',
            customer: $scope.customerId || null,
            start_term: moment($scope.quotation.start_term, 'DD/MM/YYYY').format('YYYY-MM-DD') || null,
            end_term:  moment($scope.quotation.end_term, 'DD/MM/YYYY').format('YYYY-MM-DD') || null,
        })
        .then(function(resp) {
            getQuotationData($scope.hash);
        }).catch(function(err) {
            $scope.loading = false;
        });
    }


    function saveDraft() {
        $scope.loading = true;
        QuotationService.save({
            quotation_info: $scope.quotation,
            customer_snapshot: JSON.stringify($scope.customerSnapshot) || null,
            segment: 'empresarial',
            status: 'draft',
            customer_id: $scope.customerId || null,
            start_term: moment($scope.quotation.start_term, 'DD/MM/YYYY').format('YYYY-MM-DD') || null,
            end_term:  moment($scope.quotation.end_term, 'DD/MM/YYYY').format('YYYY-MM-DD') || null,
        }).then(function(resp) {
            $scope.loading = false;
            $location.path('/cotacao-empresarial/' + resp.data.hash);
        }).catch(function(err) {
            $scope.loading = false;
            SweetAlert.error('Ops!', 'Ocorreu um erro inesperado, tente novamente, se o erro persistir, entre em contato com o suporte');
        });
    }

    function sendSolicitation() {
        SweetAlert.swal({
            title: 'Enviar solicitação?',
            text: 'Deseja realmente enviar a solicitação para cotação?',
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Voltar',
            confirmButtonColor: '#0E52AC'
         }, function(isConfirm) {
             if (isConfirm) {
                $scope.loading = true;

                QuotationService.updateGeneric({ 
                    quotation_id: $scope.quotation.id,
                    status: 'pre-quotation',
                    info: JSON.stringify($scope.quotation.info)
                })
                .then(function(resp) {
                    getQuotationData($scope.hash);
                    
                    // Send email
                    MetaService.solicitationRequestEmail({
                        customer_name: $rootScope.loggedUser.name,
                        company_name: $rootScope.loggedUser.company.name,
                        segment: 'Empresarial',
                        hash: $scope.quotation.hash
                    }).then(function(resp) {
                        $scope.loading = false;
                        console.log(resp.data);
                    });
                }).catch(function(err) {
                    $scope.loading = false;
                });
             }
         });
    }

    function capitalize(word) {
        if (!word) {
            return;    
        }
        
        return word.charAt(0).toUpperCase() + word.slice(1);
    }
	
	function onSelectCustomer(customer) {
		$scope.customerSnapshot = customer;
		$scope.customerId = customer.id;
		$scope.chooseCustomer = false;
	}

    function cancel() {
        SweetAlert.swal({
            title: "Cancelar a cotação?",
            text: "Uma vez cancelada, não é possível voltar atrás, tem certeza?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Cancelar Cotação",
            cancelButtonText: "Voltar",
            confirmButtonColor: "#ce1a1a"
         }, function(isConfirm) {
             if (isConfirm) {
                QuotationService.updateStatus({ 
                    quotation_id: $scope.quotation.id,
                    status: 'canceled' })
                .then(function(resp) {
                    getQuotationData($scope.hash);
                });
             }
         });
    }

    function closeModal() {
		$('#customerModal').modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
    }

    function validateCustomer(customer) {
		var missingFields = [];
		var fields = { 
			'name': 'Nome',
			'cpf': 'CPF',
			'birthdate': 'Data de Nascimento',
			'rg_number': 'RG',
			'rg_org': 'Rg Orgão Expeditor',
			'rg_uf': 'RG UF',
			'rg_emission_date': 'RG Data de Emissão',
			'cnpj': 'CNPJ',
			'email': 'Email',
			'phone': 'Telefone',
			'cep': 'CEP',
			'state': 'Estado',
			'city': 'Cidade',
			'address': 'Endereço',
			'number': 'Número' };

		Object.keys(fields).forEach(function(key) {
			if (!customer[key]) {
				if (key === 'cnpj' && customer.type === 'fisica') {
					return;
				}

				if (key.includes('rg') && customer.type === 'juridica') {
					return;
				}

				if (key === 'birthdate' && customer.type === 'juridica') {
					return;
				}

				if (key === 'genre' && customer.type === 'juridica') {
					return;
				}

				if (key === 'cpf' && customer.type === 'juridica') {
					return;
				}

				missingFields.push(fields[key]);
			}
		});

		missingFields = missingFields.join(', ');

		if (missingFields.length) {
			SweetAlert.warning('Preencha todos os campos obrigatórios', missingFields.toString());
			return false;
		} else {
			return customer;
		}
    }
    
    function getAddressByCep(cep) {
        if(cep && cep.length > 0){
            $scope.cepLoading = true;
            CustomerService.getAdressByCep(cep).then(function(resp) {
                var response = resp.data;

                $scope.quotation.customer.city = response.city;
                $scope.quotation.customer.address = response.street + ' - ' + response.neighborhood;
                $scope.quotation.customer.cep = response.cep;
                $scope.quotation.customer.state = response.state;

                if (!response.street || !response.neighborhood) {
                    $scope.quotation.customer.address.street = '';
                }

                $scope.cepLoading = false;
            }).catch(function(err) {
                $scope.cepLoading = false;
                SweetAlert.warning("Ops! Não encontramos o CEP digitado", 'Utilizamos 2 serviços para busca de CEP e não encontramos, verifique se você realmente digitou corretamente ou complete manualmente o endereço')
            });
        }
    }
    
    function saveCustomer() {
		var customer = JSON.parse(JSON.stringify($scope.quotation.customer));
		var validatedCustomer = validateCustomer(customer);

		if (!validatedCustomer) {
			return;
		}

		$scope.loadingCustomer = true;
		CustomerService.save({ customer: customer }).then(function(resp) {
            $scope.customerSnapshot = resp.data;
            $scope.quotation.customerId = resp.data.id;
            $scope.loadingCustomer = false;
            resetCustomer();
            closeModal();
		}).catch(function(err) {
			SweetAlert.info('Cliente já existe', 'Um cliente com este EMAIL ou CPF/CNPJ, já existe, verifique por favor.');
			$scope.loadingCustomer = false;
		});
    }

    function resetCustomer() {
        $scope.quotation.customer = {};
        $scope.quotation.customer.type = 'fisica';
        $scope.quotation.customer.genre = 'masculino';
        $scope.quotation.customer.rg_uf = 'SP';
    }
    
    function getAddressByCep(cep) {
        if (cep && cep.length > 0) {
            $scope.cepLoading = true;
            CustomerService.getAdressByCep(cep).then(function(resp) {
                var response = resp.data;

                $scope.quotation.customer.city = response.city;
                $scope.quotation.customer.address = response.street + ' - ' + response.neighborhood;
                $scope.quotation.customer.cep = response.cep;
                $scope.quotation.customer.state = response.state;

                if (!response.street || !response.neighborhood) {
                    $scope.quotation.customer.address.street = '';
                }

                $scope.cepLoading = false;
            }).catch(function(err) {
                $scope.cepLoading = false;
                SweetAlert.warning("Ops! Não encontramos o CEP digitado", 'Utilizamos 2 serviços para busca de CEP e não encontramos, verifique se você realmente digitou corretamente ou complete manualmente o endereço')
            });
        }
    }

    function getAddressByCepFromRiskyPlace() {
        var cep = $scope.newRiskyPlace.address.cep;
        
        if (cep && cep.length > 0) {
            $scope.cepLoading = true;
            CustomerService.getAdressByCep(cep).then(function(resp) {
                var response = resp.data;

                $scope.newRiskyPlace.address.city = response.city;
                $scope.newRiskyPlace.address.street = response.street;
                $scope.newRiskyPlace.address.neighborhood = response.neighborhood;
                $scope.newRiskyPlace.address.cep = response.cep;
                $scope.newRiskyPlace.address.state = response.state;

                $scope.cepLoading = false;
            }).catch(function(err) {
                $scope.cepLoading = false;
                SweetAlert.warning("Ops! Não encontramos o CEP digitado", 'Utilizamos 2 serviços para busca de CEP e não encontramos, verifique se você realmente digitou corretamente ou complete manualmente o endereço')
            });
        }
    }
    
    function enableCustomerSearch() {
		$scope.chooseCustomer = true;
		$scope.searchCustomer.value = null;
	}

	function disableCustomerSearch() {
		$scope.searchCustomer.value = '';
		$scope.chooseCustomer = false;
	}

	function getCustomers(typedTerm) {
		return CustomerService.suggest(typedTerm).then(function(resp) {
			return resp.data.map(function(item) {
				return item;
			});
		});
	};

	function editCustomerData() {
		$window.location.href = '/cliente-cadastrar/' +  $base64.encode($scope.quotation.customer.id);
	}

    function setPeriod() {
        $scope.quotation.start_term = moment().format('DD/MM/YYYY');
        $scope.quotation.end_term = moment().add(1, 'year').format('DD/MM/YYYY');
    }

    function setInitialValues() {

    }

    function openOcurrenceForm() {
        $scope.ocurrencesList =  [{}];
        $scope.showOcurrenceForm = true;
    }

    function closeOcurrenceForm() {
        $scope.ocurrencesList =  [];
        $scope.showOcurrenceForm = false;
    }

    function addOcurrence() {
        $scope.ocurrencesList.push({});
    }

    function saveNewOcurrence() {
        $scope.quotation.ocurrences = $scope.quotation.ocurrences.concat($scope.ocurrencesList);
        $scope.closeOcurrenceForm();
    }

    function removeOcurrence(index) {
        $scope.quotation.ocurrences.splice(index, 1);
    }

    function saveCoveragesList() {
        $('#coveragesModel').modal('hide');
        
        $scope.quotation.coverages = [];
        $scope.listAllCoverages.forEach(function(coverage) {
            if(coverage.status == true){
                $scope.quotation.coverages.push(coverage);
            }
        });
    }

    function changeStatusCoverage(coverage) {
        if(coverage.status == false){
            coverage.lmi = null;
        }
    }

    function openModalRiskyPlaces() {
        $scope.newRiskyPlace = {
            address: {},
            listAllPlaceFireProtection: JSON.parse(JSON.stringify($scope.listAllPlaceFireProtection)),
            listAllPlaceWaterTank: JSON.parse(JSON.stringify($scope.listAllPlaceWaterTank)),
            listAllPlaceTheftProtection: JSON.parse(JSON.stringify($scope.listAllPlaceTheftProtection))
        };

        $('#riskyPlacesModal').modal('show');
    }

    function addNewRiskyPlace() {
        if($scope.newRiskyPlace.index != null){
            //edit
            $scope.quotation.risky_places[$scope.newRiskyPlace.index] = JSON.parse(JSON.stringify($scope.newRiskyPlace));
        }else{
            //save new
            $scope.newRiskyPlace.index = $scope.quotation.risky_places.length;
            $scope.quotation.risky_places.push($scope.newRiskyPlace);
        }
        $('#riskyPlacesModal').modal('hide');
    }

    function editRiskyPlace(placeToEdit) {
        $scope.newRiskyPlace = JSON.parse(JSON.stringify(placeToEdit));
        $('#riskyPlacesModal').modal('show');
    }

    function removeRiskyPlace(index) {
        $scope.quotation.risky_places.splice(index, 1);
    }

    function clearRenovationValues() {
        if ($scope.quotation.renovation == 'novo') {
            $scope.quotation.renovation_previous_bonus = null;
            $scope.quotation.renovation_future_bonus = null;
            $scope.quotation.renovation_occurrences = null;
            $scope.quotation.renovation_final_policy_date = null;
            $scope.quotation.renovation_current_policy_date = null;
            $scope.quotation.renovation_current_ci = null;
        }
    }

    function triggerUpload(label) {
        $scope.currentImgLabel = label;
        document.getElementById('imageUpload').click();
    };

    function uploadFiles(files, errFiles) {
        $scope.uploadingFiles = files;
        $scope.errFiles = errFiles;
        $scope.finished = 0;
        $scope.hideProgressBars = false;

        angular.forEach(files, function(file, index) {
            file.upload = Upload.upload({
                url: config.uploadEndpoint,
                data: { file: file,'folder': 'docs' },
            });

            file.upload.then(function (response) {
                $scope.quotation.files.push({
                    url: response.data[0],
                    label: 'Arquivo ' + ($scope.quotation.files.length + 1)
                });
                
                $scope.finished++;

                if ($scope.finished == files.length) {
                    $scope.hideProgressBars = true;
                }
            }, function (response) {
                console.log(response.status + ': ' + response.data);
                SweetAlert.warning('Ocorreu um erro com um dos arquivos', 'Se o problema persistir, tente usar outro arquivo ou informe ao suporte.');
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        });
    }

    function removeDoc(url) {
        if (!$scope.quotation.files.length) {
            return;
        }

        $scope.quotation.files = $scope.quotation.files.filter(function(doc) {
            return url != doc.url;
        });
    }

    function accept(quote) {
        SweetAlert.swal({
            title: 'Aceitar esta cotação?',
            text: 'Selecionando esta cotação, você aceita prosseguir com o fechamento do seguro no valor e observações descritas.',
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Aceitar',
            cancelButtonText: 'Voltar',
            confirmButtonColor: '#0E52AC'
         }, function(isConfirm) {
             if (isConfirm) {
                $scope.loading = true;
                quote.accept = true;

                QuotationService.updateGeneric({ 
                    quotation_id: $scope.quotation.id,
                    status: 'accepted',
                    info: JSON.stringify($scope.quotation.info),
                    price: quote.price
                })
                .then(function(resp) {
                    MetaService.quotationChosenEmail({
                        customer_name: $rootScope.loggedUser.name,
                        company_name: $rootScope.loggedUser.company.name,
                        segment: 'Empresarial',
                        hash: $scope.quotation.hash
                    }).then(function(resp) {
                        getQuotationData($scope.hash);
                        console.log(resp.data);
                    });
                }).catch(function(err) {
                    $scope.loading = false;
                });
             }
         });
    }

	init();
    $scope.accept = accept;
    $scope.updateDraft = updateDraft;
    $scope.sendSolicitation = sendSolicitation;
    $scope.capitalize = capitalize;
    $scope.saveDraft = saveDraft;
    $scope.cancel = cancel;
    $scope.removeDoc = removeDoc;
    $scope.triggerUpload = triggerUpload;
    $scope.uploadFiles = uploadFiles;
    $scope.removeOcurrence = removeOcurrence;
    $scope.saveNewOcurrence = saveNewOcurrence;
    $scope.addOcurrence = addOcurrence;
    $scope.openOcurrenceForm = openOcurrenceForm;
    $scope.closeOcurrenceForm = closeOcurrenceForm;
    $scope.clearRenovationValues = clearRenovationValues;
    $scope.editCustomerData = editCustomerData;
	$scope.onSelectCustomer = onSelectCustomer;
	$scope.getCustomers = getCustomers;
	$scope.disableCustomerSearch = disableCustomerSearch;
    $scope.enableCustomerSearch = enableCustomerSearch;
    $scope.saveCustomer = saveCustomer;
    $scope.getAddressByCep = getAddressByCep;
    $scope.saveCoveragesList = saveCoveragesList;
    $scope.changeStatusCoverage = changeStatusCoverage;
    $scope.getAddressByCepFromRiskyPlace = getAddressByCepFromRiskyPlace;
    $scope.openModalRiskyPlaces = openModalRiskyPlaces;
    $scope.addNewRiskyPlace = addNewRiskyPlace;
    $scope.editRiskyPlace = editRiskyPlace;
    $scope.removeRiskyPlace = removeRiskyPlace;

});