module.exports = {
  attributes: {
    description: {
      type: 'string'
    },
		emails: {
			type: 'string'
    },
    queue: {
      model: 'queue',
      columnName: 'queue_id'
    },
  },
};

