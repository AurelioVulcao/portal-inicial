async function listById(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { message_id } = req.params;
    
    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'not authorized'
        });
        return;
    }

    try {
        const messages = await Message.findOne({ id: message_id, company: tokenData.company_id });
        res.status(200).json(messages);
    } catch(err) {
        console.log(err);
        res.status(500).json({ err: err });
    }
}

async function addMessage(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { message } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'not authorized'
        });
        return;
    }

    try {
        const updatedMessage = await Message.updateOne({ id: message.id }).set({ messages: message.messages });
        res.status(200).json(updatedMessage);
    } catch(err) {
        res.status(500).json({ err: err });
    }
}

module.exports = {
    listById,
    addMessage
}