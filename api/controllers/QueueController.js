async function save(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { quotation, info } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    const job = {
       description: info.description,
       slug: info.slug,
       info: JSON.stringify(quotation),
       quotation_hash: quotation.hash,
       quotation: quotation.id,
       status: 'waiting',
       user: tokenData.user_id,
       company: tokenData.company_id
    };

    try {
        const createdJob = await Queue.create(job).fetch();
        res.status(200).json(createdJob);
        return;
    } catch(err) {
        res.status(500).json({
            err: err
        });
    }
}

module.exports = {
    save
}