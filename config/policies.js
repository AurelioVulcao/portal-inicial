module.exports.policies = {
  '*': 'token',
  UserController: {
    'authenticate': true,
    'socialAuthentication': true,
    'linkedinCallback': true
  },
  MetaController: {
    'externalInformativeEmail': true,
    'externalEmail': true,
    'callBackRDStation': true
  },
  QuotationController: {
    'autoReturnCallback': true,
    'onConnect': true
  },
  JobHandlerController: {
    'handle': true,
    'errorFinder': true,
    'clearBlacklistTokens': true
  }
};
