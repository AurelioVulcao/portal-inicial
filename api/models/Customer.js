module.exports = {
  attributes: {
		name: {
			type: 'string'
		},
		type: {
      type: 'string'
    },
    cpf: {
      type: 'string'
    },
    cnpj: {
      type: 'string'
    },
		birthdate: {
      type: 'string'
    },
    rne_number: {
      type: 'string'
		},
		rg_number: {
      type: 'string'
		},
		rg_org: {
      type: 'string'
    },
    rg_uf: {
      type: 'string'
    },
    rg_emission_date: {
      type: 'string'
    },
    genre: {
      type: 'string'
    },
    email: {
      type: 'string'
    },
    phone: {
      type: 'string'
    },
    cep: {
      type: 'string'
    },
    state: {
      type: 'string'
    },
    city: {
      type: 'string'
    },
    address: {
      type: 'string'
    },
    number: {
      type: 'string'
    },
    complement: {
      type: 'string'
    },
    company: {
      model: 'company',
      columnName: 'company_id'
    },
	},
};

