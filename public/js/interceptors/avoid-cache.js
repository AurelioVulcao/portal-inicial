angular.module('vencorr').factory('PreventTemplateCache', function() {
	return {
	'request': function(config) {
		if (config.url.indexOf('views') !== -1) {
			config.url = config.url + '?t=' + Date.now();
		}
			return config;
		}
	}
});