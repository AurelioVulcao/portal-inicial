angular.module('vencorr').controller('CustomerFormController', function($scope, CustomerService, SweetAlert, UtilService, $location, $base64, $routeParams, $window) {
    $scope.loading = false;
    $scope.isEdit = false;
    $scope.missingFields = [];

    function init() {
        if ($routeParams.id) {
            $scope.customerId = $base64.decode($routeParams.id);
            $scope.isEdit = true;
            getCustomer($scope.customerId);
        }

        $scope.customer = {};
        $scope.customer.type = 'fisica';
        $scope.customer.genre = 'masculino';
        $scope.customer.rg_uf = 'SP';
        $scope.customer.state = 'SP';
        $scope.customer.rg_org = 'SDS';
    }

    function getCustomer(customerId) {
        if (!customerId) {
            return;
        }

        $scope.loading = true;
		CustomerService.getById(customerId).then(function(resp) {
            $scope.loading = false;
            $scope.customer = resp.data;
		}).catch(function(err) {
            $scope.loading = false;
        });
    }

    function save() {
		var customer = JSON.parse(JSON.stringify($scope.customer));
		var isValid = validateCustomer(customer);

		if (!isValid) {
			return;
        }
        
		$scope.loading = true;
		CustomerService.save({ customer: customer }).then(function(resp) {
            $scope.loading = false;
            $location.path('/clientes');
		}).catch(function(err) {
            if (err.data === 'customer already exist') {
                SweetAlert.info('Cliente já existe', 'Um cliente com este EMAIL ou CPF/CNPJ, já existe, verifique por favor.');
            } else {
                SweetAlert.error("Ops! Um erro inesperado aconteceu", 'Tente novamente, se o erro persistir entre em contato com o suporte, desculpe o transtorno.');
            }

            $scope.loading = false;
        });
    }

    function getAddressByCep(cep) {
		CustomerService.getAdressByCep(cep).then(function(resp) {
			var response = resp.data;

			$scope.customer.city = response.city;
			$scope.customer.address = response.street + ' - ' + response.neighborhood;
			$scope.customer.cep = response.cep;
			$scope.customer.state = response.state;

			if (!response.street || !response.neighborhood) {
				$scope.customer.address.street = '';
			}
		}).catch(function(err) {
			SweetAlert.warning("Ops! Não encontramos o CEP digitado", 'Utilizamos 2 serviços para busca de CEP e não encontramos, verifique se você realmente digitou corretamente ou complete manualmente o endereço')
		});
    }

    function validateCustomer(customer) {
        var testCustomer = UtilService.clone(customer);
        var requiredFields = [
			'name',
			'cpf',
			'birthdate',
			'rg_number',
			'rg_org',
			'rg_uf',
			'rg_emission_date',
			'cnpj',
			'email',
			'phone',
			'cep',
			'state',
			'city',
			'address',
			'number']
        var missingFields = [];
        requiredFields.forEach(function(key) {
            if (!testCustomer[key]) {
                if (key === 'cnpj' && customer.type === 'fisica') {
                    return;
                }
    
                if (key.includes('rg') && customer.type === 'juridica') {
                    return;
                }
    
                if (key === 'birthdate' && customer.type === 'juridica') {
                    return;
                }
    
                if (key === 'genre' && customer.type === 'juridica') {
                    return;
                }
    
                if (key === 'cpf' && customer.type === 'juridica') {
                    return;
                }

                missingFields.push(key);
            }
        });

        $scope.missingFields = missingFields.slice();
        if (missingFields.length) {
            return false;
        }

        return true;
    }
    
    function isMissingField(key) {
        return $scope.missingFields.indexOf(key) !== -1 ? true : false;
    }

    function update() {
        $scope.loading = true;
		CustomerService.update({
            customer: $scope.customer
        }).then(function(resp) {
            $scope.loading = false;
            $scope.customer = resp.data;
            $window.location.href ='/cliente/' + $routeParams.id;
		}).catch(function(err) {
            if (err.data === 'customer already exist') {
                SweetAlert.info('Cliente já existe', 'Um cliente com este EMAIL ou CPF/CNPJ, já existe, verifique por favor.');
            } else {
                SweetAlert.error("Ops! Um erro inesperado aconteceu", 'Tente novamente, se o erro persistir entre em contato com o suporte, desculpe o transtorno.');
            }
            
            $scope.loading = false;
        });
    }
    
    init();
    $scope.getAddressByCep = getAddressByCep;
    $scope.isMissingField = isMissingField;
    $scope.update = update;
    $scope.save = save;
});