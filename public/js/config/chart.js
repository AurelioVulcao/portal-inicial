angular.module('vencorr').config(function(ChartJsProvider) {
    ChartJsProvider.setOptions({
        chartColors : ['#FDB45C','#46BFBD'],
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                    beginAtZero: true,   // minimum value will be 0.
                    stepSize: 20,
                }
            }]
        }
    });
});
