angular.module('vencorr').controller('QuotationGenericController', function($scope, $rootScope, $routeParams, $location, QuotationService, SweetAlert, CustomerService, Upload, config, MetaService) {
	$scope.loading = false;
    $scope.isEdit = false;
    $scope.hash = $routeParams.hash;
    $scope.customerSnapshot = {};
    $scope.loadingCustomer = false;
    $scope.searchCustomer = {};
    $scope.customerId;
    $scope.chooseCustomer = false;
	$scope.cepLoading = false;
	$scope.quotationMissingFields = [];
    $scope.quotation = {
		files: [],
	};

    function init() {
        if ($scope.hash) {
            $scope.isEdit = true;
            getQuotationData($scope.hash);
		} else{
            setPeriod();
        }
		
		// Load pre data customer form
		if (!$scope.quotation.customer) {
			$scope.quotation.customer = {};
			$scope.quotation.customer.type = 'fisica';
			$scope.quotation.customer.genre = 'masculino';
			$scope.quotation.customer.rg_uf = 'SP';
		}
	}

	function getQuotationData() {
        $scope.loading = true;
        QuotationService.get($scope.hash).then(function(resp) {
            var quotationResp = resp.data;

            if (quotationResp && quotationResp.extra) {
                var receivedQuotation = JSON.parse(resp.data.extra);
                
                receivedQuotation.status = resp.data.status;
                receivedQuotation.hash = resp.data.hash;
                receivedQuotation.obs = receivedQuotation.obs;
                receivedQuotation.id = resp.data.id;
                receivedQuotation.message = resp.data.message;
                receivedQuotation.start_term = resp.data.start_term;
                receivedQuotation.end_term = resp.data.end_term;
                receivedQuotation.info = JSON.parse(resp.data.info);
                $scope.customerSnapshot = JSON.parse(resp.data.customer_snapshot);
                $scope.quotation = formatAfterGet(receivedQuotation);

                if ($scope.quotation.status == 'accepted') {
                    $scope.quotation.info.forEach(function(quote) {
                        if (quote.accept) {
                            $scope.acceptedQuote = quote;
                        }
                    });
                }
            }

            $scope.loading = false;
        }).catch(function(err) {
            console.log(err);
            $scope.loading = false;
            SweetAlert.error('Falha ao consultar cotação', 'Verifique se esta cotação existe no menu [Cotações]');
        });
    }

    function formatAfterGet(quotationToFormat) {
        quotationToFormat.start_term = moment(quotationToFormat.start_term, 'YYYY-MM-DD').format('DD/MM/YYYY');
        quotationToFormat.end_term = moment(quotationToFormat.end_term, 'YYYY-MM-DD').format('DD/MM/YYYY');
        
        return quotationToFormat; 
    }
	
	function setPeriod() {
        $scope.quotation.start_term = moment().format('DD/MM/YYYY');
        $scope.quotation.end_term = moment().add(1, 'year').format('DD/MM/YYYY');
    }
	
	function onSelectCustomer(customer) {
		$scope.customerSnapshot = customer;
		$scope.quotation.customerId = customer.id;
		$scope.chooseCustomer = false;
    }

    function closeModal() {
		$('#customerModal').modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
    }
    
    function validateCustomer(customer) {
		var missingFields = [];
		var fields = { 
			'name': 'Nome',
			'cpf': 'CPF',
			'birthdate': 'Data de Nascimento',
			'rg_number': 'RG',
			'rg_org': 'Rg Orgão Expeditor',
			'rg_uf': 'RG UF',
			'rg_emission_date': 'RG Data de Emissão',
			'cnpj': 'CNPJ',
			'email': 'Email',
			'phone': 'Telefone',
			'cep': 'CEP',
			'state': 'Estado',
			'city': 'Cidade',
			'address': 'Endereço',
			'number': 'Número' };

		Object.keys(fields).forEach(function(key) {
			if (!customer[key]) {
				if (key === 'cnpj' && customer.type === 'fisica') {
					return;
				}

				if (key.includes('rg') && customer.type === 'juridica') {
					return;
				}

				if (key === 'birthdate' && customer.type === 'juridica') {
					return;
				}

				if (key === 'genre' && customer.type === 'juridica') {
					return;
				}

				if (key === 'cpf' && customer.type === 'juridica') {
					return;
				}

				missingFields.push(fields[key]);
			}
		});

		missingFields = missingFields.join(', ');

		if (missingFields.length) {
			SweetAlert.warning("Preencha todos os campos obrigatórios", missingFields.toString());
			return false;
		} else {
			return customer;
		}
    }
    
    function saveCustomer() {
		var customer = JSON.parse(JSON.stringify($scope.quotation.customer));
		var validatedCustomer = validateCustomer(customer);

		if (!validatedCustomer) {
			return;
		}

		$scope.loadingCustomer = true;
		CustomerService.save({ customer: customer }).then(function(resp) {
            $scope.customerSnapshot = resp.data;
            $scope.quotation.customerId = resp.data.id;
            $scope.loadingCustomer = false;
            resetCustomer();
            closeModal();
		}).catch(function(err) {
			SweetAlert.info('Cliente já existe', 'Um cliente com este EMAIL ou CPF/CNPJ, já existe, verifique por favor.');
			$scope.loadingCustomer = false;
		});
    }

    function resetCustomer() {
        $scope.quotation.customer = {};
        $scope.quotation.customer.type = 'fisica';
        $scope.quotation.customer.genre = 'masculino';
        $scope.quotation.customer.rg_uf = 'SP';
    }
    
    function getAddressByCep(cep) {
		$scope.cepLoading = true;
		CustomerService.getAdressByCep(cep).then(function(resp) {
			var response = resp.data;

			$scope.quotation.customer.city = response.city;
			$scope.quotation.customer.address = response.street + ' - ' + response.neighborhood;
			$scope.quotation.customer.cep = response.cep;
			$scope.quotation.customer.state = response.state;

			if (!response.street || !response.neighborhood) {
				$scope.quotation.customer.address.street = '';
			}

			$scope.cepLoading = false;
		}).catch(function(err) {
			$scope.cepLoading = false;
			SweetAlert.warning("Ops! Não encontramos o CEP digitado", 'Utilizamos 2 serviços para busca de CEP e não encontramos, verifique se você realmente digitou corretamente ou complete manualmente o endereço')
		});
    }
    
    function enableCustomerSearch() {
		$scope.chooseCustomer = true;
		$scope.searchCustomer.value = null;
	}

	function disableCustomerSearch() {
		$scope.searchCustomer.value = '';
		$scope.chooseCustomer = false;
	}

	function getCustomers(typedTerm) {
		return CustomerService.suggest(typedTerm).then(function(resp) {
			return resp.data.map(function(item) {
				return item;
			});
		});
	};

	function editCustomerData() {
		$window.location.href = '/cliente-cadastrar/' +  $base64.encode($scope.quotation.customer.id);
	}

	function triggerUpload(label) {
        $scope.currentImgLabel = label;
        document.getElementById('imageUpload').click();
    };

    function uploadFiles(files, errFiles) {
        $scope.uploadingFiles = files;
        $scope.errFiles = errFiles;
        $scope.finished = 0;
        $scope.hideProgressBars = false;

        angular.forEach(files, function(file, index) {
            file.upload = Upload.upload({
                url: config.uploadEndpoint,
                data: { file: file,'folder': 'docs' },
            });

            file.upload.then(function (response) {
                $scope.quotation.files.push({
                    url: response.data[0],
                    label: 'Arquivo ' + ($scope.quotation.files.length + 1)
                });
                
                $scope.finished++;

                if ($scope.finished == files.length) {
                    $scope.hideProgressBars = true;
                }
            }, function (response) {
                console.log(response.status + ': ' + response.data);
                SweetAlert.warning('Ocorreu um erro com um dos arquivos', 'Se o problema persistir, tente usar outro arquivo ou informe ao suporte.');
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        });
    }

    function removeDoc(url) {
        if (!$scope.quotation.files.length) {
            return;
        }

        $scope.quotation.files = $scope.quotation.files.filter(function(doc) {
            return url != doc.url;
        });
	}
	
	function cancel() {
        SweetAlert.swal({
            title: 'Cancelar a cotação?',
            text: 'Uma vez cancelada, não é possível voltar atrás, tem certeza?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Cancelar Cotação',
            cancelButtonText: 'Voltar',
            confirmButtonColor: '#ce1a1a'
         }, function(isConfirm) {
             if (isConfirm) {
                $scope.loading = true;
                QuotationService.updateStatus({ 
                    quotation_id: $scope.quotation.id,
                    status: 'canceled' })
                .then(function(resp) {
                    getQuotationData($scope.hash);
                }).catch(function(err) {
                    $scope.loading = false;
                });
             }
         });
    }

    function accept(quote) {
        SweetAlert.swal({
            title: 'Aceitar esta cotação?',
            text: 'Selecionando esta cotação, você aceita prosseguir com o fechamento do seguro no valor e observações descritas.',
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Aceitar',
            cancelButtonText: 'Voltar',
            confirmButtonColor: '#0E52AC'
         }, function(isConfirm) {
             if (isConfirm) {
                $scope.loading = true;
                quote.accept = true;

                QuotationService.updateGeneric({ 
                    quotation_id: $scope.quotation.id,
                    status: 'accepted',
                    info: JSON.stringify($scope.quotation.info),
                    price: quote.price
                })
                .then(function(resp) {
                    MetaService.quotationChosenEmail({
                        customer_name: $rootScope.loggedUser.name,
                        company_name: $rootScope.loggedUser.company.name,
                        segment: 'Residencial',
                        hash: $scope.quotation.hash
                    }).then(function(resp) {
                        getQuotationData($scope.hash);
                        console.log(resp.data);
                    });
                }).catch(function(err) {
                    $scope.loading = false;
                });
             }
         });
	}

	function save(quotation) {
		var isValidQuotation = validateQuotation(quotation);

        if (!isValidQuotation) {
            return;
		}

        $scope.loading = true;
        QuotationService.save({
            quotation_info: $scope.quotation,
            customer_snapshot: JSON.stringify($scope.customerSnapshot),
            segment: 'outras',
            customer_id: $scope.quotation.customerId,
            start_term: moment($scope.quotation.start_term, 'DD/MM/YYYY').format('YYYY-MM-DD'),
            end_term:  moment($scope.quotation.end_term, 'DD/MM/YYYY').format('YYYY-MM-DD')
        }).then(function(resp){
            // Send email
            MetaService.solicitationRequestEmail({
                customer_name: $rootScope.loggedUser.name,
                company_name: $rootScope.loggedUser.company.name,
                segment: 'Equipamento',
                hash: resp.data.hash
            }).then(function(resp) {
                $scope.loading = false;
                console.log(resp.data);
            });

            $location.path('/cotacoes');
        }).catch(function(err) {
            console.log(err);
            $scope.loading = false;
            SweetAlert.error('Ops!', 'Ocorreu um erro inesperado, tente novamente, se o erro persistir, entre em contato com o suporte');
        });
    }

    function updateQuotation() {
        $scope.loading = true;
        
        QuotationService.updateGeneric({ 
            extra: JSON.stringify($scope.quotation),
            quotation_id: $scope.quotation.id,
            customer_snapshot: JSON.stringify($scope.customerSnapshot) || null,
            customer: $scope.customerSnapshot.id || null,
            start_term: moment($scope.quotation.start_term, 'DD/MM/YYYY').format('YYYY-MM-DD') || null,
            end_term:  moment($scope.quotation.end_term, 'DD/MM/YYYY').format('YYYY-MM-DD') || null,
        })
        .then(function(resp) {
            getQuotationData($scope.hash);
        }).catch(function(err) {
            $scope.loading = false;
        });
    }
	
	function capitalize(word) {
        if (!word) {
            return;    
        }
        
        return word.charAt(0).toUpperCase() + word.slice(1);
	}
	
	function validateQuotation(quotation) {
        var missingFields = [];
        var fields = [
            'start_term',
            'end_term',
			'customerId',
			'description'
        ];

        fields.forEach(function(key) {
            if (!$scope.quotation[key]) {
                missingFields.push(key);
            }
        });

        if (missingFields.length) {
            $scope.quotationMissingFields = missingFields;
            SweetAlert.warning('Preencha todos os campos obrigatórios', 'Estão maracados com um *');
            return false;
        } else {
            return true;
        }
	}
	
	function isMissingFields(key) {
        return $scope.quotationMissingFields.indexOf(key) !== -1 ? true : false;
    }

    init();
    $scope.updateQuotation = updateQuotation;
	$scope.save = save;
	$scope.isMissingFields = isMissingFields;
	$scope.getQuotationData = getQuotationData;
	$scope.capitalize = capitalize;
	$scope.triggerUpload = triggerUpload;
	$scope.uploadFiles = uploadFiles;
	$scope.removeDoc = removeDoc;
	$scope.accept = accept;
    $scope.cancel = cancel;
    $scope.editCustomerData = editCustomerData;
	$scope.onSelectCustomer = onSelectCustomer;
	$scope.getCustomers = getCustomers;
	$scope.disableCustomerSearch = disableCustomerSearch;
    $scope.enableCustomerSearch = enableCustomerSearch;
    $scope.saveCustomer = saveCustomer;
    $scope.getAddressByCep = getAddressByCep;
});