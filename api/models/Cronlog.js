module.exports = {
  attributes: {
    description: {
      type: 'string'
    },
    slug: {
      type: 'string'
    },
    info: {
      type: 'string'
    },
    network_info: {
      type: 'string'
    },
	},
};

