async function authenticate(req, res) {
    const { email, password } = req.body;

    if (!email || !password) {
		res.status(500).json({
			status: 'error',
			error: 'missing_credentials'
		});
		return;
    }
    
    const user = await User.findOne({ email: email, status: 'active' }).populate('company');

    if (!user || user.status === 'disabled'  || !user.id) {
        res.status(401).json({
            status: 'error',
            error: 'no permission'
        });
        return;
    }
    
    if (!EncryptionService.comparePassword(password, user.password)) {
        res.status(401).json({
            status: 'error',
            error: 'invalid_credentials'
        });
        return;
    } else {

        const foundTokens = await EncryptionService.findActiveTokens(user.id);

        if (foundTokens.length) {
            await EncryptionService.addTokenToBlacklist(foundTokens);
        }

        const token = EncryptionService.createToken(user, user.company);
        await EncryptionService.addTokenToDatabase(user.id, token);
        user.token = token;
        
        res.status(200).json(user);
    }
}

async function socialAuthentication(req, res) {
    const { email } = req.body;

    if (!email) {
		res.status(500).json({
			status: 'error',
			error: 'missing_credentials'
		});
		return;
    }

    const user = await User.findOne({ email: email, status: 'active' }).populate('company');

    if ((user && user.status === 'disabled') || !user  || !user.id) {
        res.status(401).json({
            status: 'error',
            error: 'no permission'
        });
        return;
    }

    const token = EncryptionService.createToken(user, user.company);
    user.token = token;

    res.status(200).json(user);
}

async function create(req, res) {
    const { user } = req.body;
    
    if (!user.company || !user.email) {
		res.status(400).json({
			status: 'error',
			error: 'missing_values'
		});

		return;
    }

    user.password = EncryptionService.hashPassword(user.password);
    
    const createdUser = await User.create(user).fetch();

    if (!createdUser) {
        res.status(500).json(500, {
            error: 'error on created new seller'
        });
        return;
    }

    res.status(200).json(200, createdUser);
}

async function linkedinCallback(req, res) {
    const { oauth2Code } = req.body;

    if (!oauth2Code) {
        res.status(401).json({
            error: 'oauth2 code missing'
        });
        return;
    }

    const axios = require('axios');
    const grantType = 'authorization_code';
    const code = oauth2Code;
    const redirectUri = 'http%3A%2F%2F167.71.106.140%2Fauth%2Flinkedin%2Fcallback'; // Produção
    const clientId = '786o5ie60uvoev';
    const clientSecret = '68UOZ92JaSjWrEzX';
    let url = 'https://www.linkedin.com/oauth/v2/accessToken?';
    let accessToken;

    url += 'grant_type=' + grantType;
    url += '&code=' + code;
    url += '&redirect_uri=' + redirectUri;
    url += '&client_id=' + clientId;
    url += '&client_secret=' + clientSecret;

    try {
        const accessTokenRequest = await axios({
            method: 'post',
            url: url,
        });

        if (accessTokenRequest && accessTokenRequest.data && accessTokenRequest.data.access_token) {
            accessToken = accessTokenRequest.data.access_token;
            
            const linkedinUserRequest = await axios({
                method: 'get',
                url: 'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))',
                headers: {'Authorization': 'Bearer ' + accessToken}
            });

            if (linkedinUserRequest.data) {
                const linkedinUserData = linkedinUserRequest.data.elements;
                const linkedinEmail = linkedinUserData[0]['handle~']['emailAddress']
                
                res.status(200).json({
                    email: linkedinEmail
                });
                return
            } else {
                res.status(400).json({
                    error: 'invalid_session'
                });
                return
            }

        } else {
            res.status(400).json({
                error: 'invalid_session'
            });
            return
        }
    } catch(err) {
        res.status(500).json({
            error: 'invalid_session'
        });
        return;
    }
}

module.exports = {  
    authenticate,
    socialAuthentication,
    create,
    linkedinCallback,
};

