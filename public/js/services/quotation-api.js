angular.module("vencorr").factory("QuotationService", function ($http, config) {
    function list(filter) {
        return $http.post(config.baseUrl + "/quotations", filter);
    }

    function get(hash) {
        return $http.get(config.baseUrl + "/quotation/" + hash);
    }

    function getById(id) {
        return $http.get(config.baseUrl + "/quotation/byid/" + id);
    }

    function save(data) {
        return $http.post(config.baseUrl + "/quotation", data);
    }

    function update(data) {
        return $http.put(config.baseUrl + "/quotation", data);
    }

    function updateGeneric(data) {
        return $http.put(config.baseUrl + "/quotation/update/generic", { quotation: data });
    }

    function updateGeneral(data) {
        return $http.put(config.baseUrl + "/quotation-update", data);
    }

    function updateInstallments(data) {
        return $http.put(config.baseUrl + "/installments", data);
    }

    function updateItemAdditionalInfo(data) {
        return $http.put(config.baseUrl + "/items", data);
    }

    function updateColaboratorCode(data) {
        return $http.put(config.baseUrl + "/colaborator-code", data);
    }

    function updateStatus(data) {
        return $http.put(config.baseUrl + "/quotation/status", data);
    }

    function updateTransmissionID(data) {
        return $http.put(config.baseUrl + "/quotation/transmission", data);
    }

    function getQuotationsByCustomer(customerId) {
        return $http.post(config.baseUrl + "/quotations/customer", { customer_id: customerId });
    }
    
    function updatePeriod(data) {
        return $http.put(config.baseUrl + "/quotation/update/period", data);
    }

    return {
        get: get,
        save: save,
        list: list,
        update: update,
        getById: getById,
        updateGeneric: updateGeneric,
        updatePeriod: updatePeriod,
        updateStatus: updateStatus,
        updateGeneral: updateGeneral,
        updateInstallments: updateInstallments,
        updateTransmissionID: updateTransmissionID,
        updateColaboratorCode: updateColaboratorCode,
        getQuotationsByCustomer: getQuotationsByCustomer,
        updateItemAdditionalInfo: updateItemAdditionalInfo
    };
});