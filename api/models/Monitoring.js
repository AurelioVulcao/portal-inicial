module.exports = {
	attributes: {
		resource: {
			type: 'string'
		},
		user: {
			model: 'user',
			columnName: 'user_id'
		},
		extra: {
			type: 'ref',
			columnType: 'text'
		}
	}
};
  
  