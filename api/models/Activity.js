module.exports = {
  attributes: {
		description: {
			type: 'string'
		},
    user: {
      model: 'user',
      columnName: 'user_id'
    },
    model: {
      type: 'string'
    },
    data: {
      type: 'ref',
      columnType: 'text'
    },
    company: {
      model: 'company',
      columnName: 'company_id'
    },
	},
};

