angular.module("vencorr").factory("ComissionService", function ($http, config) {
    function list(params) {
        return $http.post(config.baseUrl + "/comissions", params);
    }

    function confirm(comissionId) {
        return $http.put(config.baseUrl + "/comission", {comission_id : comissionId});
    }

    return {
        list: list,
        confirm: confirm
    };
});