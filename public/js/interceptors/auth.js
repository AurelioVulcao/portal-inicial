angular.module('vencorr').factory('AuthInterceptor', function($rootScope, StorageService, SweetAlert) {
	return {
		request: function(config) {
			if (StorageService.get('user')) {
				$rootScope.loggedUser = angular.fromJson(StorageService.get('user'));
				config.headers['Authorization'] = $rootScope.loggedUser.token;
				$rootScope.navbar = '../../views/navbar.html';
			}

			return config;
		},
		responseError: function(error) {
			if (error.status === 401 || error.status === 403) {
				if (error.data == 'Forbidden') {
					SweetAlert.swal({
						title: 'Você fez login em outro dispositivo e será desconectado deste atual',
						type: 'info',
						confirmButtonText: 'Ok',
						confirmButtonColor: '#0E52AC'
					 }, function(isConfirm) {
						 StorageService.clear();
						 window.location = '/login';
						 return;
					 });

				}
			}
		}
	}
});
