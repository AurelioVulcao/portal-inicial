angular.module('vencorr').factory('UtilService', function() {
    function formatMoney(amount) {
        var j;
        var n = amount;
        var c = 2;
        var d = ',';
        var t = '.';
        var s = n < 0 ? '-' : '';
        var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + '';
    
        j = (j = i.length) > 3 ? j % 3 : 0;
        return 'R$ ' + s + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
    }

    function validateEmail(email) {
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return regex.test(String(email).toLowerCase());
    }

    function clone(obj) {
        return JSON.parse(JSON.stringify(obj));
    }

    function extractExtension(filename) {
        return filename.substr(filename.lastIndexOf('.') + 1)
    }

    function formatSegmentName(segment) {
        if (segment === 'construcao') {
            return 'Construção';
        }

        if (segment === 'agro') {
            return 'Agro';
        }

        if (segment === 'auto') {
            return 'Auto';
        }

        if (segment === 'residencial') {
            return 'Residencial';
        }

        if (segment === 'empresarial') {
            return 'Empresarial';
        }

        if (segment === 'equipamento') {
            return 'Equipamen.';
        }

        if (segment === 'outras') {
            return 'Outras';
        }
    }

    return {
        clone: clone,
        formatMoney: formatMoney,
        validateEmail: validateEmail,
        extractExtension: extractExtension,
        formatSegmentName: formatSegmentName
    }
});