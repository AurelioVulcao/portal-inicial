async function handle(req, res) {
    let robot;
    let networkInfo = IpService.getIpAddress();
    const axios = require('axios');
    let description;
    
    // QUEUE ----------------------------
    const queueList = await Queue.find({
        where: {
            status: ['waiting', 'error'],
            attempts: { '<': 3 }
        },
    }).populate('quotation').sort('created_at ASC').limit(1);
    
    if (!queueList.length) {
        await Cronlog.create({ description: 'Chamada sem processamento', slug: 'Sem processamento', info: 'Fila vazia', network_info: networkInfo });
        res.status(200).json('empty_queue');
        return;
    }
    
    const queue = queueList[0];
    
    // ROBOTS ----------------------------
    if (queue.slug === 'quotation-agro-sompo') {
        description = 'Cotação Agro Sompo';
        robotList = await Robot.find({status: 'free', slug: 'quotation-agro-sompo'}).limit(1);
    }
    
    if (queue.slug === 'transmission-agro-sompo') {
        description = 'Transmissão Agro Sompo';
        robotList = await Robot.find({status: 'free', slug: 'transmission-agro-sompo'}).limit(1);
    }
    
    if (!robotList.length) {
        await Cronlog.create({ description, slug: queue.slug, info: 'Todos os robôs ocupados', network_info: networkInfo });
        res.status(200).json('all robots busy');
        return;
    }
    
    robot = robotList[0];

    await Cronlog.create({ description, slug: queue.slug, info: 'Processado', network_info: networkInfo });
    try {
        await axios.post(robot.address.toString(), { queue, robot});
    } catch(err) {
        console.log('ERROR ============================>');
        console.log(err);
    }

    res.status(200).json(queue);
}

async function errorFinder(req, res) {
    let networkInfo = IpService.getIpAddress();
    const { description, slug } = req.body;
    const emails = 'marcos.mendes@innogroup.com.br'; // Quando for mais de um colocar com virgula dentro da string
    
    const errorNotificationList = await ErrorNotification.find({ select: ['queue'] });
    const errorsAlreadyNotifiedIds = errorNotificationList.map(function(item) { return item.queue });
    const failedQueueList = await Queue.find({ status: 'error', attempts: { '>=': 3}, id : { '!=' : errorsAlreadyNotifiedIds } }).sort('created_at DESC').limit(1);

    if (!failedQueueList.length) {
        await Cronlog.create({ description, slug, info: 'ok', network_info: networkInfo });
        res.status(200).json('ok');
        return;
    }

    const failedQueue = failedQueueList[0];
    const notificationResult = await EmailService.notifyError(emails, {
        queue_id: failedQueue.id,
        quotation_hash: failedQueue.quotation_hash,
        error: failedQueue.error,
        stack: JSON.stringify(failedQueue)
    });

    if (!notificationResult.err) {
        await ErrorNotification.create({
            description: 'Falha de robô por mais de 3 tentativas',
            emails: emails,
            queue: failedQueue.id
        });
    } else {
        await ErrorNotification.create('Envio de email falhou: ' + 'queue_id >' + failedQueue.id, emails, null);
    }

    await Cronlog.create({ description, slug, info: 'erro encontrado, queue: ' + failedQueue.id, network_info: networkInfo });
    res.status(200).json('sent');
}

async function clearBlacklistTokens(req, res) {
    await Cronlog.create({ 
        description: 'Limpar blacklist de tokens',
        slug: 'clear-token-blacklist'
    });

    await Token.destroy({ status_code: 0 });
    res.status(200).json('cleared');
}

module.exports = {
    handle,
    errorFinder,
    clearBlacklistTokens
};
