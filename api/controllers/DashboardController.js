const moment = require('moment');
moment.locale('pt-BR');

async function lastQuotations(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { search } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        const quotationsList = await Quotation.find({
            where: {
                company: tokenData.company_id
            }
        }).populate('customer').sort('created_at DESC').limit(20);

        res.status(200).json(quotationsList);
    } catch(err) {
        res.status(500).json(err);
    }
}

async function comissionsGraph(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { search } = req.body;
    let sumPricePending = 0;
    let sumPriceReceived = 0;
    let countPending = 0;
    let countReceived = 0;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        const start = moment(search.start, 'DD/MM/YYYY"').format('YYYY-MM-DD') + ' 00:00:00';
        const end = moment(search.end, 'DD/MM/YYYY"').format('YYYY-MM-DD') + ' 23:59:59';

        let companyUsersData = await User.find({
            where: {
                company: tokenData.company_id
            },
            select: ['id']
        });

        const usersIds = companyUsersData.map(function(item) {
            return item.id;
        });

    
        const pendingComissions = await Comission.find({
            where: {
                paid_date: { '>=': start, '<=': end },
                user: usersIds,
                status: 'pending'
            },
            select: ['user', 'price']
        });

        if (pendingComissions.length) {
            pendingComissions.forEach(function(item) {
                sumPricePending += item.price;
                countPending++;
            });
        }

        const receivedComissions = await Comission.find({
            where: {
                paid_date: { '>=': start, '<=': end },
                user: usersIds,
                status: 'received'
                },
                select: ['user', 'price']
        });

        if (receivedComissions.length) {
            receivedComissions.forEach(function(item) {
                sumPriceReceived += item.price;
                countReceived++;
            });
        }

        res.status(200).json({
            pending: {
                price: sumPricePending,
                amount: countPending
            },
            received: {
                price: sumPriceReceived,
                amount: countReceived
            }
        });
    } catch(err) {
        console.log('ERRO', err);
        res.status(500).json(err);
    }
}

async function resume(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { search } = req.body;
    let sumOpen = 0;
    let sumClosed = 0;
    let segmentCount = {
        agro: 0,
        auto: 0,
        construcao: 0,
        residencial: 0,
        empresarial: 0,
        equipamento: 0,
        outras: 0
    };

    Array.min = function( array ){
        return Math.min.apply( Math, array );
    };

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        const start = moment(search.start, 'DD/MM/YYYY"').format('YYYY-MM-DD') + ' 00:00:00';
        const end = moment(search.end, 'DD/MM/YYYY"').format('YYYY-MM-DD') + ' 23:59:59';

        const openQuotationValue = await Quotation.find({
            where: {
                created_at : { '>=': start, '<=': end },
                company: tokenData.company_id,
                price: null
            },
            select: ['price', 'segment_type', 'info']
        });

        const closedQuotationValue = await Quotation.find({
            where: {
                created_at : { '>=': start, '<=': end },
                company: tokenData.company_id,
                price: { '!=': null }
            },
            select: ['price', 'segment_type']
        });

        if (openQuotationValue.length) {
            for (quotation of openQuotationValue) {
                quotation.price;

                if (!quotation.info) {
                    continue;
                }

                let quotes = JSON.parse(quotation.info);
                let prices = [];
                
                if (!quotes || !quotes.length) {
                    continue;
                }

                for (singleQuote of quotes) {
                    prices.push(singleQuote.price);
                }

                sumOpen += ~~Array.min(prices);

                segmentCount.agro += quotation.segment_type == 'agro' ? 1 : 0;
                segmentCount.auto += quotation.segment_type == 'auto' ? 1 : 0;
                segmentCount.residencial += quotation.segment_type == 'residencial' ? 1 : 0;
                segmentCount.empresarial += quotation.segment_type == 'empresarial' ? 1 : 0;
                segmentCount.construcao += quotation.segment_type == 'construcao' ? 1 : 0;
                segmentCount.equipamento += quotation.segment_type == 'equipamento' ? 1 : 0;
                segmentCount.outras += quotation.segment_type == 'outras' ? 1 : 0;
            }
        }

        if (closedQuotationValue.length) {
            for (quotation of closedQuotationValue) {

                sumClosed += quotation.price;
                segmentCount.agro += quotation.segment_type == 'agro' ? 1 : 0;
                segmentCount.auto += quotation.segment_type == 'auto' ? 1 : 0;
                segmentCount.residencial += quotation.segment_type == 'residencial' ? 1 : 0;
                segmentCount.empresarial += quotation.segment_type == 'empresarial' ? 1 : 0;
                segmentCount.construcao += quotation.segment_type == 'construcao' ? 1 : 0;
                segmentCount.equipamento += quotation.segment_type == 'equipamento' ? 1 : 0;
                segmentCount.outras += quotation.segment_type == 'outras' ? 1 : 0;
            }

        }

        res.status(200).json({
            open: {
                sum: sumOpen,
                total: openQuotationValue.length
            },
            closed: {
                sum: sumClosed,
                total: closedQuotationValue.length
            },
            segmentCount
        });
    } catch(err) {
        console.log(err);
        res.status(500).json(err);
    }
}

async function lastInjuries(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { search } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        const injuriesList = await Injury.find({
            where: {
                company: tokenData.company_id
            }
        }).populate('customer').sort('created_at DESC').limit(20);

        res.status(200).json(injuriesList);
    } catch(err) {
        res.status(500).json(err);
    }
}

async function lastActivities(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { search } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        const activities = await Activity.find({
            where: {
                company: tokenData.company_id
            }
        }).populate('user').sort('created_at DESC').limit(20);

        res.status(200).json(activities);
    } catch(err) {
        res.status(500).json(err);
    }
}

module.exports = {
    comissionsGraph,
    lastQuotations,
    lastActivities,
    lastInjuries,
    resume
};

