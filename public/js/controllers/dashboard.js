angular.module('vencorr').controller('DashboardController', function($scope, DashboardService, $base64) {
    $scope.loadingQuotations = false;
    $scope.loadingInjuries = false;
    $scope.loadingactivities = false;
    $scope.loadingResume = false;
    $scope.loadingComissionsGraph = false;
    Chart.defaults.global.defaultFontColor = '#0E52AC';
	$scope.startDate = moment().startOf('month').format('DD/MM/YYYY');
    $scope.endDate = moment().endOf('month').format('DD/MM/YYYY');
    $scope.comissionStartDateFilter = moment().startOf('month').format('DD/MM/YYYY');
    $scope.comissionEndDateFilter = moment().startOf('month').add(6,'months').format('DD/MM/YYYY');
    $scope.labels = ['Agro', 'Auto', 'Const', 'Resid', 'Empres', 'Equip', 'Outras'];
    $scope.series = ['Propostas'];
    $scope.graphData = [];
    $scope.comissionLabels = ['Pendentes', 'Recebidas'];
    $scope.comissionData = [];
    $scope.dynamicPopover = {
        advise: {
            title: 'Estimativa',
            content: 'O valor apresentado é uma estimativa, sempre considerando a cotação mais barata. Porém o valor pode aumentar dependendo da escolha do cliente'
        }
    }

    function init() {
        loadData();
    }

    function loadData() {
        listQuotations({ start: $scope.startDate, end: $scope.endDate });
        listInjuries({ start: $scope.startDate, end: $scope.endDate });
        listActivities({ start: $scope.startDate, end: $scope.endDate });
        resume({ start: $scope.startDate, end: $scope.endDate });
        feedComissionGraph({ start: $scope.comissionStartDateFilter, end: $scope.comissionEndDateFilter });
    }

    function listQuotations(filter) {
        $scope.loadingQuotations = true;
        DashboardService.listQuotations(filter).then(function(resp) {
            $scope.loadingQuotations = false;
            $scope.quotations = resp.data;
        }).catch(function(err) {
            $scope.loadingQuotations = false;
        });
    }

    function listInjuries(filter) {
        $scope.loadingInjuries = true;
        DashboardService.listInjuries(filter).then(function(resp) {
            $scope.loadingInjuries = false;
            $scope.injuries = resp.data;

            if ($scope.injuries.length) {
                $scope.injuries.forEach(function(injury) {
                    injury.encodedId = $base64.encode(injury.id);
                });
            }

        }).catch(function(err) {
            $scope.loadingInjuries = false;
        });
    }

    function listActivities(filter) {
        $scope.loadingactivities = true;
        DashboardService.listActivities(filter).then(function(resp) {
            $scope.loadingactivities = false;
            $scope.activities = resp.data;

            if ($scope.activities.length) {
                $scope.activities.forEach(function(activity) {
                    activity.data = JSON.parse(activity.data);

                    if (activity.model == 'injury' || activity.model == 'customer') {
                        activity.data.encodedId = $base64.encode(activity.data.id);
                    }
                });
            }
        }).catch(function(err) {
            $scope.loadingactivities = false;
        });
    }

    function feedComissionGraph(filter) {
        $scope.loadingComissionsGraph = true;
        DashboardService.getComissionGraphData(filter).then(function(resp) {
            $scope.loadingComissionsGraph = false;
            $scope.comissionGraphData = resp.data;
            $scope.comissionData = [resp.data.pending.amount, resp.data.received.amount];
        }).catch(function(err) {
            console.log(err)
            $scope.loadingComissionsGraph = false;
        });
    }

    function resume(filter) {
        $scope.loadingResume = true;
        DashboardService.resume(filter).then(function(resp) {
            $scope.loadingResume = false;
            $scope.resume = resp.data;
            var segment = resp.data.segmentCount;

            $scope.graphData = [
                +segment.agro, +segment.auto, +segment.construcao, +segment.residencial, +segment.empresarial, +segment.equipamento, +segment.outras
            ];
        }).catch(function(err) {
            $scope.loadingResume = false;
        });
    }

    function formatSegmentName(segment) {
        if (segment === 'construcao') {
            return 'Construção';
        }

        if (segment === 'agro') {
            return 'Agro';
        }

        if (segment === 'auto') {
            return 'Auto';
        }

        if (segment === 'residencial') {
            return 'Residencial';
        }

        if (segment === 'empresarial') {
            return 'Empresarial';
        }

        if (segment === 'equipamento') {
            return 'Equipamento';
        }

        if (segment === 'outras') {
            return 'Outras';
        }
    }

    function formatStatusName(status) {
        if (status === 'open') {
            return 'Aberta';
        }

        if (status === 'in_progress') {
            return 'Em andamento';
        }

        if (status === 'waiting') {
            return 'Aguardando Seguradora';
        }

        if (status === 'approved') {
            return 'Aprovada';
        }

        if (status === 'refused') {
            return 'Negada';
        }
    }

    function formatTypeName(type) {
        if (type === 'basic') {
            return 'Básica';
        }

        if (type === 'electrical') {
            return 'Danos Elétricos';
        }

        if (type === 'civil') {
            return 'Responsabilidade Civil';
        }

        if (type === 'glass') {
            return 'Vidros';
        }

        if (type === 'other') {
            return 'Outros';
        }
    }

	function setDateFilter(period) {
        if (period === 'week') {
            $scope.startDate = moment().day(0).format('DD/MM/YYYY');
            $scope.endDate = moment().day(6).format('DD/MM/YYYY');
        }

        if (period === 'month') {
            $scope.startDate = moment().startOf('month').format('DD/MM/YYYY');
            $scope.endDate = moment().endOf('month').format('DD/MM/YYYY');
        }

        if (period === 'lastMonth') {
            $scope.startDate = moment().startOf('month').subtract(1,'months').format('DD/MM/YYYY');
            $scope.endDate = moment().endOf('month').subtract(1,'months').format('DD/MM/YYYY');
        }

        if (period === 'lastWeek') {
            $scope.startDate = moment().day(0).subtract(1,'week').format('DD/MM/YYYY');
            $scope.endDate = moment().day(6).subtract(1,'week').format('DD/MM/YYYY');
        }

        if (period === 'lastSemesterComission') {
            $scope.comissionStartDateFilter = moment().startOf('month').subtract(6,'months').format('DD/MM/YYYY');
            $scope.comissionEndDateFilter = moment().startOf('month').format('DD/MM/YYYY');
        }

        if (period === 'nextSemesterComission') {
            $scope.comissionStartDateFilter = moment().startOf('month').format('DD/MM/YYYY');
            $scope.comissionEndDateFilter = moment().startOf('month').add(6,'months').format('DD/MM/YYYY');
        }

        if (period === 'thisMonthComission') {
            $scope.comissionStartDateFilter = moment().startOf('month').format('DD/MM/YYYY');
            $scope.comissionEndDateFilter = moment().endOf('month').format('DD/MM/YYYY');
        }

        if (period === 'nextMonthComission') {
            $scope.comissionStartDateFilter = moment().startOf('month').format('DD/MM/YYYY');
            $scope.comissionEndDateFilter = moment().startOf('month').add(1,'months').format('DD/MM/YYYY');
        }

        loadData();
	}
    
    init();
    $scope.formatSegmentName = formatSegmentName;
    $scope.formatStatusName = formatStatusName;
    $scope.formatTypeName = formatTypeName;
    $scope.setDateFilter = setDateFilter;
    $scope.loadData = loadData;
});
