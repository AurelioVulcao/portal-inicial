angular.module('vencorr').controller('WalletController', function($scope, ComissionService, SweetAlert) {
	$scope.loading = false;
	$scope.startDate = moment().startOf('month').format('DD/MM/YYYY');
    $scope.endDate = moment().endOf('month').format('DD/MM/YYYY');
    $scope.filterOption = 'all';
    
    function init() {
        list();
    }
    
    function list() {
        var params = {
            start: $scope.startDate,
            end: $scope.endDate,
        };

        if ($scope.filterOption !== 'all') {
            params.status = $scope.filterOption;
        }

        $scope.loading = true;
        ComissionService.list({ params: params}).then(function(resp) {
            $scope.comissions = resp.data.comissions;
            $scope.totals = resp.data.totals;
            $scope.loading = false;
        }).catch(function(err) {
            $scope.loading = false;
        });
    }

    function confirm(comissionId) {
        if (!comissionId) {
            return;
        }

        SweetAlert.swal({
			title: "Confirmar o recebimento?",
			text: "Uma vez depois de confirmado, não é possível desfazer a ação",
			type: "info",
			showCancelButton: true,
			confirmButtonText: "Confirmar",
			cancelButtonText: "Cancelar"
		}, function(isConfirm) {
            if (isConfirm) {
                $scope.loading = true;
                ComissionService.confirm(comissionId).then(function(resp) {
                    list();
                }).catch(function(err) {
                    $scope.loading = false;
                    SweetAlert.error('Ops! Ocorreu um erro inesperado', 'Tente novamente por favor, se o erro persistir, consulte o suporte, desculpe pelo transtorno');
                });
            }
        });
    }

	function setDateFilter(period) {
        if (period === 'today') {
            $scope.startDate = moment().format('DD/MM/YYYY');
            $scope.endDate = moment().format('DD/MM/YYYY');
        }

        if (period === 'yesterday') {
            $scope.startDate = moment().subtract(1,'day').format('DD/MM/YYYY');
            $scope.endDate = moment().subtract(1,'day').format('DD/MM/YYYY');
        }

        if (period === 'week') {
            $scope.startDate = moment().day(0).format('DD/MM/YYYY');
            $scope.endDate = moment().day(6).format('DD/MM/YYYY');
        }

        if (period === 'lastWeek') {
            $scope.startDate = moment().day(0).subtract(1,'week').format('DD/MM/YYYY');
            $scope.endDate = moment().day(6).subtract(1,'week').format('DD/MM/YYYY');
        }

        if (period === 'month') {
            $scope.startDate = moment().startOf('month').format('DD/MM/YYYY');
            $scope.endDate = moment().endOf('month').format('DD/MM/YYYY');
        }

        if (period === 'lastMonth') {
            $scope.startDate = moment().startOf('month').subtract(1,'months').format('DD/MM/YYYY');
            $scope.endDate = moment().endOf('month').subtract(1,'months').format('DD/MM/YYYY');
        }

        if (period === 'nextMonth') {
            $scope.startDate = moment().startOf('month').add(1,'months').format('DD/MM/YYYY');
            $scope.endDate = moment().endOf('month').add(1,'months').format('DD/MM/YYYY');
        }

        list();
    }

    init();
    $scope.setDateFilter = setDateFilter;
    $scope.confirm = confirm;
    $scope.list = list;
});