angular.module("vencorr").factory("AuthFactory", function(StorageService, $location) {
    function isAuthenticated() {
        if (!StorageService.getUser().logged) {
            $location.path('/');
        }
    }

    return {
        isAuthenticated: isAuthenticated
    };
});