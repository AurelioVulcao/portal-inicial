module.exports = {
  attributes: {
		items: {
			type: 'ref',
			columnType: 'text'
		},
		customer: {
      model: 'customer',
      columnName: 'customer_id'
    },
    customer_snapshot: {
			type: 'ref',
			columnType: 'text'
    },
    user: {
      model: 'user',
      columnName: 'user_id'
    },
    company: {
      model: 'company',
      columnName: 'company_id'
    },
		price: {
				type: 'ref',
				columnType: 'float'
		},
		comission: {
      type: 'string'
		},
		start_term: {
			type: 'ref',
			columnType: 'date'
    },
    end_term: {
			type: 'ref',
			columnType: 'date'
    },
    term_type: {
			type: 'string'
    },
    installments: {
      type: 'number',
      defaultsTo: 1
    },
    status: {
			type: 'string'
    },
    queue: {
      model: 'queue',
      columnName: 'queue_id'
    },
    segment_type: {
			type: 'string'
    },
    payment_type: {
			type: 'string'
    },
    hash: {
			type: 'string'
    },
    bank_slip_url: {
			type: 'ref',
			columnType: 'text'
    },
    policy_url: {
			type: 'ref',
			columnType: 'text'
    },
    obs: {
			type: 'ref',
			columnType: 'text'
    },
    extra: {
      type: 'ref',
			columnType: 'text'
    },
    info: {
      type: 'ref',
			columnType: 'text'
    },
    message: {
			model: 'message',
			columnName: 'message_id'
		},
	},
};

