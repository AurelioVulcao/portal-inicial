async function upload(req, res) {
    const tokenData = UtilService.getTokenData(req);
    let { folder } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        req.file('file').upload(async function (err, uploadedFiles) {
            let filesPath = [];

            if (err) {
                res.status(500).json('error');
                return;
            }
    
            for (file of uploadedFiles) {
                let s3Url = await UploadService.toS3(file.fd, file.type, folder || 'app');
                filesPath.push(s3Url);
            }
    
           res.status(200).json(filesPath);
        });
    } catch(err) {
        res.status(500).json(err);
    }
}

module.exports = {
    upload
};

