# Portal Franqueado

### SSL
```
cd /etc/letsencrypt/live/franqueado.vencorr.com
sudo cp cert.pem /var/www/portal-franqueado/config/ssl/
sudo cp chain.pem /var/www/portal-franqueado/config/ssl/
sudo cp privkey.pem /var/www/portal-franqueado/config/ssl/
```

### Generate Certificate

`sudo certbot --nginx -d franqueado.vencorr.com`

### Renew Certificates

```
sudo certbot renew --dry-run
```

### Run PM2 PROD

```
pm2 start app.js --name "Portal Franqueado" -- --prod
```

### NGINX

```
sudo ln -s /etc/nginx/sites-available/franqueado.vencorr.com /etc/nginx/sites-enabled/

server {
    server_name franqueado.vencorr.com;
    root /var/www/portal-franqueado/public/dist;
    index index.html;

    location / {
        try_files $uri $uri/ /index.html;
    }
}
```

# Portal Administrativo

### SSL
```
cd /etc/letsencrypt/live/digital.vencorr.com
sudo cp cert.pem /var/www/vencorr-digital-admin/config/ssl/
sudo cp chain.pem /var/www/vencorr-digital-admin/config/ssl/
sudo cp privkey.pem /var/www/vencorr-digital-admin/config/ssl/
```

### Generate Certificate

`sudo certbot --nginx -d digital.vencorr.com`

### Renew Certificates

```
sudo certbot renew --dry-run
```

### Nginx

```
sudo ln -s /etc/nginx/sites-available/digital.vencorr.com /etc/nginx/sites-enabled/

server {
    server_name digital.vencorr.com;
    root /var/www/vencorr-digital-admin/public/dist;
    index index.html;

    location / {
        try_files $uri $uri/ /index.html;
    }
}
```

### Run PM2 PROD

```
pm2 start app.js --name "ADM" -- --prod --port 1338
```

### CRON

* * * * * curl --request GET 167.71.106.140:1337/api/loldsoir8934hir438239dsaiuhdainv9rfffv98f
* * * * * curl --data "description=Verificador de Erros&slug=error-check" 167.71.106.140:1337/api/oidfNdfion90fdj0sdfjjlgfkjncmxnwsdwhufhiu


