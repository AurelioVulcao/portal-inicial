angular.module("vencorr").factory("CustomerService", function ($http, config) {
    function getAdressByCep(cep) {
        return $http.post(config.baseUrl + "/search/cep", { cep: cep });
    }

    function getById(id) {
        return $http.get(config.baseUrl + "/customer/" + id);
    }

    function save(data) {
        return $http.post(config.baseUrl + "/customer", data);
    }

    function update(data) {
        return $http.put(config.baseUrl + "/customer", data);
    }

    function list(params) {
        return $http.post(config.baseUrl + "/customers", params);
    }

    function suggest(term) {
        return $http.post(config.baseUrl + "/customers/suggest", { term: term });
    }

    return {
        getAdressByCep: getAdressByCep,
        suggest: suggest,
        getById: getById,
        update: update,
        save: save,
        list: list
    };
});