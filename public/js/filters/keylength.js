angular.module('vencorr').filter('keylength', function(){
    return function(input){
        if (!angular.isObject(input)) {
            return;
        }

        return Object.keys(input).length;
    }
});