angular.module("vencorr").factory("SompoService", function ($http, config) {
    function list() {
        return $http.get(config.baseUrl + "/sompo/list");
    }

    function generatePDF(data) {
        return $http.post(config.baseUrl + "/sompo/proposal-pdf", data);
    }

    return {
        list: list,
        generatePDF: generatePDF
    };
});