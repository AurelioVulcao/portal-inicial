angular.module('vencorr').directive('chooseCustomer', function () {
    return {
        templateUrl: 'views/choose-customer.html',
        replace: true,
        restrict: 'E'
    };
});