angular.module('vencorr').factory('StorageService', function($window, $base64) {
	var set = function(key, value) {
		var encodedKey = $base64.encode(key);
		var encodedValue = $base64.encode(value);

		$window.localStorage.setItem(encodedKey, encodedValue);
	};

	var get = function(key) {
		var encodedKey = $base64.encode(key);
		var encodedValue = $window.localStorage.getItem(encodedKey);

		if (!encodedValue) {
			return;
		}

		try {
			decodedValue = $base64.decode(encodedValue);
		} catch(err) {
			// Token inválido, o usuário é deslogado devido ao catch
			return;
		}

		return $base64.decode(encodedValue);
	};

	var remove = function(key) {
		var encodedKey = $base64.encode(key);

		$window.localStorage.removeItem(encodedKey);
	}

	var clear = function() {
		$window.localStorage.clear();
	}

	var getUser = function() {
		return angular.fromJson(get('user')) || {};
	}

	return {
		set: set,
		get: get,
		remove: remove,
		clear: clear,
		getUser: getUser
	}

});