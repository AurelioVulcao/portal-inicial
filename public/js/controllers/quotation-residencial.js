angular.module('vencorr').controller('QuotationResidencialController', function($scope, $routeParams, $rootScope, $location, config, QuotationService, SweetAlert, CustomerService, Upload, MetaService) {
    $scope.loading = false;
    $scope.isEdit = false;
    $scope.hash = $routeParams.hash;
    $scope.customerSnapshot = {};
    $scope.loadingCustomer = false;
    $scope.searchCustomer = {};
    $scope.chooseCustomer = false;
    $scope.cepLoading = false;
    $scope.quotationMissingFields = [];
    $scope.quotation = {
        files: []
    };
    $scope.residenceTypes =[
        { name: 'AEROPORTO', value: 'aeroporto' },{ name: 'ALAMEDA', value: 'alameda' },{ name: 'ÁREA', value: 'area' },{ name: 'AVENIDA', value: 'avenida' },{ name: 'CHÁCARA', value: 'chacara' },{ name: 'CONJUNTO', value: 'conjunto' },{ name: 'COLÔNIA', value: 'colonia' },{ name: 'CAMPO', value: 'campo' },{ name: 'CONDOMÍNIO', value: 'condominio' },{ name: 'DISTRITO', value: 'distrito' },{ name: 'ESPLANADA', value: 'esplanada' },{ name: 'ESTRADA', value: 'estrada' },{ name: 'ESTAÇÃO', value: 'estação' },{ name: 'FEIRA', value: 'feira' },{ name: 'FAVELA', value: 'favela' },{ name: 'FAZENDA', value: 'fazenda' },{ name: 'JARDIM', value: 'jardim' },{ name: 'LAGO', value: 'lago' },{ name: 'LADEIRA', value: 'ladeira' },{ name: 'LAGOA', value: 'lagoa' },{ name: 'LARGO', value: 'largo' },{ name: 'LOTEAMENTO', value: 'loteamento' },{ name: 'MORRO', value: 'morro' },{ name: 'NÚCLEO', value: 'nucleo' },{ name: 'OUTROS', value: 'outros' },{ name: 'PRAÇA', value: 'praca' },{ name: 'PARQUE', value: 'parque' },{ name: 'PASSARELA', value: 'passarela' },{ name: 'PÁTIO', value: 'patio' },{ name: 'QUADRA', value: 'quadra' },{ name: 'RUA', value: 'rua' },{ name: 'RECANTO', value: 'recanto' },{ name: 'RODOVIA', value: 'rodovia' },{ name: 'RESIDENCIAL', value: 'residencial' },{ name: 'SITIO', value: 'sitio' },{ name: 'SETOR', value: 'setor' },{ name: 'TRECHO', value: 'trecho' },{ name: 'TRAVESSA', value: 'travessa' },{ name: 'TREVO', value: 'trevo' },{ name: 'VIA', value: 'via' },{ name: 'VIADUTO', value: 'viaduto' },{ name: 'VILA', value: 'vila' },{ name: 'VALE', value: 'vale' },{ name: 'VIELA', value: 'viela' },{ name: 'VEREDA', value: 'vereda' }
    ];
    $scope.dynamicPopover = {
        bonus_anterior : {
            title: 'Deve-se informar a classe de bônus anterior. Seguradoras que usam esse campo:',
            content: 'Sulamerica, Bradesco, Generali'
        },
        bonus_futura : {
            title: 'Classe de bônus a ser calculada',
            content: 'Deve-se informar a classe de bonus que será calculada na nova vigência.\
            Por exemplo: Se a classe de bonus vigente é 2, então você irá informar neste campo 3.'
        }
    };

    function init() {
        if ($scope.hash) {
            $scope.isEdit = true;
            getQuotationData($scope.hash);
		}else{
            setPeriod();
            setInitialValues();
        }
		
		// Load pre data customer form
		if (!$scope.quotation.customer) {
			$scope.quotation.customer = {};
			$scope.quotation.customer.type = 'fisica';
			$scope.quotation.customer.genre = 'masculino';
			$scope.quotation.customer.rg_uf = 'SP';
		}
    }

    function getQuotationData() {
        $scope.loading = true;
        QuotationService.get($scope.hash).then(function(resp) {
            var quotationResp = resp.data;

            if (quotationResp && quotationResp.extra) {
                var receivedQuotation = JSON.parse(resp.data.extra);
                
                receivedQuotation.status = resp.data.status;
                receivedQuotation.hash = resp.data.hash;
                receivedQuotation.id = resp.data.id;
                receivedQuotation.message = resp.data.message;
                receivedQuotation.start_term = resp.data.start_term;
                receivedQuotation.end_term = resp.data.end_term;
                receivedQuotation.info = JSON.parse(resp.data.info);
                $scope.customerSnapshot = JSON.parse(resp.data.customer_snapshot);
                $scope.quotation = formatAfterGet(receivedQuotation);

                if ($scope.quotation.status == 'accepted') {
                    $scope.quotation.info.forEach(function(quote) {
                        if (quote.accept) {
                            $scope.acceptedQuote = quote;
                        }
                    });
                }
            }

            $scope.loading = false;
        }).catch(function(err) {
            console.log(err);
            $scope.loading = false;
            SweetAlert.error('Falha ao consultar cotação', 'Verifique se esta cotação existe no menu [Cotações]');
        });
    }

    function formatAfterGet(quotationToFormat) {
        quotationToFormat.start_term = moment(quotationToFormat.start_term, 'YYYY-MM-DD').format('DD/MM/YYYY');
        quotationToFormat.end_term = moment(quotationToFormat.end_term, 'YYYY-MM-DD').format('DD/MM/YYYY');
        
        return quotationToFormat; 
    }
	
	function onSelectCustomer(customer) {
		$scope.customerSnapshot = customer;
		$scope.quotation.customerId = customer.id;
		$scope.chooseCustomer = false;
    }

    function useCustomerAddress(cep) {
        if (!cep) {
            return;
        }

        $scope.cepLoading = true;
        CustomerService.getAdressByCep(cep).then(function(resp) {
            var response = resp.data;

            if(!$scope.customerSnapshot || !$scope.customerSnapshot.cep){
                SweetAlert.warning('Cliente sem CEP', 'O cliente cadastrado não possui CEP, favor informar manualmente ou cadastrar o CEP do cliente');
                return;
            }else{
                $scope.quotation.property_number = $scope.customerSnapshot.number;
                $scope.quotation.property_complement = $scope.customerSnapshot.complement;
            }

            $scope.quotation.property_city = response.city || '';
            $scope.quotation.property_address = response.street || '';
            $scope.quotation.property_neighborhood = response.neighborhood || '';
            $scope.quotation.property_cep = response.cep || '';
            $scope.quotation.property_state = response.state || '';

            $scope.cepLoading = false;
        }).catch(function(err) {
            console.log(err)
            $scope.cepLoading = false;
            SweetAlert.warning("Ops! Não encontramos o CEP digitado", 'Utilizamos 2 serviços para busca de CEP e não encontramos, verifique se você realmente digitou corretamente ou complete manualmente o endereço')
        });
    }
    
    function cancel() {
        SweetAlert.swal({
            title: 'Cancelar a cotação?',
            text: 'Uma vez cancelada, não é possível voltar atrás, tem certeza?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Cancelar Cotação',
            cancelButtonText: 'Voltar',
            confirmButtonColor: '#ce1a1a'
         }, function(isConfirm) {
             if (isConfirm) {
                QuotationService.updateStatus({ 
                    quotation_id: $scope.quotation.id,
                    status: 'canceled' })
                .then(function(resp) {
                    console.log('CHEGOU AQUI')
                    getQuotationData($scope.hash);
                });
             }
         });
    }

    function closeModal() {
		$('#customerModal').modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
    }

    function validateQuotation(quotation) {
        var missingFields = [];
        var fields = [
            'start_term',
            'end_term',
            'customerId',
            'property_cep',
            'property_type',
            'property_address',
            'property_neighborhood',
            'property_city',
            'property_state',
            'property_segment',
            'property_material',
            'property_construction_type'
        ];

        if ($scope.quotation.renovation != 'novo') {
            fields.push('renovation_previous_bonus');
            fields.push('renovation_future_bonus');
            fields.push('renovation_occurrences');
        } else {
            fields.slice(fields.indexOf('renovation_previous_bonus'), 1);
            fields.slice(fields.indexOf('renovation_future_bonus'), 1);
            fields.slice(fields.indexOf('renovation_occurrences'), 1);
        }

        fields.forEach(function(key) {
            if (!$scope.quotation[key]) {
                missingFields.push(key);
            }
        });

        if (missingFields.length) {
            $scope.quotationMissingFields = missingFields;
            SweetAlert.warning('Preencha todos os campos obrigatórios', 'Estão maracados com um *');
            return false;
        } else {
            return true;
        }
    }

    function isMissingFields(key) {
        return $scope.quotationMissingFields.indexOf(key) !== -1 ? true : false;
    }
    
    function validateCustomer(customer) {
		var missingFields = [];
		var fields = { 
			'name': 'Nome',
			'cpf': 'CPF',
			'birthdate': 'Data de Nascimento',
			'rg_number': 'RG',
			'rg_org': 'Rg Orgão Expeditor',
			'rg_uf': 'RG UF',
			'rg_emission_date': 'RG Data de Emissão',
			'cnpj': 'CNPJ',
			'email': 'Email',
			'phone': 'Telefone',
			'cep': 'CEP',
			'state': 'Estado',
			'city': 'Cidade',
			'address': 'Endereço',
			'number': 'Número' };

		Object.keys(fields).forEach(function(key) {
			if (!customer[key]) {
				if (key === 'cnpj' && customer.type === 'fisica') {
					return;
				}

				if (key.includes('rg') && customer.type === 'juridica') {
					return;
				}

				if (key === 'birthdate' && customer.type === 'juridica') {
					return;
				}

				if (key === 'genre' && customer.type === 'juridica') {
					return;
				}

				if (key === 'cpf' && customer.type === 'juridica') {
					return;
				}

				missingFields.push(fields[key]);
			}
		});

		missingFields = missingFields.join(', ');

		if (missingFields.length) {
			SweetAlert.warning('Preencha todos os campos obrigatórios', missingFields.toString());
			return false;
		} else {
			return customer;
		}
    }
    
    function saveCustomer() {
		var customer = JSON.parse(JSON.stringify($scope.quotation.customer));
        var validatedCustomer = validateCustomer(customer);

		if (!validatedCustomer) {
			return;
		}

		$scope.loadingCustomer = true;
		CustomerService.save({ customer: customer }).then(function(resp) {
            $scope.customerSnapshot = resp.data;
            $scope.quotation.customerId = resp.data.id;
            $scope.loadingCustomer = false;
            resetCustomer();
            closeModal();
		}).catch(function(err) {
            SweetAlert.info('Cliente já existe', 'Um cliente com este EMAIL ou CPF/CNPJ, já existe, verifique por favor.');
			$scope.loadingCustomer = false;
		});
    }

    function resetCustomer() {
        $scope.quotation.customer = {};
        $scope.quotation.customer.type = 'fisica';
        $scope.quotation.customer.genre = 'masculino';
        $scope.quotation.customer.rg_uf = 'SP';
    }
    
    function getAddressByCepProperty(cep) {
        if (!cep) {
            return;
        }

        $scope.cepLoading = true;
        CustomerService.getAdressByCep(cep).then(function(resp) {
            var response = resp.data;

            $scope.quotation.property_city = response.city || '';
            $scope.quotation.property_address = response.street || '';
            $scope.quotation.property_cep = response.cep || '';
            $scope.quotation.property_state = response.state || '';
            $scope.quotation.property_neighborhood = response.neighborhood || '';

            $scope.cepLoading = false;
        }).catch(function(err) {
            $scope.cepLoading = false;
            SweetAlert.warning('Ops! Não encontramos o CEP digitado', 'Utilizamos 2 serviços para busca de CEP e não encontramos, verifique se você realmente digitou corretamente ou complete manualmente o endereço')
        });
    }

    function getAddressByCep(cep) {
        if(cep && cep.length > 0){
            $scope.cepLoading = true;
            CustomerService.getAdressByCep(cep).then(function(resp) {
                var response = resp.data;

                $scope.quotation.customer.city = response.city;
                $scope.quotation.customer.address = response.street + ' - ' + response.neighborhood;
                $scope.quotation.customer.cep = response.cep;
                $scope.quotation.customer.state = response.state;

                if (!response.street || !response.neighborhood) {
                    $scope.quotation.customer.address.street = '';
                }

                $scope.cepLoading = false;
            }).catch(function(err) {
                $scope.cepLoading = false;
                SweetAlert.warning("Ops! Não encontramos o CEP digitado", 'Utilizamos 2 serviços para busca de CEP e não encontramos, verifique se você realmente digitou corretamente ou complete manualmente o endereço')
            });
        }
    }
    
    function enableCustomerSearch() {
		$scope.chooseCustomer = true;
		$scope.searchCustomer.value = null;
	}

	function disableCustomerSearch() {
		$scope.searchCustomer.value = '';
		$scope.chooseCustomer = false;
	}

	function getCustomers(typedTerm) {
		return CustomerService.suggest(typedTerm).then(function(resp) {
			return resp.data.map(function(item) {
				return item;
			});
		});
	};

	function editCustomerData() {
		$window.location.href = '/cliente-cadastrar/' +  $base64.encode($scope.quotation.customer.id);
	}
    
    function clearRenovationValues() {
        if ($scope.quotation.renovacao === 'novo') {
            $scope.quotation.renovation_previous_bonus = null;
            $scope.quotation.renovation_future_bonus = null;
            $scope.quotation.renovation_occurrences = null;
            $scope.quotation.renovation_final_policy_date = null;
            $scope.quotation.renovation_current_policy_date = null;
            $scope.quotation.renovation_current_ci = null;
        }
    }

    function setPeriod() {
        $scope.quotation.start_term = moment().format('DD/MM/YYYY');
        $scope.quotation.end_term = moment().add(1, 'year').format('DD/MM/YYYY');
    }

    function setInitialValues() {
        $scope.quotation.renovation = 'novo';
    }

    function triggerUpload(label) {
        $scope.currentImgLabel = label;
        document.getElementById('imageUpload').click();
    };

    function uploadFiles(files, errFiles) {
        $scope.uploadingFiles = files;
        $scope.errFiles = errFiles;
        $scope.finished = 0;
        $scope.hideProgressBars = false;

        angular.forEach(files, function(file, index) {
            file.upload = Upload.upload({
                url: config.uploadEndpoint,
                data: { file: file,'folder': 'docs' },
            });

            file.upload.then(function (response) {
                $scope.quotation.files.push({
                    url: response.data[0],
                    label: 'Arquivo ' + ($scope.quotation.files.length + 1)
                });
                
                $scope.finished++;

                if ($scope.finished == files.length) {
                    $scope.hideProgressBars = true;
                }
            }, function (response) {
                console.log(response.status + ': ' + response.data);
                SweetAlert.warning('Ocorreu um erro com um dos arquivos', 'Se o problema persistir, tente usar outro arquivo ou informe ao suporte.');
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        });
    }

    function removeDoc(url) {
        if (!$scope.quotation.files.length) {
            return;
        }

        $scope.quotation.files = $scope.quotation.files.filter(function(doc) {
            return url != doc.url;
        });
    }

    function checkRequiredInfo(quotation) {
        var isValidQuotation = validateQuotation(quotation);

        if (!isValidQuotation) {
            return;
        }

        if (!$scope.quotation.files.length) {
            SweetAlert.swal({
                title: 'Deseja enviar sem anexar documentos?',
                text: 'Talvez você tenha esquecido de anexar algum documento, mas você pode continuar mesmo assim',
                type: 'info',
                showCancelButton: true,
                confirmButtonText: 'Enviar assim mesmo',
                cancelButtonText: 'Cancelar'
            }, function(isConfirm) {
                if (isConfirm) {
                    save();
                } else {
                    return;
                }
            });
        } else {
            save();
        }
    }

    function save() {
        $scope.loading = true;
        QuotationService.save({
            quotation_info: $scope.quotation,
            customer_snapshot: JSON.stringify($scope.customerSnapshot),
            segment: 'residencial',
            customer_id: $scope.quotation.customerId,
            start_term: moment($scope.quotation.start_term, 'DD/MM/YYYY').format('YYYY-MM-DD'),
            end_term:  moment($scope.quotation.end_term, 'DD/MM/YYYY').format('YYYY-MM-DD'),
            items: capitalize($scope.quotation.property_segment)
        }).then(function(resp){
            // Send email
            MetaService.solicitationRequestEmail({
                customer_name: $rootScope.loggedUser.name,
                company_name: $rootScope.loggedUser.company.name,
                segment: 'Residencial',
                hash: resp.data.hash
            }).then(function(resp) {
                $scope.loading = false;
                console.log(resp.data);
            });

            $location.path('/cotacoes');
        }).catch(function(err) {
            console.log(err);
            $scope.loading = false;
            SweetAlert.error('Ops!', 'Ocorreu um erro inesperado, tente novamente, se o erro persistir, entre em contato com o suporte');
        });
    }

    function cancel() {
        SweetAlert.swal({
            title: 'Cancelar a cotação?',
            text: 'Uma vez cancelada, não é possível voltar atrás, tem certeza?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Cancelar Cotação',
            cancelButtonText: 'Voltar',
            confirmButtonColor: '#ce1a1a'
         }, function(isConfirm) {
             if (isConfirm) {
                $scope.loading = true;
                QuotationService.updateStatus({ 
                    quotation_id: $scope.quotation.id,
                    status: 'canceled' })
                .then(function(resp) {
                    getQuotationData($scope.hash);
                }).catch(function(err) {
                    $scope.loading = false;
                });
             }
         });
    }

    function accept(quote) {
        SweetAlert.swal({
            title: 'Aceitar esta cotação?',
            text: 'Selecionando esta cotação, você aceita prosseguir com o fechamento do seguro no valor e observações descritas.',
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Aceitar',
            cancelButtonText: 'Voltar',
            confirmButtonColor: '#0E52AC'
         }, function(isConfirm) {
             if (isConfirm) {
                $scope.loading = true;
                quote.accept = true;

                QuotationService.updateGeneric({ 
                    quotation_id: $scope.quotation.id,
                    status: 'accepted',
                    info: JSON.stringify($scope.quotation.info),
                    price: quote.price
                })
                .then(function(resp) {
                    MetaService.quotationChosenEmail({
                        customer_name: $rootScope.loggedUser.name,
                        company_name: $rootScope.loggedUser.company.name,
                        segment: 'Residencial',
                        hash: $scope.quotation.hash
                    }).then(function(resp) {
                        getQuotationData($scope.hash);
                        console.log(resp.data);
                    });
                }).catch(function(err) {
                    $scope.loading = false;
                });
             }
         });
    }

    function capitalize(word) {
        if (!word) {
            return;    
        }
        
        return word.charAt(0).toUpperCase() + word.slice(1);
    }

    init();
    $scope.capitalize = capitalize;
    $scope.checkRequiredInfo = checkRequiredInfo;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.removeDoc = removeDoc;
    $scope.editCustomerData = editCustomerData;
	$scope.onSelectCustomer = onSelectCustomer;
	$scope.getCustomers = getCustomers;
	$scope.disableCustomerSearch = disableCustomerSearch;
    $scope.enableCustomerSearch = enableCustomerSearch;
    $scope.saveCustomer = saveCustomer;
    $scope.getAddressByCep = getAddressByCep;
    $scope.clearRenovationValues = clearRenovationValues;
    $scope.triggerUpload = triggerUpload;
    $scope.uploadFiles = uploadFiles;
    $scope.useCustomerAddress = useCustomerAddress;
    $scope.getAddressByCepProperty = getAddressByCepProperty;
    $scope.validateQuotation = validateQuotation;
    $scope.isMissingFields = isMissingFields;
    $scope.accept = accept;
});