const jwt = require('jsonwebtoken');

function formatMoney(amount) {
    var j;
    var n = amount;
    var c = 2;
    var d = ',';
    var t = '.';
    var s = n < 0 ? '-' : '';
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + '';

    j = (j = i.length) > 3 ? j % 3 : 0;
    return 'R$ ' + s + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
}


function getCompanyId(req) {
	var token = req.headers.authorization;
	var decoded;

	decoded = jwt.decode(token);

	if (!decoded.company_id) {
		return false;
	}

	return decoded.company_id;
}

function getSellerId(req) {
	var token = req.headers.authorization;
	var decoded;

	decoded = jwt.decode(token);

	if (!decoded.seller_id) {
		return false;
	}

	return decoded.seller_id;
}

function getTokenData(req) {
	const token = req.headers.authorization;
	const decoded = jwt.decode(token);

	return decoded;
}

function getCompanyName(req) {
	var token = req.headers.authorization;
	var decoded;

	decoded = jwt.decode(token);

	if (!decoded.company_name) {
		return false;
	}

	return decoded.company_name;
}

async function saveActivity(activity) {
	if (!activity) {
		return false;
	}

	try {
		const Model = activity.model;
		const objectData = JSON.parse(activity.data);
		const objectFound = await sails.models[Model].findOne({ id: objectData.id });

		activity.data = JSON.stringify(objectFound);
		await Activity.create(activity);
		return true;
	} catch(err) {
		return err;
	}
}

function sortByAsc(key) {
	return function(a, b) {
		if (a[key] < b[key]) {
			return -1;
		}
		if (a[key] > b[key]) {
			return 1;
		}

		return 0;
	};
}

function sortByDesc(key) {
	return function(a, b) {
		if (a[key] < b[key]) {
			return 1;
		}
		if (a[key] > b[key]) {
			return -1;
		}

		return 0;
	};
}

module.exports = {
    formatMoney,
    getCompanyId,
    getSellerId,
	getCompanyName,
	getTokenData,
	saveActivity,
	sortByAsc,
	sortByDesc
}