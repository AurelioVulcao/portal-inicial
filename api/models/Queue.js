module.exports = {
  attributes: {
    description: {
      type: 'string'
    },
    slug: {
      type: 'string'
		},
		info: {
			type: 'ref',
			columnType: 'text'
    },
    status: {
      type: 'string'
    },
		error: {
      type: 'ref',
			columnType: 'text'
    },
    quotation: {
      model: 'quotation',
      columnName: 'quotation_id'
    },
		quotation_hash: {
      type: 'string'
    },
    data: {
      type: 'string'
    },
    attempts: {
      type: 'number',
      defaultsTo: 0
    },
    user: {
      model: 'user',
      columnName: 'user_id'
    },
    company: {
      model: 'company',
      columnName: 'company_id'
    },
	},
};

