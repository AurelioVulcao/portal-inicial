angular.module('vencorr').controller('LinkedinCallbackController', function($scope, $routeParams, LoginService, $location, config) {
    $scope.loading = false;
    $scope.errorAuth = false;
    $scope.errorUser = false;
    $scope.loginPath = config.loginPath;

    var oauth2Code = $routeParams.code;

    if (!oauth2Code) {
        $scope.loading = false;
        $scope.errorAuth = true;
        return;
    }

    $scope.loading = true;
    LoginService.linkedinAuth(oauth2Code).then(function(resp) {
        var linkedinEmail = resp.data.email;
        $scope.pickedEmail = resp.data.email;

        LoginService.socialAuthenticate(linkedinEmail).then(function(response) {
            LoginService.storageLogin(response.data);
			window.location.href = '/dashboard';
        }).catch(function(err) {
            $scope.errorUser = true;
            $scope.loading = false;
        });
    }).catch(function(err) {
        $scope.loading = false;
        $scope.errorAuth = true;
    });
});