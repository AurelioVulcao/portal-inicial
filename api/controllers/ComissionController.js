const moment = require('moment');

async function list(req, res) {
    const { base64encode, base64decode } = require('nodejs-base64');
    const tokenData = UtilService.getTokenData(req);
    const { params } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
    }

    
    try {

        let companyUsersData = await User.find({
            where: {
                company: tokenData.company_id
            },
            select: ['id']
        });
    
        const usersIds = companyUsersData.map(function(item) {
            return item.id;
        });

        const filter = {
            user: usersIds,
        }

        if (params.start && params.end) {
            const start = moment(params.start, 'DD/MM/YYYY"').format('YYYY-MM-DD');
            const end = moment(params.end, 'DD/MM/YYYY"').format('YYYY-MM-DD');

            filter.paid_date = { '>=': start, '<=': end }
        }

        if (params.status) {
            filter.status = params.status;
        }

        const totals = {
            received: 0,
            pending: 0,
            volume: 0
        };

        const comissions = await Comission.find(filter).populate('quotation');

        comissions.forEach(async function(item) {
            if (item && item.quotation && item.quotation.customer_snapshot) {
                item.customer = JSON.parse(item.quotation.customer_snapshot);
                item.customer.encodedId = base64encode(item.customer.id);
            }
            
            if (item && item.quotation && item.quotation.segment_type) {
                if (item.quotation.segment_type === 'agro') {
                    item.segment_name = 'Agro';
                }

                if (item.quotation.segment_type === 'auto') {
                    item.segment_name = 'Auto';
                }

                if (item.quotation.segment_type === 'construcao') {
                    item.segment_name = 'Construção';
                }

                if (item.quotation.segment_type === 'residencial') {
                    item.segment_name = 'Residencial';
                }

                if (item.quotation.segment_type === 'empresarial') {
                    item.segment_name = 'Empresarial';
                }
            }

            item.status === 'received' ? totals.received += item.price : 0;
            item.status === 'pending' ? totals.pending += item.price : 0;
            totals.volume += item.price;
        });

        res.status(200).json({
            comissions,
            totals
        });
    } catch(err) {
        res.status(500).json({
            err: err
        });
    }
}

async function updateStatus(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { comission_id } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    if (!comission_id) {
        res.status(400).json({
            message: 'comission not reachable'
        });
        return;
    }

    try {
        const todayDate = moment().format('YYYY-MM-DD');
        const updatedComission = await Comission.updateOne({ id: comission_id }).set({ status: 'received', confirm_date: todayDate});
        res.status(200).json(updatedComission);
    } catch(err) {
        res.status(500).json(err);
    }
}

module.exports = {
    updateStatus,
    list
};

