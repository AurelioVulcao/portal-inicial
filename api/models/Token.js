module.exports = {
  attributes: {
		token: {
			type: 'ref',
			columnType: 'VARCHAR(500)'
		},
		status_code: {
			type: 'boolean'
		},
		user: {
			model: 'user',
			columnName: 'user_id'
		},
		extra: {
			type: 'ref',
			columnType: 'text'
		}
	}
};

