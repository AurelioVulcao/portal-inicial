const axios = require('axios');

async function sendLead(name, phone, email, investing, state, city, has_capital, contact_pref) {
    const url = 'https://api.rd.services/platform/events';
    const data = {
        event_type: 'CONVERSION',
        event_family: 'CDP',
        payload: {
            name: name,
            email: email,
            state: state,
            city: city,
            mobile_phone: phone,
            cf_investimento: investing,
            cf_possui_capital_disponivel: has_capital,
            cf_preferencia_de_contato: contact_pref,
            traffic_source: 'https://vencorr.com/franquias',
            conversion_identifier: 'https://vencorr.com/franquias'
        }
    };


    const accessToken = await refreshToken();
    const request = await axios.post(url, data, { headers: { Authorization: 'Bearer ' + accessToken }  });

    if (!request.errors) {
        return request.data;
    } else {
        return request.errors;
    }
}

async function refreshToken() {
    const url = 'https://api.rd.services/auth/token';
    const data = {
        client_id: '6eca382c-a049-4464-8e5a-431db7dcaa1d',
        client_secret: '6885b95ce5f546599a760e50cbaeeda6',
        refresh_token: 'XMymrM7i-gQ9UJKLRutbu6g-UJzM_XvTvfsAhniWwE4'
    };

    const request = await axios.post(url, data);
    return request.data.access_token;
}

module.exports = {
    sendLead
}