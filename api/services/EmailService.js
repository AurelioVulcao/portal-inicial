const nodemailer = require("nodemailer");
const fs = require('fs');
const externalSender = '"Vencorr" <seguros@vencorr.com>';
const contactSender = '"Vencorr Contato" <contato@vencorr.com>';

async function send(filename, email) {
    let quotationPath = filename;
    let bannerAssodeerPath = 'docs/banner_assodeere.pdf';

    return await prepare(quotationPath, bannerAssodeerPath, email);
}

async function notifyError(to, message) {
    if (!to) {
        return {
            status: 'error',
            err: 'missing email'
        };
    }

    let template = "<img src='https://proposals-vsales.s3.amazonaws.com/logos/vencorr.png'>" +
    "<h2>Foi encontrado 3 falhas em um processo</h2>" +
    "<h4>Detalhes abaixo</h4>" +
    "<p>ID da fila: " + message.queue_id + "</p>" +
    "<p>Hash da cotação: " + message.quotation_hash + "</p>" +
    "<p>Stacktrace: " + message.stack + "</p>";

    let transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'seguros@vencorr.com',
			pass: 'Vnz@1122'
		}
    });

    try {
        let info = await transporter.sendMail({
            from: 'seguros@vencorr.com',
            to: to,
            subject: 'Falha de robô por mais de 3 tentativas - Portal Franqueado',
            html: template
        });

        return {
            status: true,
            data: info.messageId
        };
    } catch(err) {
        return {
            status: 'error',
            err: err
        }
    }
}

async function externalEmail(to, name, phone, message) {
    if (!to) {
        return {
            status: 'error',
            err: 'missing email'
        };
    }

    let templateCustomer = "<img src='https://proposals-vsales.s3.amazonaws.com/logos/vencorr.png'>" +
    "<h2>Vencorr</h2>" +
    "<h4>Sua mensagem foi enviada com sucesso, logo nossa equipe irá retornar o contato</h4>" +
    "<h5>Nome: " + (name || '') + "</h5>" +
    "<h5>Telefone: " + (phone || '') + "</h5>" +
    "<h5>Mensagem: " + message + "</h5>";

    let templateVencorr = "<img src='https://proposals-vsales.s3.amazonaws.com/logos/vencorr.png'>" +
    "<h2>Contato do site</h2>" +
    "<h4>Dados do contato:</h4>" +
    "<h5>Nome: " + (name || '') + "</h5>" +
    "<h5>Telefone: " + (phone || '') + "</h5>" +
    "<h5>Mensagem: " + message + "</h5>";

    const emailCompanyResult = await contactTransport(contactSender, contactSender, 'Vencorr - Contato do site | ' + name, templateVencorr);
    const emailCustomerResult = await contactTransport(contactSender, to, 'Vencorr - Contato do site | ' + name, templateCustomer);

    if (emailCompanyResult.status === 'error') {
        return {
            status: 'error',
            err: emailCompanyResult.err
        };
    }

    if (emailCustomerResult.status === 'error') {
        return {
            status: 'error',
            err: emailCustomerResult.err
        };
    }

    return {
        status: true,
        data: 'ok'
    };
}

async function externalInformativeEmail(client_name, recipients, period, policy_number, policy_url, bank_slip) {
    const path = require('path');
    let bankSlipTemplate = '';
    let template = fs.readFileSync(path.resolve(__dirname, '../templates/email-boleto.html'), 'utf-8');

    template = template.replace('@clientName', client_name);
    template = template.replace('@policyNumber', policy_number);
    template = template.replace('@policyUrl', policy_url);
    template = template.replace('@period', period);

    if (bank_slip && bank_slip.length) {
        for (let [index, bill] of bank_slip.entries()) {
            bankSlipTemplate += '<div style="height: 64px;"></div>';
            bankSlipTemplate += '<div style="display: inline-flex;">';
            bankSlipTemplate += '<a href="'+ bill.url +'"><img src="https://portal-franqueado.s3.amazonaws.com/hosted-images/icon-boleto.png" style="width: 64px;height: 52px;margin-top: 50px;"></a>';
            bankSlipTemplate += '<div style="width: 32px;"></div>';
            bankSlipTemplate += '<div style="display: grid;">';
            bankSlipTemplate += '<p style="margin-bottom: 0px;font-size: 24px;font-weight: 300;font-stretch: normal;font-style: normal;line-height: normal;letter-spacing: normal;color: #0251ac;font-family: sans-serif;">';
            bankSlipTemplate += 'Boleto ' + (index + 1) + '';
            bankSlipTemplate +=  '</p>';
            bankSlipTemplate += '<p style="margin-bottom: 0px;font-size: 18px;font-weight: bold;font-stretch: normal;font-style: normal;line-height: normal;letter-spacing: normal;color: #0251ac;font-family: sans-serif;">';
            bankSlipTemplate += 'Valor: ' + bill.price + '';
            bankSlipTemplate += '</p>';
            bankSlipTemplate += '<p style="margin-bottom: 0px;font-size: 18px;font-weight: bold;font-stretch: normal;font-style: normal;line-height: normal;letter-spacing: normal;color: #0251ac;font-family: sans-serif;">';
            bankSlipTemplate += 'Vencimento: ' + bill.due_date + '';
            bankSlipTemplate += '</p></div></div>';
        }

        template = template.replace('@boletos', bankSlipTemplate);
    } else {
        template = template.replace('@boletos', '');
    }


    const emailSendReport = await genericTransport(externalSender, recipients.toString(), 'Vencorr - Apólice e Boletos', template);

    if (emailSendReport.status === 'error') {
        return {
            status: 'error',
            err: emailSendReport.err
        };
    }

    return {
        status: true,
        data: 'ok'
    };
}

async function genericTransport(from, to, subject, template) {
    let transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'seguros@vencorr.com',
			pass: 'Vnz@1122'
		}
    });

    try {
        let info = await transporter.sendMail({
            from: from,
            to: to,
            subject: subject,
            html: template
        });

        return {
            status: true,
            data: info.messageId
        };
    } catch(err) {
        return {
            status: 'error',
            err: err
        }
    }
}

async function contactTransport(from, to, subject, template) {
    let transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'contato@vencorr.com',
			pass: 'Vnz@1122'
		}
    });

    try {
        let info = await transporter.sendMail({
            from: from,
            to: to,
            subject: subject,
            html: template
        });

        return {
            status: true,
            data: info.messageId
        };
    } catch(err) {
        return {
            status: 'error',
            err: err
        }
    }
}

async function prepare(quotationFile, bannerAssoderFile, email) {
    let template = "<h2>Proposta compartilhada via Vsales</h2>" +
	"<h4>Os arquivos encontram-se em anexo</h4>";

	let transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'seguros@vencorr.com',
			pass: 'Vnz@1122'
		}
    });
    
    try {
        let info = await transporter.sendMail({
            from: '"VSALES" <seguros@vencorr.com>',
            to: email,
            subject: "VSALES - Proposta compartilhada via Vsales ✔",
            html: template,
            attachments: [{
                filename: 'proposta.pdf',
                path: quotationFile,
                contentType: 'application/pdf'
            },
            {
                filename: 'banner_assodeerre.pdf',
                path: bannerAssoderFile,
                contentType: 'application/pdf'
            }],
        });

        await deleteFiles(quotationFile);
        return {
            status: true,
            data: info.messageId
        };
    } catch(err) {
        await deleteFiles(quotationFile);
        return {
            status: false,
            err: err
        };
    };
}

async function deleteFiles(filePath) {
	return new Promise((resolve, reject) => {
		fs.unlink(filePath, (err) => {
			if (err) {
				reject('error delete file: ' + err);
			}
			resolve();
		});
	});
}

async function adminTransport(from, to, subject, template) {
    let transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'adm@vencorr.com',
			pass: '4200vencorr@'
		}
    });

    try {
        let info = await transporter.sendMail({
            from: from,
            to: to,
            subject: subject,
            html: template
        });

        return {
            status: true,
            data: info.messageId
        };
    } catch(err) {
        return {
            status: 'error',
            err: err
        }
    }
}

async function quotationRequest(customerName, companyName, segment, url) {
    const path = require('path');
    const moment = require('moment');
    const time = moment().format('DD/MM/YYYY hh:mm');
    let template = fs.readFileSync(path.resolve(__dirname, '../templates/quotation-request.html'), 'utf-8');

    template = template.replace('@usuario', customerName);
    template = template.replace('@empresa', companyName);
    template = template.replace('@seguimento', segment);
    template = template.replace('@solicitacaoUrl', url);
    
    const emailSendReport = await adminTransport('adm@vencorr.com', 'adm@vencorr.com', 'Portal Franqueado | Solicitação Cotação ' + customerName + ' - ' + time, template);

    if (emailSendReport.status === 'error') {
        return {
            status: 'error',
            err: emailSendReport.err
        };
    }

    return {
        status: true,
        data: 'ok'
    };
}

async function quotationChosen(customerName, companyName, segment, url) {
    const path = require('path');
    const moment = require('moment');
    const time = moment().format('DD/MM/YYYY hh:mm');
    let template = fs.readFileSync(path.resolve(__dirname, '../templates/quotation-choose.html'), 'utf-8');

    template = template.replace('@usuario', customerName);
    template = template.replace('@empresa', companyName);
    template = template.replace('@seguimento', segment);
    template = template.replace('@solicitacaoUrl', url);
    
    const emailSendReport = await adminTransport('adm@vencorr.com', 'adm@vencorr.com', 'Portal Franqueado | Cotação escolhida ' + customerName + ' - ' + time, template);

    if (emailSendReport.status === 'error') {
        return {
            status: 'error',
            err: emailSendReport.err
        };
    }

    return {
        status: true,
        data: 'ok'
    };
}

async function injuryOpened(customerName, companyName, url) {
    const path = require('path');
    const moment = require('moment');
    const time = moment().format('DD/MM/YYYY hh:mm');
    let template = fs.readFileSync(path.resolve(__dirname, '../templates/injury-opened.html'), 'utf-8');

    template = template.replace('@usuario', customerName);
    template = template.replace('@empresa', companyName);
    template = template.replace('@solicitacaoUrl', url);
    
    const emailSendReport = await adminTransport('adm@vencorr.com', 'adm@vencorr.com', 'Portal Franqueado | Sinistro Aberto' + customerName + ' - ' + time, template);

    if (emailSendReport.status === 'error') {
        return {
            status: 'error',
            err: emailSendReport.err
        };
    }

    return {
        status: true,
        data: 'ok'
    };
}

async function franchiseeSiteEmail(name, phone, email, investing, state, city, has_capital, contact_pref) {
    let template = "<img src='https://proposals-vsales.s3.amazonaws.com/logos/vencorr.png'>" +
    "<h2>Contato do site Franquias</h2>" +
    "<h4>Dados do contato:</h4>" +
    "<h5>Nome: " + (name || '') + "</h5>" +
    "<h5>Telefone: " + (phone || '') + "</h5>" +
    "<h5>Email: " + email + "</h5>" +
    "<h5>Investimento: " + investing + "</h5>" +
    "<h5>Estado: " + state + "</h5>" +
    "<h5>Cidade: " + city + "</h5>" +
    "<h5>Já tem o capital?: " + has_capital + "</h5>" +
    "<h5>Contato de preferência: " + contact_pref + "</h5>";

    const emailSent = await contactTransport(externalSender, ['victor.guerra@vencorr.com', 'diego.nascimento@vencorr.com'], 'Contato Site Franquias | ' + name + '', template);

    if (emailSent.status === 'error') {
        return {
            status: 'error',
            err: emailSent.err
        };
    }

    return {
        status: true,
        data: 'ok'
    };
}

async function supportOfflineEmail(subject, message, name, email, phone, company_name) {
    const moment = require('moment');
    const time = moment().format('DD/MM/YYYY hh:mm');
    let template = "<img src='https://proposals-vsales.s3.amazonaws.com/logos/vencorr.png'>" +
    "<h2>Solicitação de suporte - portal franqueado</h2>" +
    "<h4>Dados do contato:</h4>" +
    "<h5>Nome: " + (name || '') + "</h5>" +
    "<h5>Telefone: " + (phone || '') + "</h5>" +
    "<h5>Email: " + email + "</h5>" +
    "<h5>Empresa: " + company_name + "</h5>" +
    "<h5>Assunto: " + subject + "</h5>" +
    "<h5>Mensagem: " + message + "</h5>";

    const emailSent = await adminTransport('adm@vencorr.com', ['aline.barrinuevo@vencorr.com', 'humberto.biase@vencorr.com', 'victor.guerra@vencorr.com'], 'Suporte | Portal do Franqueado ' + name + ' - ' + time, template);

    if (emailSent.status === 'error') {
        return {
            status: 'error',
            err: emailSent.err
        };
    }

    return {
        status: true,
        data: 'ok'
    };
}

async function directMessageEmail(client_name, company_name, message, files, solicitacaoUrl) {
    const path = require('path');
    let template = fs.readFileSync(path.resolve(__dirname, '../templates/direct-message.html'), 'utf-8');
    const moment = require('moment');
    const time = moment().format('DD/MM/YYYY hh:mm');
    let filesTemplate = '';

    template = template.replace('@usuario', client_name);
    template = template.replace('@empresa', company_name);
    template = template.replace('@mensagem', message);
    template = template.replace('@solicitacaoUrl', solicitacaoUrl);
    
    if (files && files.length) {
        filesTemplate = '<h3 style="margin-bottom: 0px; margin-top: 50px; margin-left: 10%;margin-right: 10%;font-size: 18px;font-weight: bold;color: #484848;font-family: sans-serif;font-stretch: normal;font-style: normal;line-height: normal;letter-spacing: normal;">Arquivos :</h3>';
        filesTemplate += '<ul>';
        
        for (file of files) {
            filesTemplate += '<li>' + file.label + ': ' + '<a href="' + file.url + '">CLIQUE AQUI PARA VER</a></li>';
        }

        filesTemplate += '</ul>';
        template = template.replace('@arquivos', filesTemplate);
    } else {
        template = template.replace('@arquivos', '');
    }

    const emailSent = await adminTransport('adm@vencorr.com', 'adm@vencorr.com', 'Mensagem direta | Franqueado ' + client_name + ' - ' + time, template);

    if (emailSent.status === 'error') {
        return {
            status: 'error',
            err: emailSent.err
        };
    }

    return {
        status: true,
        data: 'ok'
    };
}

module.exports = {
    externalInformativeEmail,
    supportOfflineEmail,
    franchiseeSiteEmail,
    contactTransport,
    quotationRequest,
    quotationChosen,
    externalEmail,
    injuryOpened,
    notifyError,
    directMessageEmail,
    send
}