const cepPromise = require('cep-promise');

async function getInfoByCep(cep) {
    return new Promise((resolve, reject) => {
        cepPromise(cep).then((resp) => {
            resolve({
                status: 200,
                data: resp
            });
        }).catch((err) => {
            reject({
                status: 500,
                data: err
            });
        });
    });
}

module.exports = {
    getInfoByCep
}