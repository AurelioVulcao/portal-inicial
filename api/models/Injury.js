module.exports = {
	attributes: {
		complainant: {
			type: 'string'
		},
		description: {
			type: 'ref',
			columnType: 'text'
		},
		status: {
			type: 'string'
		},
		event_date: {
			type: 'ref',
			columnType: 'date'
		},
		type: {
			type: 'string'
		},
		customer: {
			model: 'customer',
			columnName: 'customer_id'
		},
		quotation: {
			model: 'quotation',
			columnName: 'quotation_id'
		},
		files: {
			type: 'ref',
			columnType: 'text'
		},
		resolution_description: {
			type: 'ref',
			columnType: 'text'
		},
		resolution_files: {
			type: 'ref',
			columnType: 'text'
		},
		user: {
			model: 'user',
			columnName: 'user_id'
		},
		company: {
			model: 'company',
			columnName: 'company_id'
		},
	},
};
