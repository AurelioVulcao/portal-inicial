module.exports = {
  attributes: {
    description: {
			type: 'string'
    },
    slug: {
      type: 'string'
    },
		address: {
			type: 'string'
    },
    status: {
			type: 'string'
    },
    queue: {
      model: 'queue',
      columnName: 'queue_id'
    },
	},
};

