angular.module('vencorr').controller('QuotationEquipmentController', function($scope, $routeParams, uuid, $rootScope, $location, config, QuotationService, SweetAlert, CustomerService, Upload, MetaService) {
    $scope.loading = false;
    $scope.isEdit = false;
    $scope.hash = $routeParams.hash;
    $scope.customerSnapshot = {};
    $scope.loadingCustomer = false;
    $scope.searchCustomer = {};
    $scope.chooseCustomer = false;
    $scope.cepLoading = false;
    $scope.quotationMissingFields = [];
    $scope.missingModalField = [];
    $scope.currentEquipment = {
        coverages: [
            { 
                'name':'Básica',
                'status': false,
                'lmi': null
            },
            { 
                'name':'Danos elétricos ',
                'status': false,
                'lmi': null
            },
            { 
                'name':'Responsabilidade Civil',
                'status': false,
                'lmi': null
            },
            { 
                'name':'Furto simples',
                'status': false,
                'lmi': null
            },
            { 
                'name':'Obstáculos em solo',
                'status': false,
                'lmi': null
            },
            { 
                'name':'Içamento',
                'status': false,
                'lmi': null
            },
            { 
                'name':'Perda Pagamento Aluguel',
                'status': false,
                'lmi': null
            },
        ]
    };

    $scope.quotation = {
        files: [],
        equipments: []
    };
    $scope.dynamicPopover = {
        basic : {
            title: 'Na cobertura básica está incluido:',
            content: 'Incendio, Explosão, Raio, Roubo e furto qualificado, Tombamento, Colisão, Transporte e Salvamento'
        },
    };

    function init() {
        if ($scope.hash) {
            $scope.isEdit = true;
            getQuotationData($scope.hash);
		} else{
            setPeriod();
            setInitialValues();
        }
		
		// Load pre data customer form
		if (!$scope.quotation.customer) {
			$scope.quotation.customer = {};
			$scope.quotation.customer.type = 'fisica';
			$scope.quotation.customer.genre = 'masculino';
			$scope.quotation.customer.rg_uf = 'SP';
		}
    }

    function getQuotationData() {
        $scope.loading = true;
        QuotationService.get($scope.hash).then(function(resp) {
            var quotationResp = resp.data;

            if (quotationResp && quotationResp.extra) {
                var receivedQuotation = JSON.parse(resp.data.extra);
                
                receivedQuotation.status = resp.data.status;
                receivedQuotation.hash = resp.data.hash;
                receivedQuotation.obs = receivedQuotation.obs;
                receivedQuotation.id = resp.data.id;
                receivedQuotation.message = resp.data.message;
                receivedQuotation.start_term = resp.data.start_term;
                receivedQuotation.end_term = resp.data.end_term;
                receivedQuotation.info = JSON.parse(resp.data.info);
                $scope.customerSnapshot = JSON.parse(resp.data.customer_snapshot);
                $scope.quotation = formatAfterGet(receivedQuotation);

                if ($scope.quotation.status == 'accepted') {
                    $scope.quotation.info.forEach(function(quote) {
                        if (quote.accept) {
                            $scope.acceptedQuote = quote;
                        }
                    });
                }
            }

            $scope.loading = false;
        }).catch(function(err) {
            console.log(err);
            $scope.loading = false;
            SweetAlert.error('Falha ao consultar cotação', 'Verifique se esta cotação existe no menu [Cotações]');
        });
    }

    function formatAfterGet(quotationToFormat) {
        quotationToFormat.start_term = moment(quotationToFormat.start_term, 'YYYY-MM-DD').format('DD/MM/YYYY');
        quotationToFormat.end_term = moment(quotationToFormat.end_term, 'YYYY-MM-DD').format('DD/MM/YYYY');
        
        return quotationToFormat; 
    }
	
	function onSelectCustomer(customer) {
		$scope.customerSnapshot = customer;
        $scope.quotation.customerId = customer.id;
		$scope.chooseCustomer = false;
    }
    
    function cancel() {
        SweetAlert.swal({
            title: 'Cancelar a cotação?',
            text: 'Uma vez cancelada, não é possível voltar atrás, tem certeza?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Cancelar Cotação',
            cancelButtonText: 'Voltar',
            confirmButtonColor: '#ce1a1a'
         }, function(isConfirm) {
             if (isConfirm) {
                QuotationService.updateStatus({ 
                    quotation_id: $scope.quotation.id,
                    status: 'canceled' })
                .then(function(resp) {
                    console.log('CHEGOU AQUI')
                    getQuotationData($scope.hash);
                });
             }
         });
    }

    function closeModal() {
		$('#addEquipmentModal').modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
    }

    function validateQuotation(quotation) {
        var missingFields = [];
        var fields = [
            'start_term',
            'end_term',
            'customerId',
        ];

        fields.forEach(function(key) {
            if (!$scope.quotation[key]) {
                missingFields.push(key);
            }
        });

        if (missingFields.length) {
            $scope.quotationMissingFields = missingFields;
            SweetAlert.warning('Preencha todos os campos obrigatórios', 'Estão maracados com um *');
            return false;
        } else {
            return true;
        }
    }

    function isMissingFields(key) {
        return $scope.quotationMissingFields.indexOf(key) !== -1 ? true : false;
    }

    function isMissingFieldsModal(key) {
        return $scope.missingModalField.indexOf(key) !== -1 ? true : false;
    }
    
    function validateCustomer(customer) {
		var missingFields = [];
		var fields = { 
			'name': 'Nome',
			'cpf': 'CPF',
			'birthdate': 'Data de Nascimento',
			'rg_number': 'RG',
			'rg_org': 'Rg Orgão Expeditor',
			'rg_uf': 'RG UF',
			'rg_emission_date': 'RG Data de Emissão',
			'cnpj': 'CNPJ',
			'email': 'Email',
			'phone': 'Telefone',
			'cep': 'CEP',
			'state': 'Estado',
			'city': 'Cidade',
			'address': 'Endereço',
			'number': 'Número' };

		Object.keys(fields).forEach(function(key) {
			if (!customer[key]) {
				if (key === 'cnpj' && customer.type === 'fisica') {
					return;
				}

				if (key.includes('rg') && customer.type === 'juridica') {
					return;
				}

				if (key === 'birthdate' && customer.type === 'juridica') {
					return;
				}

				if (key === 'genre' && customer.type === 'juridica') {
					return;
				}

				if (key === 'cpf' && customer.type === 'juridica') {
					return;
				}

				missingFields.push(fields[key]);
			}
		});

		missingFields = missingFields.join(', ');

		if (missingFields.length) {
			SweetAlert.warning('Preencha todos os campos obrigatórios', missingFields.toString());
			return false;
		} else {
			return customer;
		}
    }
    
    function saveCustomer() {
		var customer = JSON.parse(JSON.stringify($scope.quotation.customer));
		var validatedCustomer = validateCustomer(customer);

		if (!validatedCustomer) {
			return;
		}

		$scope.loadingCustomer = true;
		CustomerService.save({ customer: customer }).then(function(resp) {
            $scope.customerSnapshot = resp.data;
            $scope.quotation.customerId = resp.data.id;
            $scope.loadingCustomer = false;
            resetCustomer();
            closeCustomerModal();
		}).catch(function(err) {
			SweetAlert.info('Cliente já existe', 'Um cliente com este EMAIL ou CPF/CNPJ, já existe, verifique por favor.');
			$scope.loadingCustomer = false;
		});
    }

    function resetCustomer() {
        $scope.quotation.customer = {};
        $scope.quotation.customer.type = 'fisica';
        $scope.quotation.customer.genre = 'masculino';
        $scope.quotation.customer.rg_uf = 'SP';
    }

    function closeCustomerModal() {
		$('#customerModal').modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
    }

    function getAddressByCep(cep) {
        if(cep && cep.length > 0){
            $scope.cepLoading = true;
            CustomerService.getAdressByCep(cep).then(function(resp) {
                var response = resp.data;

                $scope.quotation.customer.city = response.city;
                $scope.quotation.customer.address = response.street + ' - ' + response.neighborhood;
                $scope.quotation.customer.cep = response.cep;
                $scope.quotation.customer.state = response.state;

                if (!response.street || !response.neighborhood) {
                    $scope.quotation.customer.address.street = '';
                }

                $scope.cepLoading = false;
            }).catch(function(err) {
                $scope.cepLoading = false;
                SweetAlert.warning("Ops! Não encontramos o CEP digitado", 'Utilizamos 2 serviços para busca de CEP e não encontramos, verifique se você realmente digitou corretamente ou complete manualmente o endereço')
            });
        }
    }
    
    function enableCustomerSearch() {
		$scope.chooseCustomer = true;
		$scope.searchCustomer.value = null;
	}

	function disableCustomerSearch() {
		$scope.searchCustomer.value = '';
		$scope.chooseCustomer = false;
	}

	function getCustomers(typedTerm) {
		return CustomerService.suggest(typedTerm).then(function(resp) {
			return resp.data.map(function(item) {
				return item;
			});
		});
	};

	function editCustomerData() {
		$window.location.href = '/cliente-cadastrar/' +  $base64.encode($scope.quotation.customer.id);
    }
    
    function setInitialValues() {
        $scope.quotation.equipment_type = 'agro';
        $scope.quotation.financed = 'sim';
        $scope.quotation.third_party_use = 'sim';
    }

    function setPeriod() {
        $scope.quotation.start_term = moment().format('DD/MM/YYYY');
        $scope.quotation.end_term = moment().add(1, 'year').format('DD/MM/YYYY');
    }

    function triggerUpload(label) {
        $scope.currentImgLabel = label;
        document.getElementById('imageUpload').click();
    };

    function uploadFiles(files, errFiles) {
        $scope.uploadingFiles = files;
        $scope.errFiles = errFiles;
        $scope.finished = 0;
        $scope.hideProgressBars = false;

        angular.forEach(files, function(file, index) {
            file.upload = Upload.upload({
                url: config.uploadEndpoint,
                data: { file: file,'folder': 'docs' },
            });

            file.upload.then(function (response) {
                $scope.quotation.files.push({
                    url: response.data[0],
                    label: 'Arquivo ' + ($scope.quotation.files.length + 1)
                });
                
                $scope.finished++;

                if ($scope.finished == files.length) {
                    $scope.hideProgressBars = true;
                }
            }, function (response) {
                console.log(response.status + ': ' + response.data);
                SweetAlert.warning('Ocorreu um erro com um dos arquivos', 'Se o problema persistir, tente usar outro arquivo ou informe ao suporte.');
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        });
    }

    function removeDoc(url) {
        if (!$scope.quotation.files.length) {
            return;
        }

        $scope.quotation.files = $scope.quotation.files.filter(function(doc) {
            return url != doc.url;
        });
    }

    function removeItem(index, item) {
        $scope.quotation.equipments.splice(index, 1);
    }

    function duplicateItem(item) {
        var itemCopy = JSON.parse(JSON.stringify(item));

        itemCopy.uuid = uuid.v4();
        $scope.quotation.equipments.push(itemCopy);
    }

    function editItem(index, item) {
        $scope.modalEditing = true;
        $scope.currentEquipment = item;

        $('#addEquipmentModal').modal({ show: true })
    }

    function checkRequiredInfo(quotation) {
        var isValidQuotation = validateQuotation(quotation);

        if (!isValidQuotation) {
            return;
        }

        if (!$scope.quotation.equipments.length) {
            SweetAlert.warning('Adicione pelo menos 1 equipamento', 'Na seção "Adicionar Equipamentos" clique no + para adicionar um equipamento');
            return;
        }

        if (!$scope.quotation.files.length) {
            SweetAlert.swal({
                title: 'Deseja enviar sem anexar documentos?',
                text: 'Talvez você tenha esquecido de anexar algum documento, mas você pode continuar mesmo assim',
                type: 'info',
                showCancelButton: true,
                confirmButtonText: 'Enviar assim mesmo',
                cancelButtonText: 'Cancelar'
            }, function(isConfirm) {
                if (isConfirm) {
                    save();
                } else {
                    return;
                }
            });
        } else {
            save();
        }
    }

    function save() {
        $scope.loading = true;

        var formattedItems = '';
        
        $scope.quotation.equipments.forEach(function(item, index) {
            if (($scope.quotation.equipments.length -1) == index) {
                formattedItems += item.model_description;
            } else {
                formattedItems += item.model_description + ', ';
            }
        });

        QuotationService.save({
            quotation_info: $scope.quotation,
            customer_snapshot: JSON.stringify($scope.customerSnapshot),
            segment: 'equipamento',
            customer_id: $scope.quotation.customerId,
            start_term: moment($scope.quotation.start_term, 'DD/MM/YYYY').format('YYYY-MM-DD'),
            end_term:  moment($scope.quotation.end_term, 'DD/MM/YYYY').format('YYYY-MM-DD'),
            items: formattedItems
        }).then(function(resp){
            // Send email
            MetaService.solicitationRequestEmail({
                customer_name: $rootScope.loggedUser.name,
                company_name: $rootScope.loggedUser.company.name,
                segment: 'Equipamento',
                hash: resp.data.hash
            }).then(function(resp) {
                $scope.loading = false;
                console.log(resp.data);
            });

            $location.path('/cotacoes');
        }).catch(function(err) {
            console.log(err);
            $scope.loading = false;
            SweetAlert.error('Ops!', 'Ocorreu um erro inesperado, tente novamente, se o erro persistir, entre em contato com o suporte');
        });
    }

    function updateQuotation() {
        $scope.loading = true;
        var formattedItems = '';
        
        $scope.quotation.equipments.forEach(function(item, index) {
            if (($scope.quotation.equipments.length -1) == index) {
                formattedItems += item.model_description;
            } else {
                formattedItems += item.model_description + ', ';
            }
        });

        QuotationService.updateGeneric({ 
            extra: JSON.stringify($scope.quotation),
            quotation_id: $scope.quotation.id,
            customer_snapshot: JSON.stringify($scope.customerSnapshot) || null,
            customer: $scope.customerSnapshot.id || null,
            start_term: moment($scope.quotation.start_term, 'DD/MM/YYYY').format('YYYY-MM-DD') || null,
            end_term:  moment($scope.quotation.end_term, 'DD/MM/YYYY').format('YYYY-MM-DD') || null,
            items: formattedItems
        })
        .then(function(resp) {
            getQuotationData($scope.hash);
        }).catch(function(err) {
            $scope.loading = false;
        });
    }

    function cancel() {
        SweetAlert.swal({
            title: 'Cancelar a cotação?',
            text: 'Uma vez cancelada, não é possível voltar atrás, tem certeza?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Cancelar Cotação',
            cancelButtonText: 'Voltar',
            confirmButtonColor: '#ce1a1a'
         }, function(isConfirm) {
             if (isConfirm) {
                $scope.loading = true;
                QuotationService.updateStatus({ 
                    quotation_id: $scope.quotation.id,
                    status: 'canceled' })
                .then(function(resp) {
                    getQuotationData($scope.hash);
                }).catch(function(err) {
                    $scope.loading = false;
                });
             }
         });
    }

    function accept(quote) {
        SweetAlert.swal({
            title: 'Aceitar esta cotação?',
            text: 'Selecionando esta cotação, você aceita prosseguir com o fechamento do seguro no valor e observações descritas.',
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Aceitar',
            cancelButtonText: 'Voltar',
            confirmButtonColor: '#0E52AC'
         }, function(isConfirm) {
             if (isConfirm) {
                $scope.loading = true;
                quote.accept = true;

                QuotationService.updateGeneric({ 
                    quotation_id: $scope.quotation.id,
                    status: 'accepted',
                    info: JSON.stringify($scope.quotation.info),
                    price: quote.price
                })
                .then(function(resp) {
                    MetaService.quotationChosenEmail({
                        customer_name: $rootScope.loggedUser.name,
                        company_name: $rootScope.loggedUser.company.name,
                        segment: 'Residencial',
                        hash: $scope.quotation.hash
                    }).then(function(resp) {
                        getQuotationData($scope.hash);
                        console.log(resp.data);
                    });
                }).catch(function(err) {
                    $scope.loading = false;
                });
             }
         });
    }

    function addEquipment(equipment) {
        $scope.missingModalField = [];

        if (!equipment.model_description) {
            $scope.missingModalField.push('model_description');
        }

        if (!equipment.price) {
            $scope.missingModalField.push('price');
        }

        if (!equipment.financed) {
            $scope.missingModalField.push('financed');
        }

        if (!equipment.third_party_use) {
            $scope.missingModalField.push('third_party_use');
        }

        if ($scope.missingModalField.length) {
            SweetAlert.warning('Preencha todos os campos', 'Os campos marcadaos com * precisam ser preenchidos');
            return;
        }

        if (equipment.coverages.length) {
            equipment.coverageDescription = '';
            equipment.coverages.forEach(function(item, index) {
                if (!item.lmi) {
                    return;
                }

                var contractedCoverages = equipment.coverages.filter(function(coverage) {
                    if (coverage.status) {
                        return coverage;
                    }
                });

                if ((contractedCoverages.length - 1) == index) {
                    if (!equipment.coverageDescription) {
                        equipment.coverageDescription = item.name + ': ' + 'R$ ' + item.lmi;
                        return;
                    }

                    equipment.coverageDescription += item.name + ': ' + 'R$ ' + item.lmi;
                } else {
                    if (!equipment.coverageDescription) {
                        equipment.coverageDescription = item.name + ': ' + 'R$ ' + item.lmi + ', ';
                        return;
                    }

                    equipment.coverageDescription += item.name + ': ' + 'R$ ' + item.lmi + ', ';
                }
            });
        }

        var copyEquipment = JSON.parse(JSON.stringify(equipment));

        if (equipment.uuid) {
            $scope.quotation.equipments.forEach(function(item, index) {
                if (item.uuid == copyEquipment.uuid) {
                    $scope.quotation.equipments.splice(index, 1);
                    $scope.quotation.equipments.push(copyEquipment);
                }
            });
            
            closeModal();
            return;
        }
        
        if (!equipment.uuid) {
            copyEquipment.uuid = uuid.v4();
            $scope.quotation.equipments.push(copyEquipment);
        }
        
        closeModal();
    }

    function clearCurrentEquipment() {
        $scope.currentEquipment.model_description = null;
        $scope.currentEquipment.price = null;
        $scope.currentEquipment.financed = 'sim';
        $scope.currentEquipment.third_party_use = 'nao';
        $scope.currentEquipment.coverageDescription = null;
        $scope.currentEquipment.uuid = null;

        $scope.currentEquipment.coverages.forEach(function(coverage) {
            coverage.status = false;
            coverage.lmi = null;
        });
    }

    function copyPriceBasicCoverage(price) {
        if (!price) {
            return;
        }
        
        $scope.currentEquipment.coverages[0].status = true;
        $scope.currentEquipment.coverages[0].lmi = price;
    }

    function capitalize(word) {
        if (!word) {
            return;    
        }
        
        return word.charAt(0).toUpperCase() + word.slice(1);
    }

    function clearValueLmi(coverage) {
        if (!coverage.status) {
            coverage.lmi = null;
        }
    }

    function handleCoverage(coverage) {
        $scope.quotation.equipments.push(coverage);
    }

    init();
    $scope.resetCustomer = resetCustomer;
    $scope.updateQuotation = updateQuotation;
    $scope.clearCurrentEquipment = clearCurrentEquipment;
    $scope.removeItem = removeItem;
    $scope.clearValueLmi = clearValueLmi;
    $scope.copyPriceBasicCoverage = copyPriceBasicCoverage;
    $scope.handleCoverage = handleCoverage;
    $scope.addEquipment = addEquipment;
    $scope.capitalize = capitalize;
    $scope.checkRequiredInfo = checkRequiredInfo;
    $scope.isMissingFieldsModal = isMissingFieldsModal;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.duplicateItem = duplicateItem;
    $scope.removeDoc = removeDoc;
    $scope.editCustomerData = editCustomerData;
	$scope.onSelectCustomer = onSelectCustomer;
	$scope.getCustomers = getCustomers;
	$scope.disableCustomerSearch = disableCustomerSearch;
    $scope.enableCustomerSearch = enableCustomerSearch;
    $scope.saveCustomer = saveCustomer;
    $scope.getAddressByCep = getAddressByCep;
    $scope.triggerUpload = triggerUpload;
    $scope.uploadFiles = uploadFiles;
    $scope.validateQuotation = validateQuotation;
    $scope.isMissingFields = isMissingFields;
    $scope.accept = accept;
    $scope.editItem = editItem;
});