angular.module('vencorr').directive('directMessages', function ($rootScope, UtilService, Upload, config, MessageService, MetaService, UtilService) {
    return {
        templateUrl: 'views/template-direct-messages.html',
        restrict: 'E',
        scope: {
            quotation: '='
        },
        link: function(scope, element, attrs) {
            scope.loading = false;
            scope.loadingMessages = false;
            scope.messages = [];
            scope.directMessage = {
                files: [],
            };

            $(document).on('hide.bs.modal','#' + scope.quotation.hash, function () {
                $rootScope.$broadcast('closed-direct-message-modal');
            });

            function formatSegmentName(segment) {
                return UtilService.formatSegmentName(segment);
            }

            function triggerUpload(label) {
                document.getElementById('imageUpload').click();
            };

            function uploadFiles(files, errFiles) {
                scope.uploadingFiles = files;
                scope.errFiles = errFiles;
                scope.finished = 0;
                scope.hideProgressBars = false;
                scope.loadingFiles = true;
        
                angular.forEach(files, function(file, index) {
                    file.upload = Upload.upload({
                        url: config.uploadEndpoint,
                        data: { file: file,'folder': 'docs' },
                    });
        
                    file.upload.then(function (response) {
                        if (!scope.directMessage.files) {
                            scope.directMessage.files = [];
                        }

                        scope.directMessage.files.push({
                            url: response.data[0],
                            label: response.config.data.file.name
                        });
                        
                        scope.finished++;
        
                        if (scope.finished == files.length) {
                            scope.hideProgressBars = true;
                            scope.loadingFiles = false;
                        }
                    }, function (response) {
                        console.log(response.status + ': ' + response.data);
                        SweetAlert.warning('Ocorreu um erro com um dos arquivos', 'Se o problema persistir, tente usar outro arquivo ou informe ao suporte.');
                    }, function (evt) {
                        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });
                });
            }

            function removeDoc(url) {
                if (!scope.directMessage.files.length) {
                    return;
                }
        
                scope.directMessage.files = scope.directMessage.files.filter(function(doc) {
                    return url != doc.url;
                });
            }

            function addMessage() {
                var directMessageObj = UtilService.clone(scope.directMessage);

                if (!directMessageObj.text || directMessageObj.text.length < 3) {
                    scope.error_message = 'Descreva melhor a sua mensagem';
                    return;
                }

                scope.error_message = '';
                if (!scope.messages || !scope.messages.length) {
                    directMessageObj.messages = [];
                } else {
                    directMessageObj.messages = scope.messages;
                }
                
                directMessageObj.id = scope.direct.id;
                directMessageObj.messages.push({
                    date: moment().format('DD/MM/YYYY HH:mm'),
                    text: directMessageObj.text,
                    files: directMessageObj.files,
                    user: { id: $rootScope.loggedUser.id, name: $rootScope.loggedUser.name }
                });

                delete directMessageObj.text;
                delete directMessageObj.files;
                directMessageObj.messages = JSON.stringify(directMessageObj.messages);

                scope.loading = true;
                MessageService.add(directMessageObj).then(function(resp) {
                    scope.directMessage = {};
                    getMessages(scope.quotation.message.id);

                    var messagesCopyJSON = JSON.parse(directMessageObj.messages);
                    var lastMessage = messagesCopyJSON[messagesCopyJSON.length - 1];

                    var dataEmail = {
                        hash: scope.quotation.hash,
                        client_name: $rootScope.loggedUser.name,
                        company_name: $rootScope.loggedUser.company.name,
                        message: lastMessage.text,
                        files: lastMessage.files
                    };

                    MetaService.directMessageEmail(dataEmail).then(function(reponse) {
                        console.log('ok');
                        scope.loading = false;
                    });

                }).catch(function(err) {
                    console.log(err);
                    scope.loading = false;
                    alert('Não foi possível carregar as mensagens');
                });
            }

            function getMessages(messageId) {
                scope.loadingMessages = true;

                MessageService.list(messageId).then(function(resp) {
                    scope.direct = resp.data;
                    scope.messages = JSON.parse(scope.direct.messages);
                    
                    setTimeout(function() {
                        var messageBody = document.querySelector('#directMessagesArea');
                        messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
                    }, 500); 
                    
                    scope.loadingMessages = false;
                }).catch(function(err) {
                    console.log(err);
                    scope.loadingMessages = false;
                    alert('Não foi possível carregar as mensagens');
                });
            }

            scope.addMessage = addMessage;
            scope.getMessages = getMessages;
            scope.removeDoc = removeDoc;
            scope.formatSegmentName = formatSegmentName;
            scope.triggerUpload = triggerUpload;
            scope.uploadFiles = uploadFiles;
        }
    };
});