angular.module("vencorr").factory("InjuryService", function ($http, config) {
    function save(data) {
        return $http.post(config.baseUrl + "/injury", { injury: data });
    }

    function update(data) {
        return $http.put(config.baseUrl + "/injury", { injury: data });
    }

    function get(injuryId) {
        return $http.get(config.baseUrl + "/injury/" + injuryId);
    }

    function list(params) {
        return $http.post(config.baseUrl + "/injuries", params);
    }

    return {
        update: update,
        save: save,
        list: list,
        get: get
    };
});