async function getHour(req, res) {
    const moment = require('moment');
    const now = moment();
    res.status(200).json(now);
}

async function feedDashboard(req, res) {
    const tokenData = UtilService.getTokenData(req);
    let goal = 0;
    let reached = 0;
    let gap = 0;
    let pending = 0;
    let userAcceptedValue = 0;
    let userOpenValue = 0;

    if (!tokenData.company_id || !tokenData.seller_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    // Goal
    const company = await Company.findOne({
        where: {
            id: tokenData.company_id
        },
        select: ['agro_goal']
    });

    goal = company.agro_goal;

    // Atingidos
    const quotations = await Quotation.find({
        where: {
            company: tokenData.company_id,
            status: 'accepted'
        },
        select: ['id', 'premium']
    });

    if (!quotations.length) {
        reached = 0;
    } else {
        for (quotation of quotations) {
            reached += quotation.premium;
        }
    }

    if (goal) {
        gap = goal - reached;
    } else {
        gap = 0;
    }

    // Pending
    const pendingQuotations = await Quotation.find({
        where: {
            company: tokenData.company_id,
            status: 'pre-quotation'
        },
        select: ['id', 'premium']
    });

    if (!pendingQuotations.length) {
        pending = 0;
    } else {
        for (quotation of pendingQuotations) {
            pending += quotation.premium;
        }
    } 

    // Pendentes do usuario
    const pendingUserQuotations = await Quotation.find({
        where: {
            company: tokenData.company_id,
            seller: tokenData.seller_id,
            status: 'pre-quotation'
        },
        select: ['id', 'premium']
    });

    if (!pendingUserQuotations.length) {
        userOpenValue = 0;
    } else {
        for (quotation of pendingUserQuotations) {
            userOpenValue += quotation.premium;
        }
    }

    // Aceitas do usuario
    const acceptedUserQuotations = await Quotation.find({
        where: {
            company: tokenData.company_id,
            seller: tokenData.seller_id,
            status: 'accepted'
        },
        select: ['id', 'premium']
    });

    if (!acceptedUserQuotations.length) {
        userAcceptedValue = 0;
    } else {
        for (quotation of acceptedUserQuotations) {
            userAcceptedValue += quotation.premium;
        }
    } 

    goal = UtilService.formatMoney(goal);
    reached = UtilService.formatMoney(reached);
    gap = UtilService.formatMoney(gap);
    pending = UtilService.formatMoney(pending);
    userAcceptedValue = UtilService.formatMoney(userAcceptedValue);
    userOpenValue = UtilService.formatMoney(userOpenValue);

    res.status(200).json({
        goal,
        reached,
        gap,
        pending,
        userAcceptedValue,
        userOpenValue
    });
}

async function supportOfflineEmail(req, res) {
    const { subject, message, name, email, phone, company_name  } = req.body;
    const result = await EmailService.supportOfflineEmail(subject, message, name, email, phone, company_name);

    if (result.status === 'error') {
        res.status(500).json({
            status: 'error',
            err: result.err
        });
    } else {
        res.status(200).json('ok');
    }

}

async function externalEmail(req, res) {
    const { name, to, message, phone } = req.body;

    if (!to) {
        res.status(400).json('missing email');
        return;
    }

    const result = await EmailService.externalEmail(to, name, phone, message);

    if (result.status === 'error') {
        res.status(500).json({
            status: 'error',
            err: result.err
        });
    } else {
        res.status(200).json('ok');
    }
}

async function externalInformativeEmail(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { client_name, recipients, period, policy_number, policy_url, bank_slip } = req.body;

    if (!client_name || !recipients.length || !period || !policy_number || !policy_url) {
        res.status(400).json({
            message: 'missing_param'
        });
        return;
    }

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'not authorized'
        });
        return;
    }

    const result = await EmailService.externalInformativeEmail(client_name, recipients, period, policy_number, policy_url, bank_slip);

    if (result.status === 'error') {
        res.status(500).json({
            status: 'error',
            err: result.err
        });
    } else {
        res.status(200).json('ok');
    }
}

async function franchiseeContactSiteEmail(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { name, phone, email, investing, state, city, has_capital, contact_pref } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'not authorized'
        });
        return;
    }

    const responseRD  = await RDService.sendLead(name, phone, email, investing, state, city, has_capital, contact_pref);
    const result = await EmailService.franchiseeSiteEmail(name, phone, email, investing, state, city, has_capital, contact_pref);

    if (result.status === 'error') {
        res.status(500).json({
            status: 'error',
            err: result.err
        });
    } else {
        res.status(200).json({
            email: 'ok',
            rd: responseRD
        });
    }
}

async function quotationRequestEmail(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { customer_name, company_name, segment, hash } = req.body;
    let requestURL = `http://digital.vencorr.com/resolver-solicitacao/${hash}`;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'not authorized'
        });
        return;
    }

    const result = await EmailService.quotationRequest(customer_name, company_name, segment, requestURL);

    if (result.status === 'error') {
        res.status(500).json({
            status: 'error',
            err: result.err
        });
    } else {
        res.status(200).json('ok');
    }
}

async function quotationChosenEmail(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { customer_name, company_name, segment, hash } = req.body;
    let requestURL = `http://digital.vencorr.com/resolver-solicitacao/${hash}`;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'not authorized'
        });
        return;
    }

    const result = await EmailService.quotationChosen(customer_name, company_name, segment, requestURL);

    if (result.status === 'error') {
        res.status(500).json({
            status: 'error',
            err: result.err
        });
    } else {
        res.status(200).json('ok');
    }
}

async function injuryOpenedEmail(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { customer_name, company_name, id } = req.body;
    let requestURL = `http://digital.vencorr.com/resolver-sinistro/${id}`;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'not authorized'
        });
        return;
    }

    const result = await EmailService.injuryOpened(customer_name, company_name, requestURL);

    if (result.status === 'error') {
        res.status(500).json({
            status: 'error',
            err: result.err
        });
    } else {
        res.status(200).json('ok');
    }
}

async function directMessageEmail(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { data } = req.body;

    let requestURL = 'https://digital.vencorr.com/resolver-solicitacao/' + data.hash;
    
    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'not authorized'
        });
        return;
    }

    const result = await EmailService.directMessageEmail(data.client_name, data.company_name, data.message, data.files, requestURL);

    if (result.status === 'error') {
        res.status(500).json({
            status: 'error',
            err: result.err
        });
    } else {
        res.status(200).json('ok');
    }
}

async function callBackRDStation(req, res) {
    const { code } = req.query;

    console.log(code);
}

async function generateHash(req, res) {
    const uuidv1 = require('uuid/v1');
    const tokenData = UtilService.getTokenData(req);
    const hash = uuidv1();

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'not authorized'
        });
        return;
    }

    res.status(200).json(hash);
}

module.exports = {
    generateHash,
    callBackRDStation,
    franchiseeContactSiteEmail,
    quotationRequestEmail,
    quotationChosenEmail,
    externalInformativeEmail,
    feedDashboard,
    supportOfflineEmail,
    externalEmail,
    injuryOpenedEmail,
    getHour,
    directMessageEmail
};

