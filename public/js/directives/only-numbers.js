angular.module('vencorr').directive('onlyNumber', function($filter) {
    function onlyNumber(e) {
        var key = (window.event) ? event.keyCode : e.which;
    
        // Numbers | numeric keypad
        if ((key > 47 && key < 58) || (key >= 96 && key <= 105)) {
            return true;
        } 
    
        if (key === 8 && e.ctrlKey) {
            return true;
        }
    
        if (key === 65 && e.ctrlKey) {
            return true;
        }
    
        if (key === 67 && e.ctrlKey) {
            return true;
        }
    
        if (key === 86 && e.ctrlKey) {
            return true;
        }
    
        if (key === 88 && e.ctrlKey) {
            return true;
        }
    
        if (key === 9 && e.shiftKey) {
            return true;   
        }
    
        // Shift | Alt | Ctrl
        if (e.shiftKey || e.altKey || e.ctrlKey) {
            return false;
        }
    
        if (key === 8 || key === 9 || key === 13 // Backspace | Tab | Enter
            || key === 35 || key === 36 // Home | End
            || key === 37 || key === 39 // left | right arrows
            || key === 46 || key === 45) { // Del | Ins
            
            return true;
        }
    
        return false;
    }

    return {
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, element, attrs, ngCtrl) {
            element.bind("keydown keypress", function(event) {
                if (!event || !onlyNumber(event)) {
                    event.preventDefault();
                }
            });

            element.bind("paste", function(event) {
                var clipBoard = (event.originalEvent || event).clipboardData.getData("text");
                var pastedValue;
                var regex;

                if (clipBoard) {
                    regex = new RegExp(/^-?[0-9]+$/);
                    pastedValue = clipBoard.replace(/\./g, "").replace("/", "").replace("-", "");

                    if (!regex.test(pastedValue)) {
                        event.preventDefault();
                    }
                }
            });
        }
    };
});