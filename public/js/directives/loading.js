angular.module('vencorr').directive('vencorrLoading', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs) {
            scope.$watch(attrs.ngModel, function(value) {
                if (value) {
                    var loagingHtml =
                     '<div class="col-lg-8 col-lg-offset-1">' +
                     '  <div class="text-center m-t-20">' +
                     '      <span style="font-size: 18px; font-weight: bold;">Aguarde...</span><br />' +
                     '	    <img src="img/loading.gif" alt="loading">' +
                     '  </div>' +
                     '</div>';

                    element.append(loagingHtml);
                } else {
                    element.empty();
                }
            });
        }
    };
});