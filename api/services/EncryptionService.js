var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');

function hashPassword(password) {
	 if (!password) {
	 	return;
	 }

	 return bcrypt.hashSync(password);
}

function comparePassword(typedPass, password) {
	return bcrypt.compareSync(typedPass, password);
}

function createToken(user, company) {
	return jwt.sign({
			name: user.name,
	    	user_id: user.id,
	    	email: user.email,
			status: user.status,
			roles: user.role,
			type: user.type,
			company_id: company.id,
	    	company_name: company.name
	  	},
	  	sails.config.custom.secret,
	  	{
	    	algorithm: sails.config.custom.algorithm,
	    	expiresIn: sails.config.custom.expires,
	    	issuer: sails.config.custom.issuer,
	    	audience: sails.config.custom.audience
	  	}
	);
}

function verifyToken(token, callback) {
	return jwt.verify(token, "DSuhsihdheduiDE893228h948h4948hISuhdsdsda3332", callback);
}

async function addTokenToDatabase(userId, token) {
	return await Token.create({
		user: userId,
		token: token,
		status_code: true
	}).fetch();
}

async function findActiveTokens(userId) {
	const tokensFound = await Token.find({
		where: {
			user: userId,
			status_code: true
		},
		select: ['id']
	});

	return tokensFound;
}

async function addTokenToBlacklist(tokenList) {
	if (!tokenList.length) {
		return true;
	}

	for (token of tokenList) {
		await Token.update({ id: token.id }).set({ status_code: false });
	}

	return true;
}

module.exports = {
	createToken,
	verifyToken,
	hashPassword,
	comparePassword,
	findActiveTokens,
	addTokenToDatabase,
	addTokenToBlacklist

}