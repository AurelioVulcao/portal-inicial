angular.module("vencorr").factory("DashboardService", function ($http, config) {
    function listQuotations(data) {
        return $http.post(config.baseUrl + "/dashboard/quotations", { search : data });
    }

    function listInjuries(data) {
        return $http.post(config.baseUrl + "/dashboard/injuries", { search : data });
    }

    function listActivities(data) {
        return $http.post(config.baseUrl + "/dashboard/activities", { search : data });
    }

    function resume(data) {
        return $http.post(config.baseUrl + "/dashboard/resume", { search : data });
    }

    function getComissionGraphData(data) {
        return $http.post(config.baseUrl + "/dashboard/comission-graph", { search : data });
    }

    return {
        listQuotations: listQuotations,
        listInjuries: listInjuries,
        listActivities: listActivities,
        getComissionGraphData: getComissionGraphData,
        resume: resume
    };
});