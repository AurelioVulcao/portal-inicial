angular.module('vencorr').controller('CustomersController', function($scope, CustomerService, $base64) {
    $scope.loading = false;
    $scope.itemsPerPage = 10;

    function init() {
        listCustomers();
        $scope.filterOption = 'name';
    }

    function listCustomers(filter) {
        $scope.loading = true;
        var params = {
            page: $scope.page || 1,
        };

        if ($scope.search) {
            params.search = $scope.search;
            params.prop = $scope.filterOption;
        }

        CustomerService.list({ params: params }).then(function(resp) {
            $scope.totalCustomers = +resp.data.amount;
            if (!resp.data.customers.length) {
                $scope.customers = [];
                $scope.loading = false;
                return;
            }

            $scope.customers = resp.data.customers.map(function(customer) {
                customer.encodedId = $base64.encode(customer.id);
                return customer;
            });
            $scope.loading = false;
        }).catch(function(err) {
            $scope.loading = false;
        });
    }

    function clear() {
		$scope.search = null;
		$scope.filterOption = 'name';
		listCustomers();
    }
   
    init();
    $scope.clear = clear;
    $scope.listCustomers = listCustomers;
});