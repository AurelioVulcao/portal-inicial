async function registerAPICall(endpoint, user) {
	let networkInfo = IpService.getIpAddress();

	await Monitoring.create({
		resource: 'API',
		user,
		extra: JSON.stringify({ endpoint: endpoint,  network_info: networkInfo })
	});

	return true;
}

async function verifyToken(req, res, next) {
	var token = req.headers['authorization'];
	const tokenData = UtilService.getTokenData(req);
	const method = req.method;
	const apiUrl = req.url;

	if (!token) {
		await registerAPICall(`${method}: ${apiUrl}`, null) // Register API CALL
		return res.forbidden('You are not allowed to perform this action.'); 
	}

	await registerAPICall(`${method}: ${apiUrl}`, tokenData.user_id); // Register API CALL
	const amountOfTokensInBlacklist = await Token.count({ token, status_code: false });

	if (amountOfTokensInBlacklist) {
		console.log('Token in Blacklist');
		return res.forbidden('You are not allowed to perform this action.');
	}

 	EncryptionService.verifyToken(token, async function(err, decoded) {
 		if (err) {
			return res.forbidden('You are not allowed to perform this action.');
		}

 		if (decoded) {
 			return next();
 		}
 	});
}

module.exports = verifyToken;
