angular.module('vencorr', [
    'ngRoute',
    'ngSanitize',
    'oitozero.ngSweetAlert',
    'angular-jwt',
    'base64',
    'ui.bootstrap',
    'ui.utils.masks',
    'facebook',
    '720kb.datepicker',
    'ngFileUpload',
    'chart.js',
    'angular-uuid',
    'ui.select'
]);