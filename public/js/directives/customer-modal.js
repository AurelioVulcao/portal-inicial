angular.module('vencorr').directive('customerModal', function () {
    return {
        templateUrl: 'views/customer-modal.html',
        replace: true,
        restrict: 'E'
    };
});