angular.module('vencorr').controller('QuotationsController', function($scope, QuotationService, $location) {
    $scope.loading = false;
    $scope.isModalDirectMessagesOpen = false;
    $scope.filterOption = 'all';
    $scope.segmentFilter = 'auto';
    
    function init() {
        list();
    }

    function list() {
        $scope.loading = true;
        QuotationService.list().then(function(resp) {
            $scope.loading = false;
            $scope.quotations = resp.data;
            $scope.quotations.forEach(function(quotation) {
                if (quotation.items && quotation.segment_type !== 'auto' && quotation.segment_type !== 'residencial'
                && quotation.segment_type !== 'equipamento') {
                    quotation.itemsList = JSON.parse(quotation.items);
                } else {
                    quotation.itemsList = quotation.items;
                }

                if (quotation.segment_type == 'residencial') {
                    quotation.extra = JSON.parse(quotation.extra);
                }

                if (quotation.message) {
                    var messagesInside = JSON.parse(quotation.message.messages);
                    
                    quotation.numberOfMessages = messagesInside.length;
                }
    
                quotation.segmentTypeDisplay = formatSegmentName(quotation.segment_type);
                quotation.customerName = quotation.customer ? quotation.customer.name : 'Cliente não cadastrado';
            });
        });
    }

    function formatSegmentName(segment) {
        if (segment === 'construcao') {
            return 'Construção';
        }

        if (segment === 'agro') {
            return 'Agro';
        }

        if (segment === 'auto') {
            return 'Auto';
        }

        if (segment === 'residencial') {
            return 'Residencial';
        }

        if (segment === 'empresarial') {
            return 'Empresarial';
        }

        if (segment === 'equipamento') {
            return 'Equipamen.';
        }

        if (segment === 'outras') {
            return 'Outras';
        }
    }

    function clearSearch() {
        $scope.search = undefined;

        if ($scope.filterOption == 'segment_type') {
            filter();
        }
    }

    function filter() {
        var selectedFilter = $scope.filterOption;
        var usedSearch = $scope.search;

        if (selectedFilter == 'all') {
            usedSearch = {};
        }

        if (selectedFilter == 'segment_type') {
            usedSearch = $scope.segmentFilter;
        }

        if (selectedFilter == 'name' && !usedSearch) {
            return;
        }

        if (selectedFilter === 'date') {
            var startDate = $scope.searchDateStart;
            var endDate = $scope.searchDateEnd;

            if (!startDate || !endDate) {
                 return;
            }

            usedSearch = {
                start: startDate,
                end: endDate
            };
        }

        $scope.loading = true;
        QuotationService.list({
            key: selectedFilter,
            search: usedSearch || null
        }).then(function(resp) {
            $scope.loading = false;
            $scope.quotations = resp.data;

            if (!$scope.quotations.length) {
                return;
            }

            $scope.quotations.forEach(function(quotation) {
                var items;

                quotation.segmentTypeDisplay = formatSegmentName(quotation.segment_type);
                quotation.customerName = quotation.customer ? quotation.customer.name : 'Cliente não cadastrado';

                if (items) {
                    quotation.itemsJson = items | ' - ';
                }
            });
        });
    }

    function clear() {
		$scope.search = undefined;
		$scope.filterOption = 'all';
		list();
    }

    function goToQuotationDetails(quotation, event) {
        var targetType = event.target.type;
        var parentElementType = event.target.parentElement.type;

        if (targetType == 'button' || parentElementType == 'button') {
            $scope.isModalDirectMessagesOpen = true;
            return;
        }

        // console.log('isOpen?', $scope.isModalDirectMessagesOpen)

        if ($scope.isModalDirectMessagesOpen) {
            return;
        }

        if (quotation.segment_type === 'construcao') {
            $location.path('cotacao-geral/' + quotation.segment_type + '/' + quotation.hash);
            return;
        }

        if (quotation.segment_type === 'auto') {
            $location.path('cotacao-auto/' + quotation.hash);
            return;
        }

        if (quotation.segment_type === 'agro') {
            $location.path('cotacao-agro-sompo/' + quotation.hash);
            return;
        }

        if (quotation.segment_type === 'residencial') {
            $location.path('cotacao-residencial/' + quotation.hash);
            return;
        }

        if (quotation.segment_type === 'empresarial') {
            $location.path('cotacao-empresarial/' + quotation.hash);
            return;
        }

        if (quotation.segment_type === 'equipamento') {
            $location.path('cotacao-equipamento/' + quotation.hash);
            return;
        }

        if (quotation.segment_type === 'outras') {
            $location.path('cotacao-geral/' + quotation.hash);
            return;
        }
    }

    function setDateFilter(period) {
        if (period === 'today') {
            $scope.searchDateStart = moment().format('DD/MM/YYYY');
            $scope.searchDateEnd = moment().format('DD/MM/YYYY');
        }

        if (period === 'yesterday') {
            $scope.searchDateStart = moment().subtract(1,'day').format('DD/MM/YYYY');
            $scope.searchDateEnd = moment().subtract(1,'day').format('DD/MM/YYYY');
        }

        if (period === 'week') {
            $scope.searchDateStart = moment().day(0).format('DD/MM/YYYY');
            $scope.searchDateEnd = moment().day(6).format('DD/MM/YYYY');
        }

        if (period === 'lastWeek') {
            $scope.searchDateStart = moment().day(0).subtract(1,'week').format('DD/MM/YYYY');
            $scope.searchDateEnd = moment().day(6).subtract(1,'week').format('DD/MM/YYYY');
        }

        if (period === 'month') {
            $scope.searchDateStart = moment().startOf('month').format('DD/MM/YYYY');
            $scope.searchDateEnd = moment().endOf('month').format('DD/MM/YYYY');
        }

        filter()
    }

    function capitalize(word) {
        if (!word) {
            return;    
        }
        
        return word.charAt(0).toUpperCase() + word.slice(1);
    }

    // Event listening from direct message modal on close
    // When modal is open cant click on quotations to go into, when modal close ok!
    $scope.$on('closed-direct-message-modal', function(event, args) {
        // Trick to update scope without use $aply()
        setTimeout(function() {
            $scope.isModalDirectMessagesOpen = false;
        }, 0);
    });
    
    init();
    $scope.list = list;
    $scope.clear = clear;
    $scope.capitalize = capitalize;
    $scope.filter = filter;
    $scope.setDateFilter = setDateFilter;
    $scope.goToQuotationDetails = goToQuotationDetails;
    $scope.clearSearch = clearSearch;
});