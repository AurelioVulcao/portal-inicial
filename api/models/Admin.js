module.exports = {
  attributes: {
		name: {
			type: 'string'
    },
    email: {
			type: 'string'
    },
    password: {
			type: 'string'
    },
    phone: {
			type: 'string'
    },
    status: {
			type: 'string'
    }
	},
};