const moment = require('moment');

async function listByCustomer(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { customer_id } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        const quotationsList = await Quotation.find({
            where: {
                customer: customer_id
            },
        }).sort('created_at DESC');

        res.status(200).json(quotationsList);
        return;
    } catch(err) {
        res.status(500).json({
            message: 'error on get quotations list'
        });
    }
}

async function list(req, res) {
    let filter = {};
    const tokenData = UtilService.getTokenData(req);
    const { key, search } = req.body; // Filtro vindo do front
    let quotationsList = [];

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    // Filtro base de usuario e empresa
    filter.company = tokenData.company_id;

    if (key || search) {
        if (key === 'date') {
            const start = moment(search.start, 'DD/MM/YYYY"').format('YYYY-MM-DD') + ' 00:00:00';
            const end = moment(search.end, 'DD/MM/YYYY"').format('YYYY-MM-DD') + ' 23:59:59';

            quotationsList = await Quotation.find({
                where: {
                    updated_at : { '>=': start, '<=': end }
                }
            }).populate('customer').sort('created_at DESC');
            res.status(200).json(quotationsList);
            return;;
        }

        if (key === 'pricegt') {
            filter.premium = { '>': search };
        }

        if (key === 'pricelt') {
            filter.premium = { '<': search };
        }

        if (key === 'item') {
            quotationsList = await Quotation.find({
                where: {
                    items : {
                        'contains' : search
                    }
                }
            }).populate('customer').sort('created_at DESC');
            res.status(200).json(quotationsList);
            return;;
        }

        if (key == 'segment_type') {
            filter.segment_type = search;
        }

        // Filtro de nome de cliente
        if (key === 'name') {
            let customerIds = await Customer.find({
                where: {
                    name : {
                        'contains' : search
                    }
                },
                select: ['id']
            });

            if (!customerIds.length) {
                res.status(200).json(quotationsList);
                return;
            }

            customerIds = customerIds.map(item => {
                return item.id;
            });

            quotationsList = await Quotation.find({
                where: {
                    customer: customerIds
                }
            }).populate('customer').sort('created_at DESC');
            res.status(200).json(quotationsList);
            return;
        }

        // Cotações abertas
        if (key === 'open') {
            filter.status = 'pre-quotation';
        }

        // Cotações fechadas
        if (key === 'closed') {
            filter.status = 'accepted';
        }
    }

    try {
        const quotationsList = await Quotation.find({
            where: filter,
        }).populate('customer').populate('message').sort('created_at DESC');

        res.status(200).json(quotationsList);
        return;
    } catch(err) {
        res.status(500).json({
            message: 'error on get quotations list'
        });
    }
}

async function get(req, res) {
    const { hash } = req.params;
    const tokenData = UtilService.getTokenData(req);

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        const quotation = await Quotation.findOne({
            where: {
                company: tokenData.company_id,
                hash
            },
        }).populate('user').populate('customer').populate('queue').populate('company').populate('message');

        if (!quotation || !quotation.id) {
            res.status(403).json({
                message: 'you have no permission to access this data'
            });
            return;
        }

        res.status(200).json(quotation);
        return;
    } catch(err) {
        console.log(err);
        res.status(500).json({
            message: 'error on get quotation data'
        });
    }
}

async function getById(req, res) {
    const { quotation_id } = req.params;
    const tokenData = UtilService.getTokenData(req);

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        const quotation = await Quotation.findOne({
            where: {
                company: tokenData.company_id,
                id: quotation_id
            },
        }).populate('user').populate('customer').populate('queue').populate('message');

        if (!quotation || !quotation.id) {
            res.status(403).json({
                message: 'you have no permission to access this data'
            });
            return;
        }

        res.status(200).json(quotation);
        return;
    } catch(err) {
        console.log(err)
        res.status(500).json({
            message: 'error on get quotation data'
        });
    }
}

async function updateStatus(req, res) {
    const { status, quotation_id } = req.body;
    const tokenData = UtilService.getTokenData(req);

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        const updatedQuotation = await Quotation.updateOne({ id: quotation_id }).set({ status });
        if (updatedQuotation) {
            let translatedStatus = status === 'accepted' ? 'Aceita' : status;

            if (translatedStatus == 'canceled') {
                translatedStatus = 'Cancelada';
            }

            await UtilService.saveActivity({ 
                user: tokenData.user_id,
                company: tokenData.company_id,
                model: 'quotation',
                description: `${tokenData.name} atualizou a cotação para: ${translatedStatus}`,
                data: JSON.stringify({ id: quotation_id })
            });
            res.status(200).json(updatedQuotation);
            return;
        } else {
            res.status(401).json('quotation id not found');
            return;
        }
    } catch(err) {
        console.log(err);
        res.status(500).json({
            err: err
        });
    }
}

async function updateQueueId(req, res) {
    const { queue_id, quotation_id } = req.body;

    try {
        await Quotation.updateOne({ id: quotation_id }).set({ queue: queue_id });
        const updatedQuotation = await Quotation.findOne({id: quotation_id}).populate('queue');

        if (updatedQuotation) {
            res.status(200).json(updatedQuotation);
        } else {
            res.status(401).json('quotation id not found');
        }

        return;
    } catch(err) {
        res.status(500).json({
            err: err
        });
    }
}

async function updateQuotationInstallments(req, res) {
    const { installments, quotation_id } = req.body;

    try {
        const updatedQuotation = await Quotation.updateOne({ id: quotation_id }).set({ installments });

        if (updatedQuotation) {
            res.status(200).json(updatedQuotation);
        } else {
            res.status(401).json('quotation id not found');
        }

        return;
    } catch(err) {
        res.status(500).json({
            err: err
        });
    }
}

async function updateColaboratorCode(req, res) {
    const { code, quotation_id } = req.body;

    try {
        const updatedQuotation = await Quotation.updateOne({ id: quotation_id }).set({ sompo_colaborator_code: code });

        if (updatedQuotation) {
            res.status(200).json(updatedQuotation);
        } else {
            res.status(401).json('quotation id not found');
        }

        return;
    } catch(err) {
        res.status(500).json({
            err: err
        });
    }
}

async function updateItemAdditionalInfo(req, res) {
    const { items, quotation_id } = req.body;

    try {
        const itemsString = JSON.stringify(items);
        const updatedQuotation = await Quotation.updateOne({ id: quotation_id }).set({ items: itemsString });

        if (updatedQuotation) {
            res.status(200).json(updatedQuotation);
        } else {
            res.status(401).json('quotation id not found');
        }

        return;
    } catch(err) {
        res.status(500).json({
            err: err
        });
    }
}

async function updatePeriod(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { quotation_id, start, end } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        let start_term = moment(start, 'DD/MM/YYYY').format('YYYY-MM-DD');
        let end_term = moment(end, 'DD/MM/YYYY').format('YYYY-MM-DD');

        const updatedQuotation = await Quotation.updateOne({id: quotation_id}).set({start_term, end_term});

        res.status(200).json(updatedQuotation);
    } catch(err) {
        res.status(500).json({
            err: err
        });
    }
}

async function updateQuotation(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { id, items, comission, customer_id, hash, start_term, end_term, customer_snapshot } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    const quotation = {
        items: JSON.stringify(items),
        hash,
        comission,
        start_term: moment(start_term, 'DD/MM/YYYY').format('YYYY-MM-DD'),
        end_term: moment(end_term, 'DD/MM/YYYY').format('YYYY-MM-DD'),
        term_type: 'annual',
        status: 'pre-quotation',
        segment_type: 'agro',
        user: tokenData.user_id,
        customer_snapshot: customer_snapshot,
        company: tokenData.company_id,
        customer: customer_id || null
    };

    try {
        const updatedQuotation = await Quotation.updateOne(id).set(quotation);

        if (updatedQuotation) {
            await UtilService.saveActivity({ 
                user: tokenData.user_id,
                company: tokenData.company_id,
                model: 'quotation',
                description: `${tokenData.name} atualizou as informações da cotação`,
                data: JSON.stringify({ id })
            });
            res.status(200).json(updatedQuotation);
        } else {
            res.status(401).json('quotation id not found');
        }
        return;
    } catch(err) {
        res.status(500).json({
            err: err
        });
    }
}

async function saveQuotation(req) {
    const tokenData = UtilService.getTokenData(req);
    const uuidv1 = require('uuid/v1');
    const { items, comission, start_term, end_term, segment } = req.body;
    const hash = uuidv1();

    if (!tokenData.company_id || !tokenData.user_id) {
        return {
            message: 'user not authorized'
        };
    }

    const quotation = {
        items: JSON.stringify(items),
        hash,
        comission,
        start_term: moment(start_term, 'DD/MM/YYYY').format('YYYY-MM-DD'),
        end_term: moment(end_term, 'DD/MM/YYYY').format('YYYY-MM-DD'),
        term_type: 'annual',
        status: 'pre-quotation',
        segment_type: segment,
        user: tokenData.user_id,
        company: tokenData.company_id
    };

    try {
        const createdQuotation = await Quotation.create(quotation).fetch();
        await UtilService.saveActivity({ 
            user: tokenData.user_id,
            company: tokenData.company_id,
            model: 'quotation',
            description: `${tokenData.name} criou uma cotação`,
            data: JSON.stringify({ id: createdQuotation.id })
        });

        return createdQuotation;
    } catch(err) {
        return { err: err };
    }
}

async function gatewaySegment(req, res) {
    const { segment } = req.body;
    let result;

    if (!segment) {
        res.status(401).json({
            message: 'no segment sent'
        });
        return;
    }

    if (segment == 'agro') {
        result = await saveQuotation(req);
    }

    if (segment == 'auto') {
        result = await addAutoQuotation(req);
    }

    if (segment == 'residencial') {
        result = await addGenericQuotation(req);
    }

    if (segment == 'empresarial') {
        result = await addGenericQuotation(req);
    }

    if (segment == 'equipamento') {
        result = await addGenericQuotation(req);
    }

    if (segment == 'outras') {
        result = await addGenericQuotation(req);
    }

    if (result.error_message) {
        res.status(401).json('fail to create quotation', result);
        return;
    }

    if (result && result.err) {
        res.status(500).json(err);
        return;
    }

    res.status(200).json(result);
}

async function addAutoQuotation(req) {
    const config = require('../../config/custom');
    const axios = require('axios');
    const tokenData = UtilService.getTokenData(req);
    const quotation = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        return {
            error_message: 'user not authorized'
        };
    }

    // SEGFY
    const segfyCalcURL = config.custom.segfyCalcUrl;
    
    quotation.token_corretora = config.custom.segfy_token;
    quotation.url_retorno = config.custom.segfy_callback_url;

    const calcRequest = await axios.post(segfyCalcURL, quotation);

    /***** */

    // CUSTOMER -----------------------
    let customer = {};

    customer.name = quotation.cliente.nome;
    customer.birthdate = moment(quotation.cliente.nascimento, 'YYYY-MM-DD').format('YYYY-MM-DD');
    customer.genre = quotation.cliente.sexo === 'M' ? 'masculino': 'feminino';
    customer.type = quotation.tipo_pessoa;
    customer.email = quotation.cliente.email || '';
    customer.phone = quotation.cliente.celular || '';
    customer.cep = quotation.cotacao.cep_pernoite || '';

    if (quotation.tipo_pessoa === 'fisica') {
        customer.cpf = quotation.cliente.documento;
    } else {
        customer.cnpj = quotation.cliente.documento;
    }

    const savedCustomer = await addAutoCustomer(customer, tokenData);

    /************************************************/

    // MESSAGE
    const createdMessage = await Message.create({ messages: JSON.stringify([]), company: tokenData.company_id}).fetch();
    // QUOTATION
    const internalQuotation = {
        items: quotation.produto,
        hash: quotation.id_referencia,
        customer_snapshot: JSON.stringify(savedCustomer),
        status: 'quoted',
        segment_type: 'auto',
        customer: savedCustomer.id,
        user: tokenData.user_id,
        company: tokenData.company_id,
        comission: quotation.cotacao.comissao,
        start_term: moment(quotation.cotacao.inicio_vigencia, 'YYYY-MM-DD').format('YYYY-MM-DD'),
        end_term: moment(quotation.cotacao.termino_vigencia, 'YYYY-MM-DD').format('YYYY-MM-DD'),
        extra: JSON.stringify(quotation),
        info: null,
        message: createdMessage.id
    };

    const existedQuotation = await Quotation.findOne({
        where: {
            hash: internalQuotation.hash
        }
    });

    let returnedQuotation;

    if (existedQuotation && existedQuotation.id) {
        delete internalQuotation.hash;
        returnedQuotation = await Quotation.updateOne({ id: existedQuotation.id }).set(internalQuotation);
    } else {
        returnedQuotation = await Quotation.create(internalQuotation).fetch();
    }
    
    await Message.updateOne({ id: createdMessage.id}).set({ quotation: returnedQuotation.id });
    await UtilService.saveActivity({ 
        user: tokenData.user_id,
        company: tokenData.company_id,
        model: 'quotation',
        description: `${tokenData.name} criou uma cotação de auto`,
        data: JSON.stringify({ id: returnedQuotation.id })
    });

    console.log(calcRequest.data);
    return calcRequest.data;
}

async function addAutoCustomer(autoCustomer, tokenData) {
    const customer = autoCustomer;

    let orQuery = {};

    if (customer.type === 'juridica') {
        orQuery = [
            { cnpj: customer.cnpj }
        ];
    } else {
        orQuery = [
            { cpf: customer.cpf }
        ];
    }
    const existCustomer = await Customer.findOne({
        where: {
            company: tokenData.company_id,
            or : orQuery
        }
    });

    if (existCustomer) {
        return existCustomer;
    }

    customer.company = tokenData.company_id;
    const customerCreated = await Customer.create(customer).fetch();
    await UtilService.saveActivity({
        user: tokenData.user_id,
        company: tokenData.company_id,
        model: 'customer',
        description: `${tokenData.name} cadastrou um cliente`,
        data: JSON.stringify({ id: customerCreated.id })
    });

    return customerCreated;
}

async function addGenericQuotation(req) {
    const tokenData = UtilService.getTokenData(req);
    const uuidv1 = require('uuid/v1');
    const { quotation_info, segment, customer_snapshot, customer_id, start_term, end_term, items, status } = req.body;
    const hash = uuidv1();

    if (!tokenData.company_id || !tokenData.user_id) {
        return {
            error_message: 'user not authorized'
        };
    }

    const createdMessage = await Message.create({ messages: JSON.stringify([]), company: tokenData.company_id}).fetch();
    const quotation = {
        hash,
        items,
        start_term,
        end_term,
        customer_snapshot: customer_snapshot,
        status: status || 'pre-quotation',
        segment_type: segment,
        customer: customer_id,
        user: tokenData.user_id,
        company: tokenData.company_id,
        extra: JSON.stringify(quotation_info),
        message: createdMessage.id
    };

    try {        
        const createdQuotation = await Quotation.create(quotation).fetch();
        await Message.updateOne({ id: createdMessage.id}).set({ quotation: createdQuotation.id });
        await UtilService.saveActivity({ 
            user: tokenData.user_id,
            company: tokenData.company_id,
            model: 'quotation',
            description: `${tokenData.name} criou uma cotação`,
            data: JSON.stringify({ id: createdQuotation.id })
        });

        return createdQuotation;
    } catch(err) {
        console.log('Estourou aqui', err);
        return { err: err.toString() || '' };
    }
}

async function update(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { quotation } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        return {
            message: 'user not authorized'
        };
    }

    try {
        const updatedQuotation = await Quotation.updateOne({ id: quotation.id }).set(quotation);
        await UtilService.saveActivity({ 
            user: tokenData.user_id,
            company: tokenData.company_id,
            model: 'quotation',
            description: `${tokenData.name} atualizou os dados da cotação`,
            data: JSON.stringify({ id: updatedQuotation.id })
        });

        res.status(200).json(updatedQuotation);
    } catch(err) {
        res.status(500).json(err);
    }
}

async function onConnect(req, res) {
    const { quotation_id } = req.body;

	sails.sockets.join(req, quotation_id);
}

async function autoReturnCallback(req, res) {
    const body = req.body;
    const referenceIdAndRoom = body.id_referencia;
    const stringfiedQuote = JSON.stringify(body);

    const quotation = await Quotation.findOne({ where: { hash: body.id_referencia }, select: ['info'] });

    if (!quotation.info) {
        quotation.info = [];
        quotation.info.push(body);
    } else {
        quotation.info = JSON.parse(quotation.info);
        quotation.info.push(body);
    }

    await Quotation.updateOne({ id: quotation.id }).set({ info: JSON.stringify(quotation.info)});
    await Monitoring.create({
        resource: 'segfy-api'
    });

    if (body && body.status == 'ERRO') {
        setTimeout(function() {
            console.log('FIRE ERROR INSIDE TIMEOUT API SEGFY MESSAGE');
            sails.sockets.broadcast(referenceIdAndRoom, 'quotation', stringfiedQuote );
        }, 4000);

        return;
    }

    console.log(JSON.stringify(body));
    sails.sockets.broadcast(referenceIdAndRoom, 'quotation', stringfiedQuote );
}

async function updateGenericQuotation(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { quotation } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        return {
            message: 'user not authorized'
        };
    }

    try {
        const updatedQuotation = await Quotation.updateOne({ id: quotation.quotation_id }).set(quotation);
        res.status(200).json(updatedQuotation);
    } catch(err) {
        console.log(err);
        res.status(500).json({
            message: 'error on get quotation data'
        });
    }
}

module.exports = {
    update,
    getById,
    onConnect,
    autoReturnCallback,
    updateGenericQuotation,
    updateQuotationInstallments,
    updateItemAdditionalInfo,
    updateColaboratorCode,
    updateQueueId,
    addGenericQuotation,
    gatewaySegment,
    updateQuotation,
    listByCustomer,
    saveQuotation,
    updateStatus,
    updatePeriod,
    list,
    get
};

