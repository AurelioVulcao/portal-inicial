const moment = require('moment');

async function save(req, res) {
    const { injury } = req.body;
    const tokenData = UtilService.getTokenData(req);

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        injury.event_date = moment(injury.event_date, 'DD/MM/YYYY').format('YYYY-MM-DD');
        injury.company = tokenData.company_id;
        injury.status = 'open';
        const injuryCreated = await Injury.create(injury).fetch();
        await UtilService.saveActivity({ 
            user: tokenData.user_id,
            company: tokenData.company_id,
            model: 'injury',
            description: `${tokenData.name} cadastrou um sinistro`,
            data: JSON.stringify({ id: injuryCreated.id })
        });

        res.status(200).json(injuryCreated);
    } catch(err) {
        console.log(err)
        res.status(500).json('error on create injury');
        return;
    }
}

async function update(req, res) {
    const { injury } = req.body;
    const tokenData = UtilService.getTokenData(req);

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        delete injury.created_at;
        delete injury.updated_at;

        injury.event_date = moment(injury.event_date, 'DD/MM/YYYY').format('YYYY-MM-DD');
        injury.company = tokenData.company_id;
        const injuryUpdated = await Injury.updateOne({ id: injury.id }).set(injury);
        await UtilService.saveActivity({ 
            user: tokenData.user_id,
            company: tokenData.company_id,
            model: 'injury',
            description: `${tokenData.name} atualizou um sinistro`,
            data: JSON.stringify({ id: injuryUpdated.id })
        });

        res.status(200).json(injuryUpdated);
    } catch(err) {
        console.log(err)
        res.status(500).json('error on update injury');
        return;
    }
}

async function get(req, res) {
    const { injury_id } = req.params;
    const tokenData = UtilService.getTokenData(req);
    
    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    const injury = await Injury.findOne({
        where: {
            id: injury_id,
            company: tokenData.company_id
        }
    }).populate('customer');

    if (!injury || !injury.id) {
        res.status(401).json({
            message: 'can not find customer'
        });
        return;
    }

    res.status(200).json(injury);
}

async function list(req, res) {
    const { base64encode, base64decode } = require('nodejs-base64');
    const tokenData = UtilService.getTokenData(req);
    const { params } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    const filter = {
        company: tokenData.company_id,
    };

    if (params.start && params.end) {
        const start = moment(params.start, 'DD/MM/YYYY"').format('YYYY-MM-DD') + ' 00:00:00';
        const end = moment(params.end, 'DD/MM/YYYY"').format('YYYY-MM-DD') + ' 23:59:59';

        filter.created_at = { '>=': start, '<=': end }
    }

    if (params.customer) {
        filter.customer = params.customer;
    }

    if (params.status) {
        filter.status = params.status;
    }

    try {
        const amount = await Injury.count(filter);
        const injuries = await Injury.find({ where: filter }).populate('customer').paginate((params.page - 1), 10).sort('created_at DESC');

        injuries.forEach(function(item) {
            if (item && item.customer) {
                item.customer.encodedId = base64encode(item.customer.id);
            }
        });


        res.status(200).json({
            injuries,
            amount
        });
    } catch(err) {
        res.status(500).json({
            err: err
        });
    }
}

module.exports = {
    update,
    save,
    get,
    list
};

