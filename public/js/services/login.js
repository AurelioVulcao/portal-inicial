angular.module("vencorr").factory("LoginService", function($rootScope, $http, config, StorageService) {
    $rootScope.navbar = null;

    function authenticate(email, password) {
        return $http.post(config.baseUrl + "/login", {
            email: email,
            password: password
        });
    }

    function socialAuthenticate(email) {
        return $http.post(config.baseUrl + "/social/login", {
            email: email
        });
    }

    function storageLogin(seller) {
        StorageService.clear();
        delete seller.password;
        seller.logged = true;
        seller.navbar = '../../views/navbar.html';
        StorageService.set('user', JSON.stringify(seller));
    }

    function linkedinAuth(oauth2Code) {
        return $http.post(config.baseUrl + "/auth/linkedin/callback", {
            oauth2Code: oauth2Code
        });
    }

    return {
        authenticate: authenticate,
        storageLogin: storageLogin,
        linkedinAuth: linkedinAuth,
        socialAuthenticate: socialAuthenticate
    };
});