angular.module('vencorr').directive('customerDisplay', function () {
    return {
        templateUrl: 'views/customer-display.html',
        replace: true,
        restrict: 'E'
    };
});