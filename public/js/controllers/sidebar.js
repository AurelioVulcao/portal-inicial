angular.module('vencorr').controller('SidebarController', function($scope, StorageService) {
	$scope.loading = false;

	function logout() {
		StorageService.clear();
		window.location = '/login';
	}

	$scope.logout = logout;
});