angular.module('vencorr').controller('QuotationAutoController', function($scope, $routeParams, SegfyService, uuid, $q, UtilService, SweetAlert, QuotationService, config, $rootScope, MetaService) {
    $scope.quotationAutoSocket = config.quotationAutoSocket;
    $scope.isEdit = false;
    $scope.hash = $routeParams.hash;
    $scope.loading = false;
    $scope.missingFields = [];
    $scope.models = [];
    $scope.quotationsList = [];
    $scope.uiSelects = {};
    $scope.insurerDictionary = {
        mapfre: 'MAPFRE',
        liberty: 'LIBERTY',
        hdi: 'HDI',
        allianz: 'ALLIANZ',
        bradesco: 'BRADESCO',
        tokio: 'TOKIO',
        sompo: 'SOMPO',
        porto: 'PORTO',
        azul: 'AZUL',
        sul_america: 'SUL AMERICA',
        suhai: 'SUHAI'
    };
    $scope.quotation = {
        segment: 'auto',
        cliente: {},
        condutor_principal: {},
        veiculo: {},
        cobertura: {},
        cotacao: {},
        seguradora: ['hdi', 'mapfre', 'liberty', 'allianz', 'bradesco', 'tokio', 'sul_america', 'porto', 'sompo', 'suhai']
    };
    $scope.dynamicPopover = {
        bonus_anterior : {
            title: 'Deve-se informar a classe de bônus anterior. Seguradoras que usam esse campo:',
            content: 'Sulamerica, Bradesco, Generali'
        },
        bonus_futura : {
            title: 'Classe de bônus a ser calculada',
            content: 'Deve-se informar a classe de bonus que será calculada na nova vigência.\
            Por exemplo: Se a classe de bonus vigente é 2, então você irá informar neste campo 3.'
        },
        furto : {
            title: 'Vítima de furto ou roubo?',
            content: 'Nos últimos 2 anos o principal condutor foi vítima de roubo ou furto de veículos?'
        },
    };

    function setupSocket() {
        io.sails.url = $scope.quotationAutoSocket;
        io.sails.autoConnect = false;
    }

    function connect() {
        if ($scope.sailsSocket && $scope.sailsSocket.isConnected()) {
            $scope.sailsSocket.disconnect();
        }
        
        $scope.sailsSocket = io.sails.connect();
        $scope.sailsSocket.on('connect', function() {
            $scope.sailsSocket.post('/api/socket-quotation-auto', { quotation_id: $scope.quotation.id_referencia }, function serverResponded (body, JWR) {
                console.log('Ready!');
            });
        });


        $scope.sailsSocket.on('quotation', function(quotation) {
            var socketQuotation = JSON.parse(quotation);

            if (socketQuotation && socketQuotation.status == 'ERRO') {
                SweetAlert.warning(socketQuotation.mensagem.toString(), 'Corrija as informações e tente novamente');
                $scope.loadingCalculate = false;
                $scope.calcOnProgress = false;
            }
            
            if (socketQuotation && socketQuotation.calculo && socketQuotation.calculo.mensagem_ajuste && socketQuotation.calculo.mensagem_ajuste.length) {
                socketQuotation.calculo.mensagem_ajuste = socketQuotation.calculo.mensagem_ajuste.map(function(msg) {
                    return msg.replace(/<[^>]*>?/gm, '');
                });
            }

            if (socketQuotation && socketQuotation.calculo && socketQuotation.calculo.mensagem && socketQuotation.calculo.mensagem.length) {
                socketQuotation.calculo.mensagem = socketQuotation.calculo.mensagem.map(function(msg) {
                    return msg.replace(/<[^>]*>?/gm, '');
                });
            }

            if (socketQuotation.calculo && socketQuotation.calculo.seguradora) {
                $scope.quotationsList.push(socketQuotation);
            }

            $scope.$apply();
            window.scrollTo(0,document.body.scrollHeight);
        }); 
    }

    $scope.$on('$locationChangeStart', function(event, next, current) {
        disconnect();
    });

    function disconnect() {
        if ($scope.sailsSocket && $scope.sailsSocket.isConnected()) {
            $scope.sailsSocket.disconnect();
        }
    }
    
    function init() {
        $scope.loading = true;
        setupSocket();

        if ($scope.hash) {
            $scope.loading = true;
            $scope.isEdit = true;
            $scope.quotation.id_referencia = $scope.hash;

            $q.all([
                loadProfessions(),
                loadBrands()
             ]).then(function(data) {
                getQuotation();
             });
            
        } else {
            $scope.isEdit = false;
            $scope.quotation.id_referencia = uuid.v4();
            setPeriod();
            setInitialValues();

            $q.all([
                loadProfessions(),
                loadBrands()
             ]).then(function() {
                $scope.loading = false;
             });
        }
    }

    function getQuotation() {
        $scope.loading = true;
        QuotationService.get($scope.hash).then(function(resp) {
            var quotationResp = resp.data;

            if (quotationResp && quotationResp.extra) {
                var receivedQuotation = JSON.parse(resp.data.extra);

                $scope.quotationsList = JSON.parse(resp.data.info);
                $scope.quotation = formatAfterGet(receivedQuotation);
            }
            
            $scope.quotation.info = JSON.parse(resp.data.info);
            $scope.quotation.id = resp.data.id;
            $scope.quotation.message = resp.data.message;
            $scope.quotation.hash = resp.data.hash;
            $scope.quotation.status = resp.data.status;

            if ($scope.quotation.status == 'accepted') {
                if ($scope.quotation.info) {

                    $scope.quotation.info.forEach(function(quote) {
                        if (quote.accept) {
                            $scope.acceptedQuote = quote;
                        }
                    });
                }
            }

            $scope.loading = false;
        }).catch(function(err) {
            console.log(err);
            $scope.loading = false;
            SweetAlert.error('Falha ao consultar cotação', 'Verifique se esta cotação existe no menu [Cotações]');
        });
    }

    function formatAfterGet(quotationToFormat) {
        quotationToFormat.cotacao.inicio_vigencia = moment(quotationToFormat.cotacao.inicio_vigencia, 'YYYY-MM-DD').format('DD/MM/YYYY');
        quotationToFormat.cotacao.termino_vigencia = moment(quotationToFormat.cotacao.termino_vigencia, 'YYYY-MM-DD').format('DD/MM/YYYY');
        quotationToFormat.cliente.nascimento = moment(quotationToFormat.cliente.nascimento, 'YYYY-MM-DD').format('DD/MM/YYYY');
        quotationToFormat.condutor_principal.nascimento = moment(quotationToFormat.condutor_principal.nascimento, 'YYYY-MM-DD').format('DD/MM/YYYY');
        quotationToFormat.condutor_principal.data_habilitacao = moment(quotationToFormat.condutor_principal.data_habilitacao, 'YYYY-MM-DD').format('DD/MM/YYYY');

        // Marca
        $scope.brands.forEach(function(brand) {
            if (quotationToFormat.veiculo.marca.id == brand.id) {
                $scope.uiSelects['marca'] = brand;
            }
        });

        // Profissão
        $scope.professions.forEach(function(profession) {
            if (quotationToFormat.condutor_principal.profissao == profession.id) {
                $scope.uiSelects['profession'] = profession;
            }
        });

        $q.all([
            loadModels(quotationToFormat.veiculo.marca.id, quotationToFormat.veiculo.ano_modelo),
            loadAntiTheft(quotationToFormat.veiculo.anti_furto)
        ]).then(function() {
            $scope.models.forEach(function(model) {
                if (quotationToFormat.veiculo.modelo == model.id) {
                    $scope.uiSelects['model'] = model;
                }
            });

            if ($scope.antiTheftDevices && $scope.antiTheftDevices.length) {
                $scope.antiTheftDevices.forEach(function(device) {
                    if (quotationToFormat.veiculo.codigo_anti_furto == device.id) {
                        $scope.uiSelects['antiTheft'] = device;
                    }
                });
            }
        });
        
        return quotationToFormat; 
    }

    function loadProfessions() {
        var deferred = $q.defer();

        SegfyService.listProfessions().then(function(resp) {
            $scope.professions = resp.data;
            deferred.resolve(true);
        }).catch(function(err) {
            console.log('error load professions');
            SweetAlert.warning('Estamos com problemas de comunicação com as seguradoras', 'Atualize a página e tente novamente, se o erro persistir, entre em contato com o suporte. Código 003')
        });

        return deferred.promise;
    }

    function onSelectProfession(profession) {
        $scope.quotation.condutor_principal.profissao_descricao = profession.descricao;
        $scope.quotation.condutor_principal.profissao = 4; // Default Administrador if not selected
        $scope.quotation.condutor_principal.profissao = profession.id;
    }

    function onSelectBrand(brand) {
        $scope.brandIdSelected = brand.id;
        $scope.quotation.veiculo.marca = brand;
    }

    function checkModels() {
        // Life Cycle fix
        setTimeout(function() {
            if (($scope.quotation.veiculo.ano_modelo && $scope.quotation.veiculo.ano_modelo.length < 4) || !$scope.brandIdSelected) {
                return;
            }
    
            if (!$scope.quotation.veiculo.ano_modelo) {
                return;
            }

            loadModels($scope.brandIdSelected, $scope.quotation.veiculo.ano_modelo);

        }, 0);
    }

    function loadBrands() {
        var deferred = $q.defer();

        SegfyService.listBrands().then(function(resp) {
            $scope.brands = resp.data;
            deferred.resolve(true);
        }).catch(function(err) {
            console.log('error load brands');
            SweetAlert.warning('Estamos com problemas de comunicação com as seguradoras', 'Atualize a página e tente novamente, se o erro persistir, entre em contato com o suporte. Código 003')
        });

        return deferred.promise;
    }

    function loadModels(brandId, year) {
        var deferred = $q.defer();
        $scope.models = [];
        

        $scope.loadingModels = true;
        SegfyService.listModels(brandId, year).then(function(resp) {
            $scope.loadingModels = false;
            $scope.models = resp.data;

            if (!resp.data.length) {
                $scope.modelsFindErrorMessage = 'Nenhum modelo encontrado, verifique o ano que você digitou.';
            } else {
                $scope.modelsFindErrorMessage = null;
            }
            deferred.resolve(true);
        }).catch(function(err) {
            $scope.loadingModels = false;
            console.log('error load brands');
        });

        return deferred.promise;
    }

    function onSelectModel(model) {
        if (!model || !model.id) {
            return;
        }

        $scope.quotation.produto = model.descricao;
        $scope.quotation.veiculo.modelo = model.id;
    }

    function onSelectAntiTheftCategory() {
        var category = $scope.quotation.veiculo.anti_furto;
        loadAntiTheft(category);
    }

    function loadAntiTheft(category) {
        var deferred = $q.defer();
        if (!category) {
            return;
        }

        $scope.loadingAntiTheft = true;
        SegfyService.listAntiTheft(category).then(function(resp) {
            $scope.antiTheftDevices = resp.data;
            $scope.loadingAntiTheft = false;
            deferred.resolve(true);
        }).catch(function(err) {
            $scope.loadingAntiTheft = false;
            console.log('error load anti theft devices');
        });

        return deferred.promise;
    }

    function onSelectAntiTheftDevice(item) {
        if (!item || !item.id) {
            return;
        }

        $scope.quotation.veiculo.anti_furto_descricao = item.marca;
        $scope.quotation.veiculo.codigo_anti_furto = item.id;
    }

    function setInitialValues() {
        $scope.quotation.cotacao.renovacao = 'novo';
        $scope.quotation.tipo_pessoa = 'fisica';
        $scope.quotation.cotacao.sinistros = 0;
        $scope.quotation.veiculo.tipo_combustivel = 'FLEX';
        $scope.quotation.veiculo.tipo_veiculo = 'PARTICULAR';
        $scope.quotation.veiculo.zero_km = 'FALSE';
        $scope.quotation.veiculo.alienado = 'FALSE';
        $scope.quotation.veiculo.chassi_remarcado = 'FALSE';
        $scope.quotation.veiculo.blindado = 'FALSE';
        $scope.quotation.cotacao.garagem_residencia = 'sim';
        $scope.quotation.cotacao.garagem_trabalho = 'sim';
        $scope.quotation.cotacao.garagem_estudo = 'sim';
        $scope.quotation.cotacao.utilizacao_veiculo = 'trabalho';
        $scope.quotation.cotacao.carros_residencia = '1';
        $scope.quotation.cotacao.vitima_furto = 'FALSE';
        $scope.quotation.cotacao.distancia_residencia_trabalho = 'ATE_20_KM';
        $scope.quotation.cotacao.cobertura_para_outro_condutor = 'NAO_EXISTE';
        $scope.quotation.cotacao.relacao_cliente_condutor = 'OUTROS';
        $scope.quotation.condutor_principal.sexo = 'M';
        $scope.quotation.condutor_principal.estado_civil = 'C';
        $scope.quotation.condutor_principal.tipo_residencia = 'apartamento';
        $scope.quotation.cobertura.tipo_cobertura = 'compreensiva';
        $scope.quotation.cobertura.franquia = 'normal';
        $scope.quotation.cobertura.isencao_franquia = 'false';
        $scope.quotation.cobertura.percentual_fipe = '100';
        $scope.quotation.cobertura.assistencia_24h = '500km';
        $scope.quotation.cobertura.vidros = 'completo';
        $scope.quotation.cobertura.carro_reserva = '07dias';
        $scope.quotation.cobertura.oficina = 'referenciada';
        $scope.quotation.cobertura.perfil_carro_reserva = 'basico';
        $scope.quotation.cobertura.reposicao_zero_km = 'semzerokm';
        $scope.quotation.cobertura.despesas_extraordinarias = 0;
        $scope.quotation.cotacao.comissao = 10;
        $scope.quotation.veiculo.valor_kit_gas = 0.00;
    }

    function onChangeBackupCar() {
        if ($scope.quotation.cobertura.carro_reserva === 'semcarro') {
            $scope.quotation.cobertura.perfil_carro_reserva = 'semcarro';
        }
    }

    function setPeriod() {
        $scope.quotation.cotacao.inicio_vigencia = moment().format('DD/MM/YYYY');
        $scope.quotation.cotacao.termino_vigencia = moment().add(1, 'year').format('DD/MM/YYYY');
    }

    function repeatCep() {
        if ($scope.quotation.cotacao.cep_circulacao) {
            $scope.quotation.cotacao.cep_pernoite = $scope.quotation.cotacao.cep_circulacao;
        }
    }

    function clearRenovationValues() {
        if ($scope.quotation.cotacao.renovacao === 'novo') {
            $scope.quotation.cotacao.bonus_anterior = null;
            $scope.quotation.cotacao.bonus_atual = null;
            $scope.quotation.cotacao.sinistros = null;
            $scope.quotation.cotacao.apolice_anterior = null;
            $scope.quotation.cotacao.codigo_identificacao = null;
            $scope.quotation.cotacao.final_apolice = null;
        }
    }

    function validateQuotation(quotation) {
        var quotation = UtilService.clone(quotation);
        var missingFields = [];
        var requiredQuotationFields = [
			'inicio_vigencia',
            'termino_vigencia',
            'bonus_anterior',
            'bonus_atual',
            'cep_circulacao',
            'cep_pernoite',
            'sinistros',
            'carros_residencia',
            'km_mensal',
            'condutor_secundario_idade',
            'condutor_secundario_sexo',
            'comissao'
        ];
        var requiredCustomerFields = [
            'documento',
            'nome',
            'sexo',
            'nascimento'
        ];

        var requiredVehicleFields = [
            'ano_fabricacao',
            'ano_modelo',
            'modelo'
        ];

        var requiredRiskFields = [
            'ano_fabricacao',
            'ano_modelo',
            'modelo'
        ];

        var requiredMainDriverFields = [
            'documento',
            'nome',
            'nascimento',
            'profissao',
        ];

        var requiredCoveragesFields = [
            'percentual_fipe'
        ];

        requiredCoveragesFields.forEach(function(key) {
            if (!quotation['cobertura'][key]) {
                missingFields.push('cobertura.' + key);
            }
        });

        requiredMainDriverFields.forEach(function(key) {
            if (!quotation['condutor_principal'][key]) {
                missingFields.push('condutor_principal.' + key);
            }
        }); 

        requiredRiskFields.forEach(function(key) {
            if (!quotation['veiculo'][key]) {
                missingFields.push('veiculo.' + key);
            }
        }); 

        requiredVehicleFields.forEach(function(key) {
            if (!quotation['veiculo'][key]) {
                missingFields.push('veiculo.' + key);
            }
        }); 

        requiredCustomerFields.forEach(function(key) {
            if (!quotation['cliente'][key]) {
                missingFields.push('cliente.' + key);
            }
        }); 
            
        requiredQuotationFields.forEach(function(key) {
            if (!quotation['cotacao'][key]) {
                if (key === 'bonus_anterior' && quotation['cotacao']['renovacao'] == 'novo') {
                    return;
                }

                if (key === 'bonus_atual' && quotation['cotacao']['renovacao'] == 'novo') {
                    return;
                }

                if (key === 'sinistros' && quotation['cotacao']['sinistros'] === 0) {
                    return;
                }

                if (key === 'sinistros' && quotation['cotacao']['renovacao'] == 'novo' && !quotation['cotacao']['sinistros']) {
                    return;
                }

                if (key === 'carros_residencia' && quotation['cotacao']['carros_residencia'] === 0) {
                    return;
                }

                if (key === 'condutor_secundario_idade' && quotation['cotacao']['cobertura_para_outro_condutor'] === 'NAO_EXISTE')  {
                    return;
                }

                if (key === 'condutor_secundario_sexo' && quotation['cotacao']['cobertura_para_outro_condutor'] === 'NAO_EXISTE') {
                    return;
                }

                if (key === 'comissao' && quotation['cotacao']['comissao'] === 0) {
                    return;
                }

                missingFields.push('cotacao.' + key);
            }
        });

        $scope.missingFields = missingFields.slice();
        
        if (missingFields.length) {
            return false;
        }

        return true;
    }
    
    function isMissingField(object, key) {
        return $scope.missingFields.indexOf(object + '.' + key) !== -1 ? true : false;
    }

    function calculate() {
        var isValid = validateQuotation($scope.quotation);

		if (!isValid) {
            SweetAlert.warning('Preencha todos os campos obrigatórios', 'Existem campos obrigatórios sem preenchimento, revise o formulário por gentileza');
			return;
        }

        var quotation = formatRequest($scope.quotation);
        
        $scope.loadingCalculate = true;
        $scope.calcOnProgress = false;


        // RESET LIST QUOTATIONS
        $scope.quotation.info = [];
        $scope.quotationsList = [];

        QuotationService.save(quotation).then(function(resp) {
            if (resp.data && resp.data.status == 'OK') {
                connect();
                $scope.calcOnProgress = true;
            } else {
                SweetAlert.error('Dados inválidos', resp.data.mensagem);
            }

            $scope.loadingCalculate = false;
        }).catch(function(err) {
            SweetAlert.error('Problemas de comunicação com seguradoras', 'Tente novamente em alguns minutos por gentileza, se o problema persistir, entre em contato com o suporte, código: 001');
            $scope.loadingCalculate = false;
        });
    }

    function formatRequest(quotation) {
        var formattedQuotation = UtilService.clone(quotation);

        formattedQuotation.cotacao.inicio_vigencia = moment(quotation.cotacao.inicio_vigencia, 'DD/MM/YYYY').format('YYYY-MM-DD');
        formattedQuotation.cotacao.termino_vigencia = moment(quotation.cotacao.termino_vigencia, 'DD/MM/YYYY').format('YYYY-MM-DD');
        formattedQuotation.cliente.nascimento = moment(quotation.cliente.nascimento, 'DD/MM/YYYY').format('YYYY-MM-DD');
        formattedQuotation.condutor_principal.nascimento = moment(quotation.condutor_principal.nascimento, 'DD/MM/YYYY').format('YYYY-MM-DD');
        formattedQuotation.condutor_principal.data_habilitacao = moment(quotation.condutor_principal.data_habilitacao, 'DD/MM/YYYY').format('YYYY-MM-DD');

        if (formattedQuotation.condutor_principal.data_habilitacao === 'Invalid date') {
            formattedQuotation.condutor_principal.data_habilitacao = null;
        }

        if (!formattedQuotation.cotacao.sinistros) {
            formattedQuotation.cotacao.sinistros = 0;
        }

        if (!formattedQuotation.veiculo.valor_kit_gas) {
            formattedQuotation.veiculo.valor_kit_gas = 0.00;
        }

        if (!formattedQuotation.cobertura.danos_materiais) {
            formattedQuotation.cobertura.danos_materiais = 0;
        }

        if (!formattedQuotation.cobertura.danos_corporais) {
            formattedQuotation.cobertura.danos_corporais = 0;
        }

        if (!formattedQuotation.cobertura.danos_morais) {
            formattedQuotation.cobertura.danos_morais = 0;
        }

        if (!formattedQuotation.cobertura.morte_invalidez) {
            formattedQuotation.cobertura.morte_invalidez = 0;
        }

        return formattedQuotation;
    }

    function onSelectRelation() {
        if ($scope.quotation.cotacao.relacao_cliente_condutor === 'PROPRIO' && $scope.quotation.tipo_pessoa === 'fisica') {
            $scope.quotation.condutor_principal.documento = $scope.quotation.cliente.documento;
            $scope.quotation.condutor_principal.nome = $scope.quotation.cliente.nome;
            $scope.quotation.condutor_principal.nascimento = $scope.quotation.cliente.nascimento;
            $scope.quotation.condutor_principal.sexo = $scope.quotation.cliente.sexo;
        }
    }

    function setModalInfo(quotation) {
        $scope.modalInfo = quotation;
    }

    function capitalize(word) {
        if (!word) {
            return;    
        }
        
        return word.charAt(0).toUpperCase() + word.slice(1);
    }

    function accept(quote) {
        SweetAlert.swal({
            title: 'Aceitar esta cotação?',
            text: 'Selecionando esta cotação, você aceita prosseguir com o fechamento do seguro no valor e observações descritas.',
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Aceitar',
            cancelButtonText: 'Voltar',
            confirmButtonColor: '#0E52AC'
         }, function(isConfirm) {
             if (isConfirm) {
                $scope.loading = true;
                quote.accept = true;

                QuotationService.updateGeneric({ 
                    quotation_id: $scope.quotation.id,
                    status: 'accepted',
                    info: JSON.stringify($scope.quotationsList),
                    price: quote.calculo.valor
                })
                .then(function(resp) {

                    MetaService.quotationChosenEmail({
                        customer_name: $rootScope.loggedUser.name,
                        company_name: $rootScope.loggedUser.company.name,
                        segment: 'Auto',
                        hash: $scope.quotation.hash
                    }).then(function(resp) {
                        getQuotation();
                        console.log(resp.data);
                    });
                }).catch(function(err) {
                    console.log(err);
                    $scope.loading = false;
                });
             }
         });
    }

    function order(key) {
        if (key == 'price') {
            $scope.quotationsList = $scope.quotationsList.sort(function(a, b) {
                if (a.calculo.valor > b.calculo.valor) {
                    return 1;
                }
                if (a.calculo.valor < b.calculo.valor) {
                    return -1;
                }
        
                return 0;
            });
        }
    }
    
    init();
    $scope.order = order;
    $scope.accept = accept;
    $scope.onSelectAntiTheftDevice = onSelectAntiTheftDevice;
    $scope.onSelectAntiTheftCategory = onSelectAntiTheftCategory;
    $scope.clearRenovationValues = clearRenovationValues;
    $scope.onSelectProfession = onSelectProfession;
    $scope.onChangeBackupCar = onChangeBackupCar;
    $scope.onSelectBrand = onSelectBrand;
    $scope.onSelectModel = onSelectModel;
    $scope.checkModels = checkModels;
    $scope.repeatCep = repeatCep;
    $scope.calculate = calculate;
    $scope.isMissingField = isMissingField;
    $scope.onSelectRelation = onSelectRelation;
    $scope.setModalInfo = setModalInfo;
    $scope.capitalize = capitalize;
});