angular.module('vencorr').controller('InjuriesController', function($scope, CustomerService, InjuryService, $base64) {
    $scope.loading = false;
    $scope.itemsPerPage = 10;
    $scope.chooseCustomer = true;
    $scope.filterOption = 'all';

    function init() {
        list();
    }

    function checkFilter(filter) {
        if (filter === 'all') {
            list();
        }
    }

    function list(filter) {
        $scope.loading = true;
        var params = {
            page: $scope.page || 1,
        };

        if ($scope.filterOption === 'date') {
            params.start = $scope.startDate;
            params.end = $scope.endDate;
        }

        if ($scope.filterOption === 'customer') {
            params.customer = $scope.customer.id || null;
        }

        if ($scope.filterOption === 'status') {
            params.status = $scope.status;
        }

        InjuryService.list({ params: params }).then(function(resp) {
            $scope.totalInjuries = +resp.data.amount;

            if (!resp.data.injuries.length) {
                $scope.injuries = [];
                $scope.loading = false;
                return;
            }
            
            $scope.injuries = resp.data.injuries.map(function(injury) {
                injury.encodedId = $base64.encode(injury.id);
                injury.typeName = formatTypeName(injury.type);
                injury.statusName = formatStatusName(injury.status);
                return injury;
            });


            $scope.loading = false;
        }).catch(function(err) {
            $scope.loading = false;
        });
    }

    function formatTypeName(type) {
        if (type === 'basic') {
            return 'Básica';
        }

        if (type === 'electrical') {
            return 'Danos Elétricos';
        }

        if (type === 'civil') {
            return 'Responsabilidade Civil';
        }

        if (type === 'glass') {
            return 'Vidros';
        }

        if (type === 'other') {
            return 'Outros';
        }
    }

    function formatStatusName(status) {
        if (status === 'open') {
            return 'Aberta';
        }

        if (status === 'in_progress') {
            return 'Em andamento';
        }

        if (status === 'waiting') {
            return 'Aguardando Seguradora';
        }

        if (status === 'approved') {
            return 'Aprovada';
        }

        if (status === 'refused') {
            return 'Negada';
        }
    }

    function onSelectCustomer(customer) {
        $scope.customer = customer;
        $scope.chooseCustomer = false;
        list();
    }

    function getCustomers(typedTerm) {
		return CustomerService.suggest(typedTerm).then(function(resp) {
			return resp.data.map(function(item) {
				return item;
			});
		});
    };

    function enableCustomerSearch() {
		$scope.chooseCustomer = true;
        $scope.searchCustomer.value = null;
        $scope.customer = null;
	}

    function setDateFilter(period) {
        if (period === 'today') {
            $scope.startDate = moment().format('DD/MM/YYYY');
            $scope.endDate = moment().format('DD/MM/YYYY');
        }

        if (period === 'yesterday') {
            $scope.startDate = moment().subtract(1,'day').format('DD/MM/YYYY');
            $scope.endDate = moment().subtract(1,'day').format('DD/MM/YYYY');
        }

        if (period === 'week') {
            $scope.startDate = moment().day(0).format('DD/MM/YYYY');
            $scope.endDate = moment().day(6).format('DD/MM/YYYY');
        }

        if (period === 'lastWeek') {
            $scope.startDate = moment().day(0).subtract(1,'week').format('DD/MM/YYYY');
            $scope.endDate = moment().day(6).subtract(1,'week').format('DD/MM/YYYY');
        }

        if (period === 'month') {
            $scope.startDate = moment().startOf('month').format('DD/MM/YYYY');
            $scope.endDate = moment().endOf('month').format('DD/MM/YYYY');
        }

        if (period === 'lastMonth') {
            $scope.startDate = moment().startOf('month').subtract(1,'months').format('DD/MM/YYYY');
            $scope.endDate = moment().endOf('month').subtract(1,'months').format('DD/MM/YYYY');
        }

        if (period === 'nextMonth') {
            $scope.startDate = moment().startOf('month').add(1,'months').format('DD/MM/YYYY');
            $scope.endDate = moment().endOf('month').add(1,'months').format('DD/MM/YYYY');
        }

        list();
    }

    init();
    $scope.setDateFilter = setDateFilter;
    $scope.enableCustomerSearch = enableCustomerSearch;
    $scope.onSelectCustomer = onSelectCustomer;
    $scope.getCustomers = getCustomers;
    $scope.checkFilter = checkFilter;
    $scope.list = list;
});
