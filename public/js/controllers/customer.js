angular.module('vencorr').controller('CustomerController', function($scope, CustomerService, $base64, $routeParams, $window) {
    $scope.loading = false;

    function init() {
        if ($routeParams.id) {
            $scope.customerId = $base64.decode($routeParams.id);
            getCustomer($scope.customerId);
        } else {
            $location.path('/clientes');
        }
    }

    function goToEdit() {
        $window.location.href ='/cliente-cadastrar/' + $routeParams.id;
    }

    function getCustomer(customerId) {
        if (!customerId) {
            return;
        }

        $scope.loading = true;
		CustomerService.getById(customerId).then(function(resp) {
            $scope.loading = false;
            $scope.customer = resp.data;
		}).catch(function(err) {
            $scope.loading = false;
        });
    }
   
    init();
    $scope.goToEdit = goToEdit;
});