var baseUrl;
var deploy = 'local';
var production = ['franqueado.vencorr.com', 'https://franqueado.vencorr.com'];
var loginPath;
var redirectUriLinkedin;
var uploadEndpoint;

if (production.indexOf(window.location.host) !== -1) {
	deploy = 'prod';
	baseUrl = 'https://franqueado.vencorr.com:1337/api';
	loginPath = 'https://franqueado.vencorr.com';
	redirectUriLinkedin = 'https%3A%2F%2Ffranqueado.vencorr.com%2Fauth%2Flinkedin%2Fcallback';
	uploadEndpoint = 'https://franqueado.vencorr.com:1337/api/upload';
	chatSocket = 'https://digital.vencorr.com:1338';
	chatAPIUrl = 'https://digital.vencorr.com:1338/api';
	quotationAutoSocket = 'https://franqueado.vencorr.com:1337';
}

if (deploy === 'local') {
	baseUrl = 'http://localhost:1337/api';
	loginPath = 'http://franchisee.test/';
	redirectUriLinkedin = 'http%3A%2F%2Ffranchisee.test%2Fauth%2Flinkedin%2Fcallback';
	uploadEndpoint = 'http://localhost:1337/api/upload';
	chatSocket = 'http://localhost:1338';
	chatAPIUrl = 'http://localhost:1338/api';
	quotationAutoSocket = 'http://localhost:1337';
}

angular.module('vencorr').value("config", {
	baseUrl: baseUrl,
	loginPath: loginPath,
	redirectUriLinkedin: redirectUriLinkedin,
	uploadEndpoint: uploadEndpoint,
	chatSocket: chatSocket,
	chatAPIUrl: chatAPIUrl,
	quotationAutoSocket: quotationAutoSocket
});