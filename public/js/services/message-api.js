angular.module('vencorr').factory('MessageService', function ($http, config) {
    function list(id) {
        return $http.get(config.baseUrl + '/messages/' + id);
    }

    function add(data) {
        return $http.post(config.baseUrl + '/messages', { message: data });
    }

    return {
        add: add,
        list: list
    }
});