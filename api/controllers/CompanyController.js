async function updateGoal(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { goal } = req.body;
    
    if (!tokenData.company_id || !tokenData.seller_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    const updatedCompany = await Company.updateOne({
        id: tokenData.company_id
    }).set({
        agro_goal: goal
    });

    res.status(200).json(updatedCompany);
}

async function getGoal(req, res) {
    const tokenData = UtilService.getTokenData(req);

    if (!tokenData.company_id || !tokenData.seller_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    const goal = await Company.findOne({
        where: {
            id: tokenData.company_id
        },
        select: ['agro_goal']
    });

    res.status(200).json(goal);
}

module.exports = {
  updateGoal,
  getGoal
};

