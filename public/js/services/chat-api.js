angular.module("vencorr").factory("ChatService", function ($http, config) {
    function saveMessage(data) {
        return $http.put(config.chatAPIUrl + "/chat-update-messages", data);
    }

    return {
        saveMessage: saveMessage,
    };
});