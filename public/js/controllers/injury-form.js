angular.module('vencorr').controller('InjuryFormController', function($scope, $rootScope, $base64, $window, $routeParams, Upload, config, SweetAlert, UtilService, CustomerService, QuotationService, InjuryService, MetaService) {
    $scope.loading = false;
    $scope.injuryId = $routeParams.id;
    $scope.dynamic = 0;
    $scope.type = 'success';
    $scope.files = [];
    $scope.injury = {};
    $scope.isEdit = false;
    $scope.chooseCustomer = true;
    $scope.loadingQuotations = false;
    $scope.knownFormats = ['pdf', 'xlsx', 'xls', 'doc', 'docx', 'png', 'jpeg', 'jpg', 'mp4', 'avi'];
    $scope.searchCustomer = {};

    function init() {
        $scope.injury.type = 'basic';
        $scope.injury.event_date = moment().format('DD/MM/YYYY');

        if ($scope.injuryId) {
            $scope.injuryId = $base64.decode($scope.injuryId);
            $scope.isEdit = true;
            getInjury($scope.injuryId);
        }
    }

    function getInjury(injuryId) {
        if (!injuryId) {
            return;
        }

        $scope.loading = true;
		InjuryService.get(injuryId).then(function(resp) {
            $scope.loading = false;
            $scope.injury = resp.data;
            $scope.customer = resp.data.customer.id;
            $scope.customerEncodedId = $base64.encode(resp.data.customer.id);
            $scope.searchCustomer.value = resp.data.customer.name;
            $scope.chooseCustomer = false;
            $scope.injury.event_date = moment(resp.data.event_date, 'YYYY-MM-DD').format('DD/MM/YYYY');
            $scope.files = JSON.parse($scope.injury.files);
            $scope.injury.files = JSON.parse($scope.injury.files);
            $scope.injury.resolution_files = JSON.parse($scope.injury.resolution_files);
            onSelectCustomer(resp.data.customer);
            getQuotationById($scope.injury.quotation);
		}).catch(function(err) {
            $scope.loading = false;
        });
    }

    function removeFile(path) {
        $scope.files.splice($scope.files.indexOf(path), 1);
    }

    function getQuotationById(id) {
        $scope.loading = true;
        QuotationService.getById(id).then(function(resp) {
            $scope.loading = false;
            $scope.searchedQuotation = resp.data;
        }).catch(function(err) {
            $scope.loading = false;
        });
    }

    function formatStatusName(status) {
        if (status === 'open') {
            return 'Aberta';
        }

        if (status === 'in_progress') {
            return 'Em andamento';
        }

        if (status === 'waiting') {
            return 'Aguardando Seguradora';
        }

        if (status === 'approved') {
            return 'Aprovada';
        }

        if (status === 'refused') {
            return 'Negada';
        }
    }

    function formatTypeName(type) {
        if (type === 'basic') {
            return 'Básica';
        }

        if (type === 'electrical') {
            return 'Danos Elétricos';
        }

        if (type === 'civil') {
            return 'Responsabilidade Civil';
        }

        if (type === 'glass') {
            return 'Vidros';
        }

        if (type === 'other') {
            return 'Outros';
        }
    }

    function save(injury) {
        var isValid = validateInjury(injury);

        if (!isValid) {
            SweetAlert.warning('Preencha todas as informações', $scope.missingFields.toString().replace(/,/g, ', '));
            return;
        }

        $scope.loading = true;
        injury.files = JSON.stringify($scope.files);
        injury.customer = $scope.customer.id || null;
        injury.user = $rootScope.loggedUser.id || null;
        InjuryService.save(injury).then(function(resp) {

            // Send email
            MetaService.injuryOpenedEmail({
                customer_name: $rootScope.loggedUser.name,
                company_name: $rootScope.loggedUser.company.name,
                id: resp.data.id
            }).then(function(response) {
                $scope.loading = false;
                $window.location.href = '/sinistros';
            });
        }).catch(function(err) {
            console.log(err);
            SweetAlert.error('Ops, aconteceu um erro', 'Atualize a página e tente novamente, se o erro persistir, entre em contato com o suporte');
            $scope.loading = false;
        })
    }

    function capitalize(word) {
        if (!word) {
            return;    
        }
        
        return word.charAt(0).toUpperCase() + word.slice(1);
    }

    function update(injury) {
        var isValid = validateInjury(injury);

        if (!isValid) {
            SweetAlert.warning('Preencha todas as informações', $scope.missingFields.toString().replace(/,/g, ', '));
            return;
        }

        $scope.loading = true;
        injury.files = JSON.stringify($scope.files);
        injury.customer = $scope.customer.id || null;
        InjuryService.update(injury).then(function(resp) {
            $window.location.href = '/sinistros';
        }).catch(function(err) {
            SweetAlert.error('Ops, aconteceu um erro', 'Atualize a página e tente novamente, se o erro persistir, entre em contato com o suporte');
            $scope.loading = false;
        })
    }

    function validateInjury(injury) {
        $scope.missingFields = [];
        var testInjury = UtilService.clone(injury);
        var missingFields = [];
        
        if (!testInjury.complainant) {
            missingFields.push('Nome do Reclamante');
        }

        if (!$scope.customer || !$scope.customer.id) {
            missingFields.push('Cliente');
        }

        if (!testInjury.quotation) {
            missingFields.push('Cotação');
        }

        if (!testInjury.description) {
            missingFields.push('Descrição');
        }

        if (!missingFields.length) {
            return true;
        } else {
            $scope.missingFields = missingFields;
            return false;
        }
    }

    function getCustomers(typedTerm) {
		return CustomerService.suggest(typedTerm).then(function(resp) {
			return resp.data.map(function(item) {
				return item;
			});
		});
    };
    
    function onSelectCustomer(customer) {
        $scope.customer = customer;
        $scope.chooseCustomer = false;

        $scope.loadingQuotations = true;
        QuotationService.getQuotationsByCustomer(customer.id).then(function(resp) {
            $scope.quotations = resp.data;

            $scope.quotations.forEach(function(item) {
                item.listDescription = moment(item.created_at).format('DD/MM/YYYY') + ' - ' + formatSegmentName(item.segment_type) + ' - ' + item.hash;
            });

            $scope.loadingQuotations = false;
        }).catch(function(err) {
            console.log('Aconteceu um Erro', err);
            $scope.loadingQuotations = false;
        });
    }

    function formatSegmentName(segment) {
        if (segment === 'auto') {
            return 'Auto';
        }

        if (segment === 'agro') {
            return 'Agrícola';
        }

        if (segment === 'construcao') {
            return 'Construção';
        }

        if (segment === 'empresarial') {
            return 'Empresarial';
        }

        if (segment === 'residencial') {
            return 'Residencial';
        }

        if (segment === 'equipamento') {
            return 'Equipamento';
        }

        if (segment === 'outras') {
            return 'Outras';
        }
    }

    function uploadFiles(files, errFiles) {
        $scope.uploadingFiles = files;
        $scope.errFiles = errFiles;
        $scope.finished = 0;
        $scope.hideProgressBars = false;

        angular.forEach(files, function(file) {
            file.upload = Upload.upload({
                url: config.uploadEndpoint,
                data: { file: file,'folder': 'sinistros' },
            });

            file.upload.then(function (response) {
                $scope.files.push(response.data[0]);
                
                $scope.finished++;

                if ($scope.finished == files.length) {
                    $scope.hideProgressBars = true;
                }
            }, function (response) {
                console.log(response.status + ': ' + response.data);
                SweetAlert.warning('Ocorreu um erro com um dos arquivos', 'Se o problema persistir, tente usar outro arquivo ou informe ao suporte.');
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        });
    }

    function triggerUpload() {
        document.getElementById('imageUpload').click();
    };

    function getExtension(filename) {
        if (!filename) {
            return;
        }

        return UtilService.extractExtension(filename);
    }

    function enableCustomerSearch() {
		$scope.chooseCustomer = true;
        $scope.searchCustomer.value = null;
        $scope.customer = null;
        $scope.quotations = [];
        $scope.injury.quotation = null;
	}

    init();
    $scope.formatTypeName = formatTypeName;
    $scope.formatSegmentName = formatSegmentName;
    $scope.formatStatusName = formatStatusName;
    $scope.update = update;
    $scope.save = save;
    $scope.capitalize = capitalize;
    $scope.removeFile = removeFile;
    $scope.enableCustomerSearch = enableCustomerSearch;
    $scope.getExtension = getExtension;
    $scope.uploadFiles = uploadFiles;
    $scope.getCustomers = getCustomers;
    $scope.triggerUpload = triggerUpload;
    $scope.onSelectCustomer = onSelectCustomer;
});
