module.exports = {
  attributes: {
		description: {
			type: 'string'
    },
    price: {
      type: 'ref',
      columnType: 'float'
    },
    status: {
			type: 'string'
    },
    payment: {
			type: 'string'
    },
    due_date: {
			type: 'ref',
			columnType: 'date',
    },
    paid_date: {
			type: 'ref',
      columnType: 'date',
    },
    confirm_date: {
			type: 'ref',
      columnType: 'date'
    },
    quotation: {
      model: 'quotation',
      columnName: 'quotation_id'
    },
    user: {
      model: 'user',
      columnName: 'user_id'
    },
    admin_user: {
      model: 'admin',
      columnName: 'admin_user'
    },
    company: {
      model: 'company',
      columnName: 'company_id'
    }
	},
};

