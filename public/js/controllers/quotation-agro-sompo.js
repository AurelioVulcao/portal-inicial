angular.module('vencorr').controller('QuotationAgroSompoController', function($scope, uuid, $routeParams, $location, SompoService, SweetAlert, UtilService, QuotationService, SompoBrandService) {
    $scope.loading = true;
    $scope.items = [];
    $scope.selectedCoverages = [];
    $scope.installmentsReady = false;
    $scope.quotedItems = [];
    $scope.brands = [];
    $scope.quotedItem = {};
    $scope.quotationId;
    $scope.quotationHash = $routeParams.hash;
    $scope.isEdit = false;

    function init() {
        listProdutcs(function() {
            availableCoverages();
            setPeriod();
            
            if ($scope.quotationHash) {
                $scope.isEdit = true;
                getQuotationData($scope.quotationHash);
            }
        });
        $scope.comission = '40%';
    }

    function onSelectItem(itemId) {
        $scope.selectedCoverages = [];
        setInitialCoverages();
        populateBrands(itemId);

        $scope.items.forEach(function(product) {
            if (product.id === itemId) {
                $scope.quotedItem = product;
            }
        });

        $scope.quotedItem.year = '2019';
        $scope.selectedCoverages[0].multiple = '1';
    }

    function populateBrands(productId) {
        $scope.brands = SompoBrandService.getAvailableBrands(productId);
    }

    function addItemToList() {
        var data = formatData();
        var invalidCoverages = 0;

        if (!$scope.selectedCoverages.length) {
            SweetAlert.warning('É obrigatório adicionar cobertura de roubo e furto');
            return;
        }

        $scope.selectedCoverages.forEach(function(coverage) {
            if (!coverage.lmi) {
                invalidCoverages++;
                SweetAlert.warning('LMI não preenchido para: ' + coverage.name + '', 'Adicione um valor de LMI');
                return;
            }
        });

        if (invalidCoverages) {
            return;
        }

        var isCoveragesValid = validateCoveragesLimits($scope.selectedCoverages, data.price);
        
        if (!isCoveragesValid) {
            return;
        }
        
        reset();
        $scope.loading = false;
        $scope.quotedItems.push(data);
    }

    function validateCoveragesLimits(coverages, itemPrice) {
        var isValid = true;

        coverages.forEach(function(coverage) {
            if (coverage.name === 'Responsabilidade Civil') {
                if (+coverage.lmi > 300000) {
                    SweetAlert.warning('LMI Acima do limite máximo', 'O valor máximo da cobertura "Responsabilidade Civil" é de 300 Mil reais');
                    isValid = false;
                }

                if (+coverage.lmi < 30000) {
                    SweetAlert.warning('LMI abaixo do limite mínimo', 'O valor mínimo da cobertura de "Responsabilidade Civil" é de 30 Mil reais');
                    isValid = false;
                }
            } else {
                if (+coverage.lmi > itemPrice) {
                    SweetAlert.warning('LMI Acima do limite mínimo', 'O valor máximo permitido para coberturas é de 100% do valor do item, exceto RC');
                    isValid = false;
                }
            }
        });

        return isValid;
    }

    function editItem(index, item) {
        populateBrands(item.id);
        $scope.quotedItems.splice(index, 1);
        $scope.quotedItem = JSON.parse(JSON.stringify(item));
        fillCoverages($scope.quotedItem);
    }

    function save() {
        var data = {
            start_term: $scope.startTerm,
            end_term: $scope.endTerm,
            comission: $scope.comission,
            items: $scope.quotedItems,
            segment: 'agro'
        };

        if (!$scope.quotedItems) {
            return;
        }

        $scope.loading = true;
        QuotationService.save(data).then(function(resp){
            $location.path('/cotacao-agro-sompo/' + resp.data.hash);
        }).catch(function(err) {
            console.log('Err', err);
        });
    }

    function getQuotationData(hash) {
        $scope.loading = true;
        QuotationService.get(hash).then(function(resp) {
            $scope.loading = false;
            $scope.quotationId = resp.data.id;
            $scope.comission = resp.data.comission;
            $scope.startTerm = moment(resp.data.start_term, 'YYYY-MM-DD').format('DD/MM/YYYY');
            $scope.endTerm = moment(resp.data.end_term, 'YYYY-MM-DD').format('DD/MM/YYYY');
            $scope.quotedItems = JSON.parse(resp.data.items);
            $scope.quotationStatus = resp.data.status;

            if (resp.data.customer && resp.data.customer.id) {
                $scope.customer_id = resp.data.customer.id || null;
            }

            $scope.quotedItems.forEach(function(item) {
                fillCoverages(item);
            });
        });
    }

    function update() {
        var data = {
            id: $scope.quotationId,
            start_term: $scope.startTerm,
            end_term: $scope.endTerm,
            comission: $scope.comission,
            items: $scope.quotedItems,
            hash: $scope.quotationHash,
            customer_id: $scope.customer_id || null
        };

        if ($scope.quotationStatus === 'accepted') {
            SweetAlert.error("Proposta já aceita!", "Não é possível editar propostas com aceitação confirmada.");
            return;
        }

        $scope.loading = true;
        QuotationService.update(data).then(function(resp) {
            $location.path('/cotacao-agro-sompo/' + resp.data.hash);
        }).catch(function(err) {
            console.log('Err', err);
        });
    }

    function setInitialCoverages() {
        $scope.selectedCoverages.push({
            name: 'Roubo/Furto',
            lmi: null,
            multiple: null
        });
    }

    function fillCoverages(item) {
        if (!item.coverages.length) {
            return;
        }

        $scope.selectedCoverages = [];
        item.coverages.forEach(function(coverage) {
            $scope.selectedCoverages.push({
                name: coverage.name,
                lmi: coverage.lmi,
                multiple: coverage.multiple || 1
            });
        });
    }

    function reduceYears(year) {
        var min = +moment().format('YYYY');
        var endTerm = moment($scope.endTerm, 'DD/MM/YYYY').format('YYYY');
        min += 1;

        if (endTerm <= min) {
            return;
        }

        $scope.endTerm = moment($scope.endTerm, 'DD/MM/YYYY').subtract(1, 'year').format('DD/MM/YYYY');
    }

    function increaseYears(year) {
        var max = +moment().add(5, 'year').format('YYYY');
        var endTerm = moment($scope.endTerm, 'DD/MM/YYYY').year();

        if (endTerm >= max) {
            return;
        }

        $scope.endTerm = moment($scope.endTerm, 'DD/MM/YYYY').add(1, 'year').format('DD/MM/YYYY');
    }

    function setPeriod() {
        $scope.startTerm = moment().format('DD/MM/YYYY');
        $scope.endTerm = moment().add(1, 'year').format('DD/MM/YYYY');
    }

    function listProdutcs(callback) {
        $scope.loading = true;
        SompoService.list().then(function(resp) {
            $scope.items = resp.data;
            $scope.loading = false;

            callback();
        }).catch(function(err) {
            $scope.loading = false;
            console.log('ERRO', err);
        });
    }

    function availableCoverages() {
        $scope.availableCoverages = [
            'Danos Elétricos',
            'Pagamento de Aluguel a Terceiros  (PI = 3 meses)',
            'Responsabilidade Civil'
        ];
    }

    function selectCoverage(coverageName) {
        if (!$scope.quotedItem.price) {
            return;
        }

        $scope.availableCoverages.splice($scope.availableCoverages.indexOf(coverageName), 1);
        $scope.selectedCoverages.push({
            name: coverageName,
            multiple: '1'
        });
    }

    function removeCoverage(index, coverageName) {
        $scope.selectedCoverages.splice(index, 1);
        $scope.availableCoverages.unshift(coverageName);
    }

    function formatData() {
        var data = {};

        data.item = $scope.quotedItem;
        data.item.coverages = $scope.selectedCoverages;
        data.item.comission = $scope.comission;
        data.item.uuid = uuid.v4();

        return data.item;
    }

    function reset() {
        $scope.quotedItem = {};
        $scope.selectedCoverages = [];
        $scope.availableCoverages = [];
        $scope.selectedCoverages.push({
            name: 'Roubo/Furto',
            lmi: null,
            multiple: null
        });

        $scope.availableCoverages = [
            'Danos Elétricos',
            'Pagamento de Aluguel a Terceiros  (PI = 3 meses)',
            'Responsabilidade Civil'
        ];
    }

    function removeItem(index, item) {
        $scope.quotedItems.splice(index, 1);
    }

    function addDefaultCoverage(price) {
        if (price) return false;
        SweetAlert.swal("Preencha o preço do item", "Para prosseguir é necessário preencher o preço do item", "warning");
    }

    function suggestPrice(index, coverageName) {
        if (coverageName === 'Roubo/Furto') {
            $scope.selectedCoverages[index].lmi = $scope.quotedItem.price;
        }

        if (coverageName === 'Danos Elétricos') {
            $scope.selectedCoverages[index].lmi = (($scope.quotedItem.price * 20) / 100).toFixed(2);
        }
    }

    function duplicateItem(item) {
        var itemCopy = JSON.parse(JSON.stringify(item));

        itemCopy.uuid = uuid.v4();
        $scope.quotedItems.push(itemCopy);
    }

    init();
    $scope.addDefaultCoverage = addDefaultCoverage;
    $scope.removeCoverage = removeCoverage;
    $scope.selectCoverage = selectCoverage;
    $scope.addItemToList = addItemToList;
    $scope.onSelectItem = onSelectItem;
    $scope.suggestPrice = suggestPrice;
    $scope.duplicateItem = duplicateItem;
    $scope.increaseYears = increaseYears;
    $scope.reduceYears = reduceYears;
    $scope.removeItem = removeItem;
    $scope.editItem = editItem;
    $scope.update = update;
    $scope.save = save;
});
