angular.module('vencorr').controller('ChatController', function($scope, $rootScope, config, $base64, MetaService, ChatService) {
    $scope.chatSocket = config.chatSocket;
    $scope.loading = false;
    $scope.showUnavailableMessage = false;
    $scope.messages = [];
    $scope.connectedUser = $rootScope.loggedUser;
    delete $scope.connectedUser.token;
    $scope.room = $base64.encode($rootScope.loggedUser.id);
    $scope.blockChat = false;
    $scope.currentChatId;
    
    function init() {
        checkTime();
        $scope.messages.push({
            content: 'Olá, seja bem vindo(a) ao nosso atendimento por chat, no que podemos ajudar?',
            user: 'Vencorr'
        });
    }

    function checkTime() {
        $scope.loading = true;
        MetaService.getTime().then(function(resp) {
            var hour = moment(resp.data).hour();
            $scope.loading = false;

            if (hour < 8 || hour >= 17) {
                $scope.messageUnavailable = 'Nosso atendimento via chat funciona em horário comercial, volte em breve por favor';
                $scope.showUnavailableMessage = true;
                return;
            }
            
            if (hour == 12) {
                $scope.messageUnavailable = 'Olá, estamos em horário de almoço no momento, volte em breve para atendimento via chat';
                $scope.showUnavailableMessage = true;
                return;
            }

            $scope.showUnavailableMessage = false;
        });
    }

    io.socket.on('connect', function() {
        io.socket.post('/api/chat-connect', { customer: $scope.connectedUser, room: $scope.room }, function serverResponded (chatId, JWR) {
            $scope.currentChatId = chatId;
        });
    });

    io.socket.on('disconnect', function() {
        $scope.messages.push({
            content: ' ',
            user: ' '
        });
        $scope.messages.push({
            content: 'Obrigado por utilizar nosso suporte via chat. A sessão foi encerrada',
            user: '-- Portal do Franqueado --'
        });
        $scope.messages.push({
            content: ' ',
            user: ' '
        });
        $scope.blockChat = true;
        $scope.$apply();
    });

    io.socket.on('leave', function() {
        $scope.messages.push({
            content: ' ',
            user: ' '
        });
        $scope.messages.push({
            content: 'Obrigado por utilizar nosso suporte via chat. A sessão foi encerrada',
            user: '-- Portal do Franqueado --'
        });
        $scope.messages.push({
            content: ' ',
            user: ' '
        });
        $scope.blockChat = true;
        $scope.$apply();
    });

    io.socket.on('chat', function(message) {
        $scope.messages.push(message);
        $scope.$apply();

        ChatService.saveMessage({
            chat_id: $scope.currentChatId,
            messages: JSON.stringify($scope.messages)
        });

        // Scroll chat always bottom
        var messageBody = document.querySelector('#chatArea');
        messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
    });
    
    function sendMessage(content) {
        var content = content;

        if (!content) {
            return;
        }

        $scope.content = '';
        io.socket.post('/api/chat', { 
            user: $scope.connectedUser.name,
            content: content,
            room: $scope.room
        }, function(body, JWR) {});
    }

    init();
    $scope.sendMessage = sendMessage;
});