module.exports.routes = {
    // LOGIN
    'POST /api/user': { controller: 'UserController', action:'create' },
    'POST /api/login': { controller: 'UserController', action:'authenticate' },
    'POST /api/social/login': { controller: 'UserController', action:'socialAuthentication' },
    'POST /api/auth/linkedin/callback': { controller: 'UserController', action:'linkedinCallback' },
    
    // QUOTATION
    'POST /api/quotations/customer': { controller: 'QuotationController', action:'listByCustomer' },
    'GET /api/quotation/:hash': { controller: 'QuotationController', action:'get' },
    'PUT /api/quotation': { controller: 'QuotationController', action:'updateQuotation' },
    'PUT /api/quotation-update': { controller: 'QuotationController', action:'update' },
    'POST /api/quotation': { controller: 'QuotationController', action:'gatewaySegment' },
    'POST /api/quotations': { controller: 'QuotationController', action:'list' },
    'GET /api/quotation/byid/:quotation_id': { controller: 'QuotationController', action:'getById' },
    'PUT /api/quotation/status': { controller: 'QuotationController', action:'updateStatus' },
    'PUT /api/quotation/update/queue-info': { controller: 'QuotationController', action:'updateQueueId' },
    'PUT /api/quotation/update/period': { controller: 'QuotationController', action:'updatePeriod' },
    'POST /api/quotation/callback/auto': { controller: 'QuotationController', action:'autoReturnCallback' },
    'PUT /api/quotation/update/generic': { controller: 'QuotationController', action:'updateGenericQuotation' },
    
    // UPDATE QUOTATION
    'PUT /api/installments': { controller: 'QuotationController', action:'updateQuotationInstallments' },
    'PUT /api/items': { controller: 'QuotationController', action:'updateItemAdditionalInfo' },
    'PUT /api/colaborator-code': { controller: 'QuotationController', action:'updateColaboratorCode' },

    // CUSTOMER
    'POST /api/search/cep': { controller: 'CustomerController', action:'searchCep' },
    'POST /api/customer': { controller: 'CustomerController', action:'save' },
    'PUT /api/customer': { controller: 'CustomerController', action:'update' },
    'GET /api/customer/:customer_id': { controller: 'CustomerController', action:'get' },
    'POST /api/customers': { controller: 'CustomerController', action:'list' },
    'POST /api/customers/suggest': { controller: 'CustomerController', action:'suggest' },
    
    // ROBOTS QUOTATION
    'POST /api/queue/job': { controller: 'QueueController', action:'save' },

    // COMISSION
    'POST /api/comissions': { controller: 'ComissionController', action:'list' },
    'PUT /api/comission': { controller: 'ComissionController', action:'updateStatus' },

    // UPLOAD FILES
    'POST /api/upload': { controller: 'FileController', action:'upload' },

    // INJURY
    'POST /api/injury/': { controller: 'InjuryController', action:'save' },
    'PUT /api/injury/': { controller: 'InjuryController', action:'update' },
    'GET /api/injury/:injury_id': { controller: 'InjuryController', action:'get' },
    'POST /api/injuries': { controller: 'InjuryController', action:'list' },

    // META
    'POST /api/external/email': { controller: 'MetaController', action:'externalEmail' },
    'POST /api/external/franchisee': { controller: 'MetaController', action:'franchiseeContactSiteEmail' },
    'GET /api/hour': { controller: 'MetaController', action:'getHour' },
    'POST /api/external/informative/email': { controller: 'MetaController', action:'externalInformativeEmail' },
    'POST /api/admin/quotation-request': { controller: 'MetaController', action:'quotationRequestEmail' },
    'POST /api/admin/quotation-chosen': { controller: 'MetaController', action:'quotationChosenEmail' },
    'POST /api/admin/injury': { controller: 'MetaController', action:'injuryOpenedEmail' },
    'GET /api/auth/rd/callback': { controller: 'MetaController', action:'callBackRDStation' },
    'POST /api/support/offline/email': { controller: 'MetaController', action:'supportOfflineEmail' },
    'GET /api/hash': { controller: 'MetaController', action:'generateHash' },
    'POST /api/direct-email': { controller: 'MetaController', action:'directMessageEmail' },
    
    //DASHBOARD
    'POST /api/dashboard/resume': { controller: 'DashboardController', action:'resume' },
    'POST /api/dashboard/comission-graph': { controller: 'DashboardController', action:'comissionsGraph' },
    'POST /api/dashboard/quotations': { controller: 'DashboardController', action:'lastQuotations' },
    'POST /api/dashboard/injuries': { controller: 'DashboardController', action:'lastInjuries' },
    'POST /api/dashboard/activities': { controller: 'DashboardController', action:'lastActivities' },

    // CRON
    'GET /api/loldsoir8934hir438239dsaiuhdainv9rfffv98f': { controller: 'JobHandlerController', action:'handle' },
    'POST /api/oidfNdfion90fdj0sdfjjlgfkjncmxnwsdwhufhiu': { controller: 'JobHandlerController', action:'errorFinder' },
    'GET /api/aorjkf8847620fojm598db69f7dfjekld8934jedkufd98': { controller: 'JobHandlerController', action:'clearBlacklistTokens' },

    // SEGFY
    'GET /api/segfy/professions': { controller: 'SegfyController', action:'listProfessions' },
    'GET /api/segfy/brands': { controller: 'SegfyController', action:'listBrands' },
    'GET /api/segfy/models/:brand_id/:model_year': { controller: 'SegfyController', action:'listModels' },
    'GET /api/segfy/antitheft/:category': { controller: 'SegfyController', action:'listAntiTheft' },

    // SOCKET AUTO QUOTATION
    'POST /api/socket-quotation-auto': { controller: 'QuotationController', action:'onConnect' },

    // MESSAGE
    'GET /api/messages/:message_id': { controller: 'MessageController', action:'listById' },
    'POST /api/messages': { controller: 'MessageController', action:'addMessage' },
};
