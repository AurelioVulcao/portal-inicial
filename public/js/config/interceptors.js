angular.module('vencorr').config(function($httpProvider) {
    $httpProvider.interceptors.push('PreventTemplateCache');
    $httpProvider.interceptors.push('AuthInterceptor');
});