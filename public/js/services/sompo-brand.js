angular.module('vencorr').factory('SompoBrandService', function($window, $base64) {
    
    function getAvailableBrands(productId) {
        if (productId == 11 || productId == 3
            || productId == 6 || productId == 8
            || productId == 9 || productId == 12
            || productId == 17 || productId == 18
            || productId == 24 || productId == 33) return ['OUTROS'];
        
        if (productId == 1) {
            return [
                'OUTROS',
                'AGRALE',
                'AGRITECH',
                'BALDAN',
                'CASE',
                'CATERPILLAR',
                'CBT',
                'FIAT ALLIS',
                'FORD',
                'HENNIPMAN',
                'JAN',
                'JOHN DEERE',
                'KOMATSU',
                'LS TRACTOR',
                'MARCHESAN',
                'MASSEY FERGUSON',
                'MONTANA',
                'MOTOCANA',
                'NEW HOLLAND',
                'SANTAL',
                'STARA',
                'TATU MARCHESAN',
                'VALTRA VALMET',
                'VOLVO',
                'YANMAR'
            ];
        }

        if (productId == 2) {
            return [
                'OUTROS',
                'CASE',
                'FORD',
                'JACTO',
                'JAN',
                'JOHN DEERE',
                'MASSEY FERGUSON',
                'MONTANA',
                'NEW HOLLAND',
                'STARA',
                'VALTRA VALMET',
                'VOLVO'
            ];
        }

        if (productId == 4) {
            return [
                'OUTROS',
                'CASE',
                'CATERPILLAR',
                'JOHN DEERE',
                'MASSEY FERGUSON',
                'NEW HOLLAND',
                'SANTAL',
                'VALTRA VALMET'
            ];
        }

        if (productId == 5) {
            return [
                'OUTROS',
                'CASE',
                'FORD',
                'JACTO',
                'JOHN DEERE',
                'MASSEY FERGUSON',
                'MIAC',
                'MONTANA',
                'NEW HOLLAND',
                'OXBO',
                'SANTAL',
                'SLC',
                'STARA',
                'TATU MARCHESAN',
                'TURIM',
                'VALTRA VALMET'
            ];
        }

        if (productId == 7) {
            return [
                'OUTROS',
                'CASE',
                'CLAAS JAGUAR',
                'FORD',
                'JAN',
                'JOHN DEERE',
                'KRONE',
                'KUHN',
                'MASSEY FERGUSON',
                'NEW HOLLAND',
                'NOGUEIRA',
                'TATU MARCHESAN',
                'VALTRA VALMET',
                'YANMAR'
            ];
        }

        if (productId == 10) {
            return [
                'OUTROS',
                'CASE',
                'CATERPILLAR',
                'DOOSAN',
                'FIAT ALLIS',
                'HYUNDAI',
                'JOHN DEERE',
                'KOMATSU',
                'MASSEY FERGUSON',
                'NEW HOLLAND',
                'SANY',
                'VOLVO',
                'XCMG'
            ];
        }

        if (productId == 13) {
            return [
                'OUTROS',
                'BALDAN',
                'DMB',
                'FORD',
                'JACTO',
                'JAN',
                'JOHN DEERE',
                'LS TRACTOR',
                'MASSEY FERGUSON',
                'NEW HOLLAND',
                'NOGUEIRA',
                'STARA',
                'TATU MARCHESAN',
                'VALTRA VALMET',
                'VICON',
                'YANMAR'
            ];
        }

        if (productId == 14) {
            return [
                'OUTROS',
                'BOBCAT',
                'CASE',
                'FORD',
                'JCB',
                'JOHN DEERE',
                'MASSEY FERGUSON',
                'MOTOCANA',
                'NEW HOLLAND',
                'SANTAL',
                'VALTRA VALMET'
            ];
        }

        if (productId == 15) {
            return [
                'OUTROS',
                'CASE',
                'JOHN DEERE',
                'MONTANA'
            ];
        }

        if (productId == 16) {
            return [
                'OUTROS',
                'CASE',
                'JACTO',
                'MASSEY FERGUSON',
                'NEW HOLLAND',
                'TDI',
                'VALTRA VALMET',
                'YANMAR'
            ];
        }

        if (productId == 19) {
            return [
                'OUTROS',
                'DRIA'
            ];
        }

        if (productId == 20) {
            return [
                'OUTROS',
                'CASE',
                'CATERPILLAR',
                'FIAT ALLIS',
                'JOHN DEERE',
                'KOMATSU',
                'NEW HOLLAND',
                'SANTAL',
                'VALTRA VALMET',
                'VOLVO'
            ];
        }

        if (productId == 21) {
            return [
                'OUTROS',
                'BALDAN',
                'CASE',
                'CIVEMASA',
                'JACTO',
                'JAN',
                'JOHN DEERE',
                'JUMIL',
                'KUHN',
                'MARCHESAN',
                'MASSEY FERGUSON',
                'NEW HOLLAND',
                'NOGUEIRA',
                'STARA',
                'STILL',
                'VALTRA VALMET',
                'VENCE TUDO',
                'VOLVO'
            ];
        }
    
        if (productId == 22) {
            return [
                'OUTROS',
                'AGRONORTE',
                'CASE',
                'FORD',
                'JACTO',
                'JOHN DEERE',
                'JUMIL',
                'MASSEY FERGUSON',
                'MONTANA',
                'STARA',
                'STILL',
                'TATU MARCHESAN',
                'VALTRA VALMET',
                'VENCE TUDO'
            ];
        }

        if (productId == 23) {
            return [
                'OUTROS',
                'CASE',
                'CLAAS JAGUAR',
                'FORD',
                'GTS',
                'JACTO',
                'JOHN DEERE',
                'MACDON',
                'MASSEY FERGUSON',
                'MONTANA',
                'NEW HOLLAND',
                'SLC',
                'STARA',
                'VALTRA VALMET',
                'VOLVO'
            ];
        }

        if (productId == 25) {
            return [
                'OUTROS',
                'AGRALE',
                'BALDAN',
                'CASE',
                'CLAAS JAGUAR',
                'GTS',
                'GTS DO BRASIL',
                'JACTO',
                'JAN',
                'JOHN DEERE',
                'MASSEY FERGUSON',
                'METHAL-C',
                'MONTANA',
                'NEW HOLLAND',
                'NOGUEIRA',
                'OXBO',
                'SLC',
                'STARA',
                'TURIM',
                'VALTRA VALMET',
                'VENCE TUDO',
                'VOLVO'
            ];
        }

        if (productId == 26) {
            return [
                'OUTROS',
                'BALDAN',
                'CASE',
                'DMB',
                'FORD',
                'JACTO',
                'JAN',
                'JOHN DEERE',
                'MARCHESAN',
                'MASSEY FERGUSON',
                'MONTANA',
                'NATALI',
                'NEW HOLLAND',
                'STARA',
                'TATU MARCHESAN',
                'VALTRA VALMET'
            ];
        }

        // 27 Foi excluido do banco, pulverizador não acoplado ao trator que não existe na sompo

        if (productId == 28) {
            return [
                'OUTROS',
                'BOBCAT',
                'CASE',
                'CATERPILLAR',
                'JCB',
                'JOHN DEERE',
                'MASSEY FERGUSON',
                'NEW HOLLAND',
                'SANTAL',
                'VALTRA VALMET',
                'VOLVO'
            ];
        }

        if (productId == 29) {
            return [
                'OUTROS',
                'BALDAN',
                'CASE',
                'JAN',
                'JOHN DEERE',
                'KUHN',
                'MASSEY FERGUSON',
                'NEW HOLLAND',
                'NOGUEIRA',
                'STARA',
                'VALTRA VALMET'
            ];
        }

        if (productId == 30) {
            return [
                'OUTROS',
                'BALDAN',
                'CASE',
                'JACTO',
                'JOHN DEERE',
                'MASSEY FERGUSON',
                'NEW HOLLAND',
                'NOGUEIRA',
                'SEMEATO',
                'STARA',
                'TATU MARCHESAN',
                'VALTRA VALMET',
                'VENCE TUDO'
            ];
        }

        if (productId == 31) {
            return [
                'OUTROS',
                'ANTONIOSI',
                'BALDAN',
                'CASE',
                'CIVEMASA',
                'DMB',
                'JOHN DEERE',
                'MASSEY FERGUSON',
                'NEW HOLLAND',
                'NOGUEIRA',
                'SANTA IZABEL',
                'SANTAL',
                'TESTON',
                'TRACAN',
                'USICAMP',
                'VALTRA VALMET',
                'VOLVO'
            ];
        }

        if (productId == 32) {
            return [
                'OUTROS',
                'CASE',
                'CATERPILLAR',
                'FIAT ALLIS',
                'FORD',
                'JOHN DEERE',
                'KOMATSU',
                'MASSEY FERGUSON',
                'NEW HOLLAND',
                'VALTRA VALMET',
                'YANMAR'
            ];
        }
    }

	return {
		getAvailableBrands: getAvailableBrands
    }
    
    // IDs que aparece o POPUP 4, 5, 15, 16
});