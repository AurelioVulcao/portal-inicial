angular.module('vencorr').controller('LoginController', function($scope, LoginService, StorageService, Facebook, $window, config) {
	$scope.error = false;
	$scope.respError = { error: false, message: '' };
	StorageService.clear();

	function authenticate() {
		var email = $scope.email;
		var password = $scope.password;
		
		if(!email || !password) {
			$scope.respError.error = true;
			$scope.respError.message = 'E-mail ou senha inválido';
			return;
		}

		$scope.loading = true;
		$scope.respError.status = false;
		LoginService.authenticate(email, password).then(function(resp) {
			$scope.loading = false;
			LoginService.storageLogin(resp.data);
			window.location.href = '/dashboard';
		}).catch(function(err) {
			console.log('Entrou no Erro', err);
			$scope.loading = false;
			$scope.respError.status = true;
			$scope.respError.message = 'E-mail ou senha inválido';
		});
	}

	function facebookAuth() {
        Facebook.login(function(response) {
			if (response && response.status === 'unknown') {
				$scope.respError.error = true;
				$scope.respError.message = 'Falha ao tentar entrar com facebook';
				return;
			}

			if (response && response.status === 'connected') {
				$scope.respError.error = false;
				me();
			}
        }, { scope: 'email' });
	}
	
	function me() {
		Facebook.api('/me?fields=name,email', function(response) {
			LoginService.socialAuthenticate(response.email).then(function(resp) {
				$scope.loading = false;
				LoginService.storageLogin(resp.data);
				window.location.href = '/dashboard';
			}).catch(function(err) {
				console.log(err)
				$scope.loading = false;
				$scope.respError.status = true;
				$scope.respError.message = 'Usuário desativado ou não existente';
			});
		});
	}

	function linkedinSigin() {
		var clientId = '786o5ie60uvoev';
		var redirect_uri = config.redirectUriLinkedin;
		var state = 'dshiudfDFoifjkdhfd';
		var scope = 'r_emailaddress';
		var url = 'https://www.linkedin.com/oauth/v2/authorization';
		
		url += '?response_type=code';
		url += '&client_id=' + clientId;
		url += '&redirect_uri=' + redirect_uri;
		url += '&state=' + state;
		url += '&scope=' + scope;

		$window.location.href = url;
	}

	$scope.authenticate = authenticate;
	$scope.facebookAuth = facebookAuth;
	$scope.linkedinSigin = linkedinSigin;
});