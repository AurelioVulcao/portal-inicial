var gulp = require('gulp');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var es = require('event-stream');
var htmlmin = require('gulp-htmlmin');
var cleanCSS = require('gulp-clean-css');
var runSequence = require('run-sequence');
var rename = require('gulp-rename');
var ngAnnotate = require('gulp-ng-annotate');
var replace = require('gulp-replace');

gulp.task('clean', function() {
	return gulp.src('dist/')
	.pipe(clean());
});

gulp.task('uglify', function() {
	return es.merge([
		gulp.src([
            'front_modules/jquery/dist/jquery.min.js',
            'front_modules/bootstrap/dist/js/bootstrap.min.js',
            'front_modules/angular/angular.min.js',
            'front_modules/angular-route/angular-route.min.js',
            'front_modules/angular-sanitize/angular-sanitize.min.js',
            'local_modules/locale/angular-locale_pt-br.js',
            'local_modules/angular-input-masks/angular-input-masks-standalone.min.js',
            'front_modules/angular-base64/angular-base64.min.js',
            'front_modules/angular-bootstrap/ui-bootstrap-tpls.min.js',
            'front_modules/moment/min/moment-with-locales.min.js',
			'front_modules/angular-moment/angular-moment.min.js',
			'local_modules/locale/moment-pt-br.js',
            'front_modules/sweetalert/dist/sweetalert.min.js',
            'front_modules/ngSweetAlert/SweetAlert.min.js',
			'front_modules/angular-jwt/dist/angular-jwt.min.js',
			'front_modules/angular-facebook/lib/angular-facebook.js',
			'front_modules/angularjs-datepicker/dist/angular-datepicker.min.js',
			'front_modules/ng-file-upload/ng-file-upload.min.js',
			'front_modules/chart.js/dist/Chart.min.js',
			'front_modules/angular-chart.js/dist/angular-chart.min.js',
			'local_modules/uuid/angular-uuid.js',
			'front_modules/angular-ui-select/dist/select.min.js'
		]),
		gulp.src(['js/**/*.js'])
		.pipe(concat('scripts.js'))
		.pipe(ngAnnotate())
	])
	.pipe(concat('app.js'))
	.pipe(uglify())
	.pipe(gulp.dest('dist'));
});

gulp.task('copy_local_modules', function() {
	return gulp.src('local_modules/sails-socket/sails.io.js')
	.pipe(gulp.dest('dist/local_modules/sails-socket'));
});

gulp.task('htmlmin', function() {
	return gulp.src('views/*.html')
	.pipe(htmlmin({collapseWhitespace: true}))
	.pipe(gulp.dest('dist/views'));
});

gulp.task('cssmin', function() {
	return gulp.src([
		'front_modules/bootstrap/dist/css/bootstrap.min.css',
		'front_modules/sweetalert/dist/sweetalert.css',
		'front_modules/angularjs-datepicker/dist/angular-datepicker.min.css',
		'front_modules/angular-ui-select/dist/select.min.css',
		'css/*.css'])
	.pipe(cleanCSS())
	.pipe(concat('app.min.css'))
	.pipe(gulp.dest('dist'));
});

gulp.task('copy', function() {
	return gulp.src('index-prod.html')
	.pipe(rename('index.html'))
	.pipe(gulp.dest('dist/'));
});

gulp.task('img', function() {
	return gulp.src('img/*.{png,jpg,jpeg,gif,ico,svg}')
	.pipe(gulp.dest('dist/img/'));
});

gulp.task('logoInsurers', function() {
	return gulp.src('img/seguradoras/*.{png,jpg,jpeg,gif,ico}')
	.pipe(gulp.dest('dist/img/seguradoras'));
});


gulp.task('fonts', function() {
    return gulp.src('front_modules/bootstrap/fonts/*')
        .pipe(gulp.dest('dist/fonts'))
});

var globalHash = Date.now();

gulp.task('renamecss', ['uglify', 'htmlmin', 'cssmin'], function() {
	return gulp.src('dist/app.min.css')
	.pipe(rename('app-' + globalHash + '.min.css'))
	.pipe(gulp.dest('dist/'));
});

gulp.task('renamejs', ['uglify', 'htmlmin', 'cssmin'], function() {
	return gulp.src('dist/app.js')
	.pipe(rename('app-' + globalHash + '.js'))
	.pipe(gulp.dest('dist/'));
});

gulp.task('updateReference', ['copy', 'renamecss', 'renamejs'], function() {
	gulp.src(['dist/index.html'])
	.pipe(replace('@css', 'app-' + globalHash + '.min.css'))
	.pipe(replace('@js', 'app-' + globalHash + '.js'))
	.pipe(gulp.dest('dist'));
});

gulp.task('default', function(cb) {
	return runSequence('clean', ['uglify', 'htmlmin', 'fonts', 'cssmin', 'copy', 'copy_local_modules', 'img', 'logoInsurers', 'renamecss', 'renamejs', 'updateReference'], cb);
});