angular.module('vencorr').config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/login.html',
            controller: 'LoginController'
        })

        .when('/auth/linkedin/callback', {
            templateUrl: 'views/linkedin-callback.html',
            controller: 'LinkedinCallbackController'
        })

        .when('/dashboard', {
            templateUrl: 'views/dashboard.html',
            controller: 'DashboardController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/config', {
            templateUrl: 'views/config.html',
            controller: 'ConfigController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/chat', {
            templateUrl: 'views/chat.html',
            controller: 'ChatController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/clientes', {
            templateUrl: 'views/customers.html',
            controller: 'CustomersController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/cliente/:id', {
            templateUrl: 'views/customer.html',
            controller: 'CustomerController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/carteira', {
            templateUrl: 'views/wallet.html',
            controller: 'WalletController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/sinistros', {
            templateUrl: 'views/injuries.html',
            controller: 'InjuriesController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/sinistro/:id', {
            templateUrl: 'views/injury.html',
            controller: 'InjuryController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/sinistro-editar/:id?', {
            templateUrl: 'views/injury-form.html',
            controller: 'InjuryFormController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/cliente-cadastrar/:id?', {
            templateUrl: 'views/customer-form.html',
            controller: 'CustomerFormController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/suporte', {
            templateUrl: 'views/support.html',
            controller: 'SupportController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/cotacao-geral/:hash?', {
            templateUrl: 'views/quotation-generic.html',
            controller: 'QuotationGenericController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/cotacao-residencial/:hash?', {
            templateUrl: 'views/quotation-residencial.html',
            controller: 'QuotationResidencialController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/cotacao-equipamento/:hash?', {
            templateUrl: 'views/quotation-equipment.html',
            controller: 'QuotationEquipmentController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/cotacao-empresarial/:hash?', {
            templateUrl: 'views/quotation-empresarial.html',
            controller: 'QuotationEmpresarialController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/cotacao-agro-sompo/:hash', {
            templateUrl: 'views/quotation-agro-sompo-details.html',
            controller: 'QuotationAgroSompoDetailsController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/cotar-seguimento', {
            templateUrl: 'views/quote-segment.html',
            controller: 'QuoteSegmentController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/cotacao-agro/:hash?', {
            templateUrl: 'views/quotation-agro-sompo.html',
            controller: 'QuotationAgroSompoController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/cotacao-auto/:hash?', {
            templateUrl: 'views/quotation-auto.html',
            controller: 'QuotationAutoController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/cotacoes', {
            templateUrl: 'views/quotations.html',
            controller: 'QuotationsController',
            resolve: {
                authenticated : function(AuthFactory) {
                    AuthFactory.isAuthenticated();
                }
            }
        })

        .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'LoginController'
        });

    $routeProvider.otherwise({
        redirectTo: "/"
    });
    
	$locationProvider.html5Mode(true);
});