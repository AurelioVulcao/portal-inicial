module.exports = {
	attributes: {
		messages: {
			type: 'ref',
			columnType: 'text'
		},
    	quotation: {
			model: 'quotation',
			columnName: 'quotation_id'
		},
		company: {
			model: 'company',
			columnName: 'company_id'
		},
	}
};
  
  