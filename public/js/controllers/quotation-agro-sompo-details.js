angular.module('vencorr').controller('QuotationAgroSompoDetailsController', function($scope, $routeParams, $location, 
	QuotationService, UtilService, CustomerService, SweetAlert, RobotsService, SompoService, $window, $base64) {
	
	$scope.loadingShare = false;
	$scope.searchCustomer = {};
	$scope.customerSnapshot = {};
	$scope.quotationHash = $routeParams.hash;
	$scope.loadingCustomer = false;
	$scope.loadingAutoQuotation = false;
	$scope.loadingInstallments = false;
	$scope.customerId;
	$scope.quotation = {};
	$scope.loading = false;
	$scope.loadingNF = false;
	$scope.loadingDateNF = false;
	$scope.loadingSerieNumber = false;
	$scope.isEditItemNF = false;
	$scope.isEditItemDateNF = false;
	$scope.isEditItemSerieNumber = false;
	$scope.currentItem;
	$scope.loadingPDF = false;
	$scope.chooseCustomer = false;
	
	function init() {
		getQuotationData($scope.quotationHash);
	}

	function getCurrentItem(item) {
		$scope.currentItem = item;
	}

	function getQuotationData(hash, hideLoading) {
		if (!hideLoading) {
			$scope.loading = true;
		}

		QuotationService.get(hash).then(function(resp) {
			$scope.quotation = formatData(resp.data);
			$scope.loading = false;
			$scope.loadingCustomer = false;
			generateInstallments();
			setTransmissionRequirements();
			setAutoQuotationRequirements();
			checkTransmissionRequirements();
			checkAutoQuoteRequirements();

			if ($scope.quotation.customer_snapshot) {
				$scope.customerSnapshot = JSON.parse($scope.quotation.customer_snapshot);
			}

			// Set saved installment
			if ($scope.quotation.installments) {
				$scope.installments.forEach(function(item) {
					if (+item.description.substring(0, 1) === $scope.quotation.installments) {
						$scope.quotation.installments = item;
					}
				});
			}

			// Load pre data customer form
			if (!$scope.quotation.customer) {
				$scope.quotation.customer = {};
				$scope.quotation.customer.type = 'fisica';
				$scope.quotation.customer.genre = 'masculino';
				$scope.quotation.customer.rg_uf = 'SP';
			}
		});
	}

	function getAddressByCep(cep) {
		CustomerService.getAdressByCep(cep).then(function(resp) {
			var response = resp.data;

			$scope.quotation.customer.city = response.city;
			$scope.quotation.customer.address = response.street + ' - ' + response.neighborhood;
			$scope.quotation.customer.cep = response.cep;
			$scope.quotation.customer.state = response.state;

			if (!response.street || !response.neighborhood) {
				$scope.quotation.customer.address.street = '';
			}
		}).catch(function(err) {
			SweetAlert.warning("Ops! Não encontramos o CEP digitado", 'Utilizamos 2 serviços para busca de CEP e não encontramos, verifique se você realmente digitou corretamente ou complete manualmente o endereço')
		});
	}

	function saveCustomer() {
		var customer = JSON.parse(JSON.stringify($scope.quotation.customer));
		var validatedCustomer = validateCustomer(customer);

		if (!validatedCustomer) {
			return;
		}

		$scope.loadingCustomer = true;
		closeModal();
		CustomerService.save({ customer: customer }).then(function(resp) {
			var data = {
				id: $scope.quotation.id,
				start_term: $scope.quotation.start_term,
				end_term: $scope.quotation.end_term,
				comission: $scope.quotation.comission,
				items: $scope.quotation.items,
				hash: $scope.quotation.hash,
				customer_id: resp.data.id,
				customer_snapshot: JSON.stringify(resp.data)
			};

			QuotationService.update(data).then(function(resp) {
				getQuotationData($scope.quotationHash, true);
				$scope.loadingCustomer = false;
			}).catch(function(err) {
				SweetAlert.warning("Ops, Ocorreu um erro na rede", 'Desculpe pelo transtorno, você poderia cadastrar o cliente novamente por favor?');
			});
		}).catch(function(err) {
			if (err.data === 'customer already exist') {
				SweetAlert.info('Cliente já existe', 'Um cliente com este EMAIL ou CPF/CNPJ, já existe, verifique por favor.');
			} else {
				SweetAlert.warning("Ops, Ocorreu um erro na rede", 'Desculpe pelo transtorno, você poderia cadastrar o cliente novamente por favor?');
			}
			
			$scope.loadingCustomer = false;
		});
	}

	function closeModal() {
		$('#customerModal').modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
	}

	function closeModalQuestions() {
		$('#riskModal').modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
	}

	function validateCustomer(customer) {
		var missingFields = [];
		var fields = { 
			'name': 'Nome',
			'cpf': 'CPF',
			'birthdate': 'Data de Nascimento',
			'rg_number': 'RG',
			'rg_org': 'Rg Orgão Expeditor',
			'rg_uf': 'RG UF',
			'rg_emission_date': 'RG Data de Emissão',
			'cnpj': 'CNPJ',
			'email': 'Email',
			'phone': 'Telefone',
			'cep': 'CEP',
			'state': 'Estado',
			'city': 'Cidade',
			'address': 'Endereço',
			'number': 'Número' };

		Object.keys(fields).forEach(function(key) {
			if (!customer[key]) {
				if (key === 'cnpj' && customer.type === 'fisica') {
					return;
				}

				if (key.includes('rg') && customer.type === 'juridica') {
					return;
				}

				if (key === 'birthdate' && customer.type === 'juridica') {
					return;
				}

				if (key === 'genre' && customer.type === 'juridica') {
					return;
				}

				if (key === 'cpf' && customer.type === 'juridica') {
					return;
				}

				missingFields.push(fields[key]);
			}
		});

		missingFields = missingFields.join(', ');

		if (missingFields.length) {
			SweetAlert.warning("Preencha todos os campos obrigatórios", missingFields.toString());
			return false;
		} else {
			return customer;
		}
	}

	function formatData(data) {
		if (!data || !data.id) {
			console.log('Erro');
		}

		data.start_term = moment(data.start_term, 'YYYY-MM/DD').format('DD/MM/YYYY');
		data.end_term = moment(data.end_term, 'YYYY-MM/DD').format('DD/MM/YYYY');
		data.items = JSON.parse(data.items);
		data.presentationalComission = formatPresentationalComission(data.comission);

		if (data.items.length) {
			data.items.forEach(function(item) {
				if (!item.questions) {
					item.questions = {};
					item.questions.tracker = 'nenhum';
				}
			});
		}

		return data;
	}

	function formatPresentationalComission(comission) {
		if (comission == '40%') return '24%';
		if (comission == '35%') return '21%';
		if (comission == '30%') return '18%';
		if (comission == '25%') return '15%';
		if (comission == '20%') return '12%';
	}

	function generateInstallments() {
		var totalPrice = $scope.quotation.premium;
		var installments = [];

		for (var i=1; i < 7; i++) {
			installments.push({
				description: i + 'x de ' + UtilService.formatMoney((totalPrice / i)) + ' (sem juros)',
				price: (totalPrice / i)
			});
		}

		$scope.installments = installments;
		$scope.quotation.installment = installments[0];
	}

	function setTransmissionRequirements() {
		$scope.transmissionRequirements = [
			{
				id: 4,
				description: 'Proposta Aceita',
				status: false
			}
		]
	}

	function setAutoQuotationRequirements() {
		$scope.autoQuotationRequirements = [
			{
				id: 1,
				description: 'Cadastro de cliente completo',
				status: false
			},
			{
				id: 2,
				description: 'Questionário de risco de todos os itens',
				status: false
			},
			{
				id: 3,
				description: 'Completar informações de NF dos itens',
				status: false
			}
		]
	}

	function checkAutoQuoteRequirements() {
		var quotation = $scope.quotation;
		$scope.autoQuotationrequirementCount = 0;

		$scope.autoQuotationRequirements.forEach(function(requirement) {
			// Cadastro de cliente
			if (quotation && quotation.customer
				&& quotation.customer.id && requirement.id === 1) {
				requirement.status = true;
			}

			// Questionario de Risco
			if (requirement.id === 2) {
				var filled = true;
				var keys = ['water', 'nf', 'rent', 'use']

				$scope.quotation.items.forEach(function(item) {
					keys.forEach(function(questionKey) {
						if (!item.questions[questionKey]) {
							filled = false;
						}
					});
				});

				requirement = requirement.status = filled;
			}

			// Informações de NF
			if (requirement.id === 3) {
				var completed = true;

				$scope.quotation.items.forEach(function(item) {
					if (!item.nf_number || !item.nf_delivery_data || !item.serie_number) {
						completed = false;
					}
				});

				requirement = requirement.status = completed;
			}
		});

		$scope.autoQuotationRequirements.forEach(function(requirement) {
			if (requirement.status) {
				$scope.autoQuotationrequirementCount++;
			}
		});
	}

	function checkTransmissionRequirements() {
		var quotation = $scope.quotation;
		$scope.transmissionRequirementCount = 0;

		$scope.transmissionRequirements.forEach(function(requirement) {
			// Proposta Aceita
			if (quotation && quotation.status === 'accepted' && requirement.id === 4) {
				requirement.status = true;
			}
		});

		$scope.transmissionRequirements.forEach(function(requirement) {
			if (requirement.status) {
				$scope.transmissionRequirementCount++;
			}
		});
	}

	function share() {
		var emailCheck =  UtilService.validateEmail($scope.quotation.shareEmail);
		
		if (!emailCheck) {
			alert('Digite um email válido');
			return;
		}

		$scope.loadingShare = true;
		SompoService.generatePDF({ 
			data: $scope.quotation.items,
			period: {
				start: $scope.quotation.start_term,
				end: $scope.end_term 
			},
			installments: $scope.quotation.installments,
			share: {
				status: true,
				email: $scope.quotation.shareEmail
			}
		}).then(function(resp) {
			$scope.loadingShare = false;
			SweetAlert.success('Email enviado', 'Destino: ' + $scope.quotation.shareEmail);
		}).catch(function(err) {
			$scope.loadingShare = false;
			SweetAlert.error('Não conseguimos enviar o email', 'Verifique se o email que digitou é um email válido');
		});
	}

	function validatePeriod(quotation) {
		var isSame = moment(quotation.start_term, 'DD/MM/YYYY').isSame(moment(), 'day');

		if (isSame) {
			return true;
		}

		SweetAlert.swal({
			title: "A data de vigência é inferior ao dia de hoje",
			text: "Clique em Atualizar, para corrigir automaticamente a vigência para data de hoje",
			type: "info",
			showCancelButton: true,
			confirmButtonText: "Atualizar Data",
			cancelButtonText: "Cancelar"
		}, function(isConfirm) {
			if (isConfirm) {
				QuotationService.updatePeriod({ 
					quotation_id: $scope.quotation.id,
					start: moment().format('DD/MM/YYYY'),
					end: moment().add(1, 'year').format('DD/MM/YYYY')
				}).then(function(resp) {
					getQuotationData($scope.quotationHash);
				});
			}
		});
	}

	function autoQuotation() {
		if ($scope.loadingAutoQuotation) {
			return;
		}

		var isPeriodValid = validatePeriod($scope.quotation);
		if (!isPeriodValid) {
			return;
		}

		$scope.loadingAutoQuotation = true;
		RobotsService.save({
			quotation: $scope.quotation,
			info: { description: 'Cotação agro na sompo', slug: 'quotation-agro-sompo' }
		}).then(function(resp) {
			RobotsService.updateQuotationQueueID({
				quotation_id: $scope.quotation.id,
				queue_id: resp.data.id
			}).then(function(response) {
				$scope.loadingAutoQuotation = false;
				$scope.quotation.queue = response.data.queue;
			});

		});
	}

	function transmit() {
		if ($scope.loadingAutoQuotation) {
			return;
		}

		var isPeriodValid = validatePeriod($scope.quotation);
		if (!isPeriodValid) {
			return;
		}

		$scope.loadingAutoQuotation = true;
		RobotsService.save({
			quotation: $scope.quotation,
			info: { description: 'Transmissão agro na sompo', slug: 'transmission-agro-sompo' }
		}).then(function(resp) {
			RobotsService.updateQuotationQueueID({
				quotation_id: $scope.quotation.id,
				queue_id: resp.data.id
			}).then(function(response) {
				$scope.loadingAutoQuotation = false;
				$scope.quotation.queue = response.data.queue;
			});
		});
	}

	function edit() {
		var hash = $scope.quotation.hash;
		$location.path('/cotacao-agro/' + hash);
	}

	function saveInstallments() {
		var installments = +$scope.quotation.installments.description.substring(0, 1);

		$scope.loadingInstallments = true;
		QuotationService.updateInstallments({ installments: installments, quotation_id: $scope.quotation.id }).then(function(resp) {
			$scope.loadingInstallments = false;
		});
	}

	function setAdditionalInfo(value, key) {
		if (!value) {
			alert('Valores vazios não são válidos');
			return;
		}

		if (key === 'nf_delivery_data') {
			$scope.loadingDateNF = true;
			$scope.isEditItemDateNF = false;
		}

		if (key === 'nf_number') {
			$scope.loadingNF = true;
			$scope.isEditItemNF = false;
		}

		if (key === 'serie_number') {
			$scope.loadingSerieNumber = true;
			$scope.isEditItemSerieNumber = false;
		}
		
		QuotationService.updateItemAdditionalInfo({ 
			items: $scope.quotation.items,
			quotation_id: $scope.quotation.id })
		.then(function(resp) {
			$scope.loadingDateNF = false;
			$scope.loadingNF = false;
			$scope.loadingSerieNumber = false;
			getQuotationData($scope.quotationHash, true);
		});
	}

	function openEdit(key) {
		if (key === 'nf_delivery_data') {
			$scope.isEditItemDateNF = true;
		}

		if (key === 'nf_number') {
			$scope.isEditItemNF = true;
		}

		if (key === 'serie_number') {
			$scope.isEditItemSerieNumber = true;
		}
	}

	function saveColaborator() {
		var code = $scope.quotation.sompo_colaborator_code;

		QuotationService.updateColaboratorCode({ 
			code: code,
			quotation_id: $scope.quotation.id })
		.then(function(resp) {
			getQuotationData($scope.quotationHash, true);
		});
	}

	function saveQuestion() {
		closeModalQuestions();
		QuotationService.updateItemAdditionalInfo({ 
			items: $scope.quotation.items,
			quotation_id: $scope.quotation.id })
		.then(function(resp) {
			getQuotationData($scope.quotationHash, true);
		});
	}

	function accept() {
		SweetAlert.swal({
			title: "Marcar como proposta aceita?",
			text: "Uma vez depois de aceita, não é mais possível editar a mesma",
			type: "info",
			showCancelButton: true,
			confirmButtonText: "Aceitar",
			cancelButtonText: "Cancelar"
			}, function(isConfirm) {
				if (isConfirm) {
				QuotationService.updateStatus({ 
					quotation_id: $scope.quotation.id,
					status: 'accepted' })
				.then(function(resp) {
					getQuotationData($scope.quotationHash, true);
				});
				}
			});
	}

	function getPdf() {
		$scope.loadingPDF = true;
		SompoService.generatePDF({ 
			data: $scope.quotation.items,
			period: {
				start: $scope.quotation.start_term,
				end: $scope.end_term 
			},
			installments: $scope.quotation.installments
		}).then(function(resp) {
			$scope.loadingPDF = false;
			window.open(resp.data.url, '_blank');
		}).catch(function(err) {
			$scope.loadingPDF = false;
		});
	}

	function enableCustomerSearch() {
		$scope.chooseCustomer = true;
	}

	function disableCustomerSearch() {
		$scope.searchCustomer.value = '';
		$scope.chooseCustomer = false;
	}

	function getCustomers(typedTerm) {
		return CustomerService.suggest(typedTerm).then(function(resp) {
			return resp.data.map(function(item) {
				return item;
			});
		});
	};

	function onSelectCustomer(customer) {
		var data = {
			customer: customer.id,
			customer_snapshot: JSON.stringify(customer),
			id: $scope.quotation.id
		};

		$scope.chooseCustomer = false;
		$scope.loadingCustomer = true;
		QuotationService.updateGeneral({
			quotation: data
		}).then(function(resp) {
			getQuotationData($scope.quotationHash, true);
			$scope.loadingCustomer = false;
			$scope.chooseCustomer = false;
		}).catch(function(err) {
			$scope.chooseCustomer = true;
			$scope.loadingCustomer = false;
			SweetAlert.warning("Ops, Ocorreu um erro na rede", 'Desculpe pelo transtorno, você poderia cadastrar o cliente novamente por favor?');
		});
	}

	function editCustomerData() {
		$window.location.href = '/cliente-cadastrar/' +  $base64.encode($scope.quotation.customer.id);
	}

	init();
	$scope.editCustomerData = editCustomerData;
	$scope.onSelectCustomer = onSelectCustomer;
	$scope.getCustomers = getCustomers;
	$scope.disableCustomerSearch = disableCustomerSearch;
	$scope.enableCustomerSearch = enableCustomerSearch;
	$scope.generateInstallments = generateInstallments;
	$scope.saveInstallments = saveInstallments;
	$scope.setAdditionalInfo = setAdditionalInfo;
	$scope.getAddressByCep = getAddressByCep;
	$scope.saveCustomer = saveCustomer;
	$scope.saveColaborator = saveColaborator;
	$scope.autoQuotation = autoQuotation;
	$scope.getPdf = getPdf;
	$scope.openEdit = openEdit;
	$scope.transmit = transmit;
	$scope.share = share;
	$scope.edit = edit;
	$scope.accept = accept;
	$scope.getCurrentItem = getCurrentItem;
	$scope.saveQuestion = saveQuestion;
});