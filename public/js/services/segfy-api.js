angular.module('vencorr').factory('SegfyService', function ($http, config) {
    function listProfessions() {
        return $http.get(config.baseUrl + '/segfy/professions');
    }

    function listBrands() {
        return $http.get(config.baseUrl + '/segfy/brands');
    }

    function listModels(brandId, modelYear) {
        return $http.get(config.baseUrl + '/segfy/models/' + brandId + '/' + modelYear);
    }

    function listAntiTheft(category) {
        return $http.get(config.baseUrl + '/segfy/antitheft/' + category);
    }

    return {
        listProfessions: listProfessions,
        listAntiTheft: listAntiTheft,
        listBrands: listBrands,
        listModels: listModels,
    };
});