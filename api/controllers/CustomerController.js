const moment = require('moment');

async function searchCep(req, res) {
    const { cep } = req.body;
    const response = await CepService.getInfoByCep(cep);
    res.status(response.status).json(response.data);
}

async function save(req, res) {
    const { customer } = req.body;
    const tokenData = UtilService.getTokenData(req);
    
    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        let orQuery = {};

        if (customer.type === 'juridica') {
            orQuery = [
                { email: customer.email },
                { cnpj: customer.cnpj }
            ];
        } else {
            orQuery = [
                { email: customer.email },
                { cpf: customer.cpf }
            ];
        }
        const existCustomer = await Customer.count({
            where: {
                company: tokenData.company_id,
                or : orQuery
            }
        });

        if (existCustomer) {
            res.status(403).json('customer already exist');
            return;
        }

        customer.company = tokenData.company_id;
        const customerCreated = await Customer.create(customer).fetch();
        await UtilService.saveActivity({ 
            user: tokenData.user_id,
            company: tokenData.company_id,
            model: 'customer',
            description: `${tokenData.name} cadastrou um cliente`,
            data: JSON.stringify({ id: customerCreated.id })
        });

        res.status(200).json(customerCreated);
    } catch(err) {
        res.status(500).json('error on create customer');
        return;
    }
}

async function get(req, res) {
    const { customer_id } = req.params;

    const customer = await Customer.findOne({
        where: {
            id: customer_id
        }
    });

    if (!customer || !customer.id) {
        res.status(401).json({
            message: 'can not find customer'
        });
        return;
    }

    res.status(200).json(customer);
}

async function list(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { params } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    let filter = {
        company: tokenData.company_id,
    };

    if (params && params.prop) {
        if (params.prop === 'document') {
            filter['or'] = [
                    { cpf: {'contains': params.search } },
                    { cnpj: {'contains': params.search } }
                ];
        } else {
            filter[params.prop] = {
                'contains': params.search
            };
        }
    }

    try {
        const amount = await Customer.count(filter);
        const customers = await Customer.find({ where: filter }).paginate((params.page - 1), 10).sort('created_at DESC');
        res.status(200).json({
            customers,
            amount
        });
    } catch(err) {
        res.status(500).json({
            err: err
        });
    }
}

async function suggest(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { term } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        const customers = await Customer.find({
            where: {
                company: tokenData.company_id,
                name: {
                    'contains': term
                }
            },
            limit: 10
        });

        res.status(200).json(customers);
    } catch(err) {
        res.status(500).json({
            err: err
        });
    }
}

async function update(req, res) {
    const tokenData = UtilService.getTokenData(req);
    const { customer } = req.body;

    if (!tokenData.company_id || !tokenData.user_id) {
        res.status(401).json({
            message: 'user not authorized'
        });
        return;
    }

    try {
        let orQuery = {};

        if (customer.type === 'juridica') {
            orQuery = [
                { email: customer.email },
                { cnpj: customer.cnpj }
            ];
        } else {
            orQuery = [
                { email: customer.email },
                { cpf: customer.cpf }
            ];
        }
        const existCustomer = await Customer.count({
            where: {
                company: tokenData.company_id,
                or : orQuery
            }
        });
        
        if (existCustomer > 1) {
            res.status(403).json('customer already exist');
            return;
        }

        delete customer.created_at;
        delete customer.updated_at;

        const updatedCustomer = await Customer.updateOne({ id: customer.id }).set(customer);
        await UtilService.saveActivity({ 
            user: tokenData.user_id,
            company: tokenData.company_id,
            model: 'customer',
            description: `${tokenData.name} atualizou os dados de um cliente`,
            data: JSON.stringify({ id: updatedCustomer.id })
        });

        res.status(200).json(updatedCustomer);
    } catch(err) {
        res.status(500).json({
            message: 'error on update user: ' + err
        });
    }
}

module.exports = {
    searchCep,
    suggest,
    update,
    save,
    list,
    get
};

