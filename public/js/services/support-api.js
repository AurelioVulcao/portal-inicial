angular.module("vencorr").factory("SupportService", function ($http, config) {
    function supportEmail(data) {
        return $http.post(config.baseUrl + '/support/offline/email', data);
    }

    return {
        supportEmail: supportEmail
    };
});