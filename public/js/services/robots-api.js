angular.module("vencorr").factory("RobotsService", function ($http, config) {
    function save(data) {
        return $http.post(config.baseUrl + "/queue/job", data);
    }

    function updateQuotationQueueID(data) {
        return $http.put(config.baseUrl + "/quotation/update/queue-info", data);
    }

    return {
        save: save,
        updateQuotationQueueID: updateQuotationQueueID
    };
});