angular.module('vencorr').controller('QuoteSegmentController', function($scope) {
    $scope.segments = [{
        url: '/cotacao-equipamento',
        img: '../../img/img-cotar-agricola.png',
        description: 'Equipamento'
    }, {
        url: '/cotacao-auto',
        img: '../../img/img-cotar-auto.png',
        description: 'Automóvel'
    }, {
        url: '/cotacao-residencial',
        img: '../../img/img-cotar-residencial.png',
        description: 'Residencial'
    }, {
        url: '/cotacao-empresarial',
        img: '../../img/img-cotar-const.png',
        description: 'Empresarial'
    }, {
        url: '/cotacao-geral',
        img: '../../img/img-cotar-outros.png',
        description: 'Outros'
    }];
});
