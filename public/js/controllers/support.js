angular.module('vencorr').controller('SupportController', function($scope, $rootScope, SupportService, SweetAlert, UtilService) {
	$scope.loading = false;
	$scope.contact = {};

	function send() {
		if (!$scope.contact.subject) {
			SweetAlert.warning('Preencha o assunto por favor');
			return;
		}

		if (!$scope.contact.message) {
			SweetAlert.warning('Preencha a mensagem por favor');
			return;
		}

		if ($scope.contact.message && $scope.contact.message.length < 10) {
			SweetAlert.warning('Mensagem muito curta', 'Descreva melhor o problema para que possamos lhe ajudar com mais eficiência');
			return;
		}
		
		var formattedContact = formatBeforeSend($scope.contact);
		$scope.loading = true;
		SupportService.supportEmail(formattedContact).then(function(resp) {
			$scope.loading = false;
			$scope.contact = {};
			SweetAlert.info('Sua mensagem foi enviada!', 'Entraremos em contato com você o mais rápido possível.');
		});
	}

	function formatBeforeSend(contact) {
		var contactObj = UtilService.clone(contact);
		var user = $rootScope.loggedUser;

		contactObj.name = user.name;
		contactObj.email = user.email;
		contactObj.phone = user.phone;
		contactObj.company_name = user.company.name;

		return contactObj;
	}

	$scope.send = send;
});