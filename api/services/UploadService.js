const config = require('../../config/custom');
const AWS = require('aws-sdk');
const fs =  require('fs');

async function toS3(filePath, contentType, folder) {
	let file = fs.readFileSync(filePath);
	let s3 = new AWS.S3({
		accessKeyId: config.custom.aws_access_key_id,
		secretAccessKey: config.custom.aws_secret_key,
    });

	const params = {
		Bucket: `portal-franqueado`,
		Key: `${folder}/${filePath.substring(40)}`,
		Body: file,
		ContentType: contentType,
		StorageClass: 'STANDARD',
		ACL: 'public-read'
    };
    
	return new Promise(function(resolve, reject) {
		s3.putObject(params, async function (err, data) {
            if (err) {
                reject('upload_fail: ' + err);
                return;
            }

            await deleteLocalImage(filePath);
            resolve(`https://portal-franqueado.s3.amazonaws.com/${folder}/${filePath.substring(40)}`);
        });
    });
}

async function deleteLocalImage(filePath) {
    return new Promise((resolve, reject) => {
        fs.unlink(filePath, (err) => {
            if (err) {
                reject('error_on_deleting_image: ' + err);
            }
            resolve();
        });
    });
}

module.exports = {
    toS3
}