angular.module('vencorr').factory('MetaService', function ($http, config) {
    function getTime() {
        return $http.get(config.baseUrl + '/hour');
    }

    function solicitationRequestEmail(data) {
        return $http.post(config.baseUrl + '/admin/quotation-request', data);
    }

    function quotationChosenEmail(data) {
        return $http.post(config.baseUrl + '/admin/quotation-chosen', data);
    }

    function injuryOpenedEmail(data) {
        return $http.post(config.baseUrl + '/admin/injury', data);
    }

    function directMessageEmail(data) {
        return $http.post(config.baseUrl + '/direct-email', { data: data});
    }

    return {
        getTime: getTime,
        injuryOpenedEmail: injuryOpenedEmail,
        quotationChosenEmail: quotationChosenEmail,
        solicitationRequestEmail: solicitationRequestEmail,
        directMessageEmail: directMessageEmail
    }
});